//
//  MySetHomeModel.swift
//  Voice
//
//  Created by kasim on 2018/11/25.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import Foundation
import RxDataSources

struct MySetHomeModel {
    var title: String = ""
}

struct SectionOfMySetHome {
    var header: String
    var items: [Item]
}

extension SectionOfMySetHome: SectionModelType {
    typealias Item = MySetHomeModel

    init(original: SectionOfMySetHome, items: [SectionOfMySetHome.Item]) {
        self = original
        self.items = items
    }
}
