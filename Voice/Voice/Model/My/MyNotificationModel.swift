//
//  MyNotificationModel.swift
//  Voice
//
//  Created by kasim on 2018/11/25.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import Foundation
import RxDataSources

struct MyNotificationModel {
    var title: String = ""
    var date: String = ""
    var isRead = false
}

struct SectionOfMyNotification {
    var header: String
    var items: [Item]
}

extension SectionOfMyNotification: SectionModelType {
    typealias Item = MyNotificationModel

    init(original: SectionOfMyNotification, items: [SectionOfMyNotification.Item]) {
        self = original
        self.items = items
    }
}
