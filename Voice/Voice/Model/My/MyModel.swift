//
//  MyModel.swift
//  Voice
//
//  Created by kasim on 2018/11/11.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import Foundation
import RxDataSources

struct MyModel {
    var title: String = ""
    var imageName: String = ""
}

struct MyTitleAndDot {
    var title: String = ""
    var hasDot: Bool = false
}

struct MyTitleAndSelectItem {
    var title: String = ""
    var selectItem: String = ""
}

struct MyHeader {
    var name: String = ""
    var currencies: String = ""
    var profileImageUrl: String = ""
    var isCheckIn: Bool = false
}

struct SingleSectionModelMy<T>: SectionModelType {

    typealias Item = T

    var header: String
    var items: [T]

    init(header: String, items: [T]) {
        self.header = header
        self.items = items
    }

    init(original: SingleSectionModelMy<T>, items: [T]) {
        self = original
        self.items = items
    }
}

struct SectionOfMy {
    var header: MyHeader
    var items: [Item]
}

extension SectionOfMy: SectionModelType {
    typealias Item = MyModel

    init(original: SectionOfMy, items: [SectionOfMy.Item]) {
        self = original
        self.items = items
    }
}
