//
//  MyAlarmClockModel.swift
//  Voice
//
//  Created by kasim on 2018/11/26.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import Foundation
import RxDataSources

struct MyAlarmClockModel {
    var title: String = ""
    var selectItem: String = ""
    var isOnForSwitch: Bool = false
}

struct SectionOfMyAlarmClock {
    var header: String
    var items: [Item]
}

extension SectionOfMyAlarmClock: SectionModelType {
    typealias Item = MyAlarmClockModel

    init(original: SectionOfMyAlarmClock, items: [SectionOfMyAlarmClock.Item]) {
        self = original
        self.items = items
    }
}
