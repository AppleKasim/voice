//
//  RecordModel.swift
//  Voice
//
//  Created by kasim on 2018/11/20.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import Foundation
import RxDataSources

struct RecordModel {
    var leftImageUrl = ""
    var title = ""
    var detail = ""
    var isKeepButton = false
    var keepAmount = ""
    var charge = ""
}

struct SectionOfRecordModel {
    var header: String
    var items: [Item]
}

extension SectionOfRecordModel: SectionModelType {
    typealias Item = RecordModel

    init(original: SectionOfRecordModel, items: [SectionOfRecordModel.Item]) {
        self = original
        self.items = items
    }
}
