//
//  PlayModel.swift
//  Voice
//
//  Created by kasim on 2018/11/23.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import Foundation
import RxDataSources

protocol PlayIntroductionType {

}

struct EmptyModel: Codable {
}

struct PlayModel: Codable {
    var name: String = ""
    var attention: Bool = false
}

struct PlayReviewModel: Codable, PlayIntroductionType {
    var profileImageUrl: String = ""
    var name: String = ""
    var date: String = ""
    var content: String = ""
}

struct PlayVoice: Codable, ListPageContent {
    var voiceId: String = ""
    var title: String = ""
    var url: String = ""
    var click: String = ""

    enum CodingKeys: String, CodingKey {
        case voiceId = "id"
        case title
        case url
        case click
    }
}

struct PlayIntroduction: Codable, PlayIntroductionType {
    let introductionId: String// = ""
    let title: String// = "123"
    let cover: String// = ""
    let introduce: String
    let isFree: String// = ""
    let price: String// = ""
    let authors: [AuthorBasic]// = []
    let voiceActors: [AuthorBasic]// = []

    enum CodingKeys: String, CodingKey {
        case introductionId = "id"
        case title
        case cover
        case introduce
        case isFree = "is_free"
        case price
        case authors = "autor"
        case voiceActors = "cv"
    }
}

struct SectionOfPlayIntroduction {
    var header: String
    var items: [Item]
}

extension SectionOfPlayIntroduction: SectionModelType {
    typealias Item = PlayReviewModel

    init(original: SectionOfPlayIntroduction, items: [SectionOfPlayIntroduction.Item]) {
        self = original
        self.items = items
    }
}
