//
//  CourseListModel.swift
//  Voice
//
//  Created by kasim on 2018/11/23.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import Foundation
import RxDataSources

struct CourseListModel {
    var title: String = ""
    var date: String = ""
}

struct SectionOfCourseList {
    var header: String
    var items: [Item]
}

extension SectionOfCourseList: SectionModelType {
    typealias Item = CourseListModel

    init(original: SectionOfCourseList, items: [SectionOfCourseList.Item]) {
        self = original
        self.items = items
    }
}
