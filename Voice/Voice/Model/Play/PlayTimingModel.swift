//
//  PlayTimingModel.swift
//  Voice
//
//  Created by kasim on 2018/11/23.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import Foundation
import RxDataSources

struct PlayTimingHeaderModel {
    var title: String = ""
    var isEnable: Bool = false
}

struct PlayTimingModel: Codable {
    var content: String = ""
}

struct SectionOfPlayTiming {
    var header: PlayTimingHeaderModel
    var items: [Item]
}

extension SectionOfPlayTiming: SectionModelType {
    typealias Item = PlayTimingModel

    init(original: SectionOfPlayTiming, items: [SectionOfPlayTiming.Item]) {
        self = original
        self.items = items
    }
}
