//
//  TextPageContentModel.swift
//  Voice
//
//  Created by kasim on 2018/11/18.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import Foundation
import RxDataSources

struct CreatorInfo: Codable {
    let creatorid: String
    let nickname: String
    let email: String
    let gender: String
    let education: String
    let birthday: String
    let photo: String?
    let brief: String
    let fans: Int

    enum CodingKeys: String, CodingKey {
        case creatorid = "id"
        case nickname = "nickname"
        case email = "email"
        case gender = "gender"
        case education = "education"
        case birthday = "birthday"
        case photo = "photo"
        case brief = "brief"
        case fans = "fans"
    }
}

struct TextPageContentModel: Codable {
    let audioBooksId: String = ""
    let title: String = ""
    let cover: String = ""
    let isFree: String = ""
    let price: String = ""
    let authors: [AuthorBasic] = []
    let voiceActors: [AuthorBasic] = []

    enum CodingKeys: String, CodingKey {
        case audioBooksId = "id"
        case title
        case cover
        case isFree = "is_free"
        case price
        case authors = "author"
        case voiceActors = "cv"
    }
}
