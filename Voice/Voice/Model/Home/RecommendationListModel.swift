//
//  RecommendationListModel.swift
//  Voice
//
//  Created by kasim on 2018/11/10.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import Foundation
import RxDataSources

struct SectionOfRecommendationList {
    var header: String
    var items: [Item]
}

extension SectionOfRecommendationList: SectionModelType {
    typealias Item = RecommendationModel

    init(original: SectionOfRecommendationList, items: [SectionOfRecommendationList.Item]) {
        self = original
        self.items = items
    }
}
