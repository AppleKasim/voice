//
//  PlayListPageContentModel.swift
//  Voice
//
//  Created by kasim on 2018/11/18.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import Foundation
import RxDataSources

protocol ListPageContent {
}

struct PlayListPageContentModel: Codable, ListPageContent {

//    let audioBooksId: String = ""
//    let title: String = ""
//    let cover: String = ""
//    let isFree: String = ""
//    let price: String = ""
//    let authors: [Author] = []
//    let voiceActors: [Author] = []

    var voicesId: String = ""
    var title: String = ""
    var url: String = ""
    var click: String = ""

    enum CodingKeys: String, CodingKey {
        case voicesId = "id"
        case title
        case url
        case click
    }
}

//struct CoursePageContentModel: Codable, ListPageContent {
//    var userAlbumsId: String = ""
//    var title: String = ""
//    var profile: String = ""
//    var isFree: String = ""
//    var price: String = ""
//    var introduce: String = ""
//    var authors: [AuthorBasic] = []
//    var voiceActors: [AuthorBasic] = []
//
//    enum CodingKeys: String, CodingKey {
//        case userAlbumsId = "id"
//        case title
//        case profile = "cover"
//        case isFree = "is_free"
//        case price
//        case introduce
//        case authors = "author"
//        case voiceActors = "cv"
//    }
//}

struct SectionOfPlayListPageContent {
    var header: String
    var items: [Item]
}

extension SectionOfPlayListPageContent: SectionModelType {
    typealias Item = ListPageContent

    init(original: SectionOfPlayListPageContent, items: [SectionOfPlayListPageContent.Item]) {
        self = original
        self.items = items
    }
}
