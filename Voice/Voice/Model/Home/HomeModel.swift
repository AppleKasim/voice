//
//  BannerImages.swift
//  Voice
//
//  Created by kasim on 2018/10/27.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import Foundation
import RxDataSources

protocol HomeModelType {
}

struct StringName: HomeModelType {
    var str: String = ""
}

struct ProductType: Codable {
    var productId: String = ""
    var title: String = ""

    enum CodingKeys: String, CodingKey {
        case productId = "id"
        case title
    }
}

struct Categories: Codable {
    var categoriesId: String = ""
    var title: String = ""

    enum CodingKeys: String, CodingKey {
        case categoriesId = "id"
        case title
    }
}

struct AudioBook: Codable, HomeModelType, ListPageContent {
    var audioBooksId: String = ""
    var title: String = ""
    var profile: String = ""
    var isFree: String = ""
    var price: String = ""
    var introduce: String = ""
    var authors: [AuthorBasic] = []
    var voiceAuthors: [AuthorBasic] = []

    enum CodingKeys: String, CodingKey {
        case audioBooksId = "id"
        case title
        case profile = "cover"
        case isFree = "is_free"
        case price
        case introduce
        case authors = "author"
        case voiceAuthors = "cv"
    }
}

struct AuthorBasic: Codable {
    let authorId: String
    let alias: String
    let brief: String?
    let email: String?
    let gender: String?
    let education: String?
    let birthday: String?
    let photo: String?

    enum CodingKeys: String, CodingKey {
        case authorId = "id"
        case alias
        case brief
        case email
        case gender
        case education
        case birthday
        case photo
    }
}

struct Author: Codable, HomeModelType {
    let authorId: String
    let alias: String
    let profileUrl: String?
    let brief: String
    let introduce: String

    enum CodingKeys: String, CodingKey {
        case authorId = "id"
        case alias
        case profileUrl = "photo"
        case brief
        case introduce
    }
}

struct RecommendationModel: Codable, HomeModelType {
    var coverImageUrl: String = ""
    var introduction: String = ""
    var course: String = ""
    var students = ""
    var isKeep = false
    var keepAmount = ""
    var chargeType = ""
    var courseModel = CourseModel()
}

struct AuthorModel: Codable, HomeModelType {
    var anchorId: String = ""
    var title: String = ""
    var profile: String = ""
    var isFree: String = ""
    var price: String = ""
    var authors: [AuthorBasic] = []
    var voiceActors: [AuthorBasic] = []

    enum CodingKeys: String, CodingKey {
        case anchorId = "id"
        case title
        case profile = "cover"
        case isFree = "is_free"
        case price
        case authors = "author"
        case voiceActors = "cv"
    }
}

struct CourseModel: Codable {
    var title: String = ""
    var authorName: String = ""
}

struct HomeHeader {
    var title: String = ""
    var titleEnglish: String = ""
}

struct SectionOfHome {
    var header: String
    var items: [Item]
}

extension SectionOfHome: SectionModelType {
    typealias Item = [HomeModelType]

    init(original: SectionOfHome, items: [SectionOfHome.Item]) {
        self = original
        self.items = items
    }
}

struct SingleSectionModel<Header, T>: SectionModelType {

    typealias Item = T

    var header: Header
    var items: [T]

    init(header: Header, items: [T]) {
        self.header = header
        self.items = items
    }

    init(original: SingleSectionModel<Header, T>, items: [T]) {
        self = original
        self.items = items
    }
}
