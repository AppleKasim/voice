//
//  AnchorListModel.swift
//  Voice
//
//  Created by kasim on 2018/11/12.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import Foundation
import RxDataSources

struct AuthorListHeader {
    var title: String = ""
    var englishTitle: String = ""

}

struct SectionOfAnchorList {
    var header: AuthorListHeader
    var items: [Item]
}

extension SectionOfAnchorList: SectionModelType {
    typealias Item = AuthorModel

    init(original: SectionOfAnchorList, items: [SectionOfAnchorList.Item]) {
        self = original
        self.items = items
    }
}
