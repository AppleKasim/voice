//
//  ClassificationModel.swift
//  Voice
//
//  Created by kasim on 2018/11/4.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import Foundation
import RxDataSources

struct ClassificationModel {
    var imageUrl: String = ""
    var content: String = ""
    var name: String = ""
    var englishName: String = ""
}

struct SectionOfClassification {
    var header: String
    var items: [Item]
}

extension SectionOfClassification: SectionModelType {
    typealias Item = ClassificationModel

    init(original: SectionOfClassification, items: [SectionOfClassification.Item]) {
        self = original
        self.items = items
    }
}
