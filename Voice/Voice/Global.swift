//
//  GlobalConstant.swift
//  Voice
//
//  Created by kasim on 2018/10/26.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import AVFoundation
import MediaPlayer

struct Global {

//    #if DEBUG
//    static let baseUrl = "http://test.iami-web.me"
//    //static let baseUrl = "http://jerry.iami-web.me"
//    #else
//    static let baseUrl = "http://www.iami-web.me"
//    #endif
//
    private init() {

    }

    static let baseUrl = "http://api.walkear.com/api"

    //Color
    static let yellowColorF8BF00 = UIColor(rgb: 0xF8BF00)

    static let blackColor00 = UIColor(rgb: 0x000000)
    static let blackColor29 = UIColor(rgb: 0x292929)
    static let blackColor333540 = UIColor(rgb: 0x333540)
    static let blackColor4F5358 = UIColor(rgb: 0x4F5358)
    static let blackColor121724 = UIColor(rgb: 0x121724)
    static let blackColor1F2336 = UIColor(rgb: 0x1F2336)
    static let blackColor2A2E43 = UIColor(rgb: 0x2A2E43)
    static let blackColor353A50 = UIColor(rgb: 0x353A50)

    static let grayColorD2 = UIColor(rgb: 0xd2d2d2)
    static let grayColorD9 = UIColor(rgb: 0xD9D9D9)
    static let grayColorB2 = UIColor(rgb: 0xB2B2B2)
    static let grayColorF5 = UIColor(rgb: 0xF5F5F5)
    static let grayColorEB = UIColor(rgb: 0xEBEBEB)
    static let grayColorE0 = UIColor(rgb: 0xE0E0E0)
    static let grayColor86 = UIColor(rgb: 0x868686)
    static let grayColor70 = UIColor(rgb: 0x707070)
    static let grayColor81878E = UIColor(rgb: 0x81878E)
    static let grayColorEFF4FC = UIColor(rgb: 0xEFF4FC)
    static let grayColor78849E = UIColor(rgb: 0x78849E)
    static let grayColor707D95 = UIColor(rgb: 0x707D95)
    static let grayColor4F5358 = UIColor(rgb: 0x4F5358)
    static let grayColor555869 = UIColor(rgb: 0x555869)

    static let redColorFF4F85 = UIColor(rgb: 0xFF4F85)
    static let redColorE3203E = UIColor(rgb: 0xE3203E)
    static let redColorFF415F = UIColor(rgb: 0xFF415F)
    static let redColorF63D27 = UIColor(rgb: 0xF63D27)

    static let greenColor00CE87 = UIColor(rgb: 0x00CE87)

    static let blueColorA2AACB = UIColor(rgb: 0xA2AACB)
    static let blueColorADCAFF = UIColor(rgb: 0xADCAFF)
    static let blueColor3C5B9B = UIColor(rgb: 0x3C5B9B)

    static let greenColor08BC7E = UIColor(rgb: 0x08BC7E)

    //Size
    static let screenWidth = UIScreen.main.bounds.width
    static let screenHeight = UIScreen.main.bounds.height

    static let notNavigationScreenHeight: CGFloat = {
        let navigationHeight: CGFloat = 44

        let statusbarHeight = UIApplication.shared.statusBarFrame.height
        return Global.screenHeight - navigationHeight - statusbarHeight
    }()

    static let columnTableViewCellHeight = 95 / 667 * screenHeight

    static var tabBarHeight: CGFloat = 0

//    static let purpleImage = #imageLiteral(resourceName: "Gradation")
//
//    struct DefaultsKey {
//        static let userKey = "userName"
//        static let token = "Token"
//        static let login = "Login"
//        static let imAccount = "IMAccount"
//        static let title = "title"
//        static let content = "Content"
//        static let isFirst = "IsFirst"
//        static let accessToken = "Access Token"
//        static let memberId = "member_id"
//        static let birth = "birth"
//        static let mobile = "mobile"
//        static let userImage = "image"
//        static let banner = "banner"
//        static let email = "email"
//        static let password = "password"
//        static let infoShow = "info_show"
//        static let language = "language"
//    }

}
