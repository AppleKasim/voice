//
//  CustomPageControl.swift
//  Voice
//
//  Created by kasim on 2018/11/30.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit

class CustomPageControl: UIPageControl {

    var currentPageImage: UIImage = UIImage(named: "bar-active")!
    var otherPagesImage: UIImage = UIImage(named: "bar")!

    override var numberOfPages: Int {
        didSet {
            updateDots()
        }
    }

    override var currentPage: Int {
        didSet {
            updateDots()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        pageIndicatorTintColor = .clear
        currentPageIndicatorTintColor = .clear
        clipsToBounds = false
    }

    private func updateDots() {

        for (index, subview) in subviews.enumerated() {
            let imageView: UIImageView
            if let existingImageview = getImageView(forSubview: subview) {
                imageView = existingImageview
            } else {
                imageView = UIImageView(image: otherPagesImage)
                imageView.contentMode = .scaleToFill
                imageView.center = subview.center
                subview.addSubview(imageView)
                //subview.clipsToBounds = false

                imageView.translatesAutoresizingMaskIntoConstraints = false

                imageView.centerXAnchor.constraint(equalTo: subview.centerXAnchor).isActive = true
                imageView.centerYAnchor.constraint(equalTo: subview.centerYAnchor).isActive = true
                imageView.heightAnchor.constraint(equalTo: subview.heightAnchor, multiplier: 0.35).isActive = true

                imageView.widthAnchor.constraint(equalTo: subview.widthAnchor, multiplier: 1.5).isActive = true
            }
            imageView.image = currentPage == index ? currentPageImage : otherPagesImage
        }
    }

    private func getImageView(forSubview view: UIView) -> UIImageView? {
        if let imageView = view as? UIImageView {
            return imageView
        } else {
            let view = view.subviews.first { (view) -> Bool in
                return view is UIImageView
                } as? UIImageView

            return view
        }
    }
}
