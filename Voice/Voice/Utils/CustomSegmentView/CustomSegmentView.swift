//
//  TransitionSegmentView.swift
//  TransitionSegment
//
//  Created by wuliupai on 16/9/21.
//  Copyright © 2016年 wuliu. All rights reserved.
//

import UIKit

let widthAddition:CGFloat = 20.0
let heightAddtion:CGFloat = 16.0
let cornerRadius:CGFloat = 0.0

let tagAddition = 100

struct SegmentConfigure {
    
    var textSelectColor:UIColor
    
    var highlightColor:UIColor
    
    var titles:[String]
}


class CustomSegmentView: UIView {


    var configure: SegmentConfigure!{
        didSet{
            self.configUI()
        }
    }
    
    typealias SegmentClosureType = (Int)->Void
    
    //闭包回调方法
    public var scrollClosure:SegmentClosureType?
    
    //字体非选中状态颜色
    private var textNorColor:UIColor = Global.grayColor81878E
    
    //字体大小
    private var textFont:CGFloat = 14.0
    
    //底部scrollview容器
    private var bottomContainer:UIScrollView?
    
    //高亮区域
    private var highlightView: UIView?
    private var highlightLineView: UIView?
    
    //顶部scrollView容器
    private var topContainer:UIScrollView?

    var scrollViewFrame: CGRect?

    lazy var lineHeight: CGFloat = {
        return 3 / 45 * (highlightView?.frame.height)!
    }()

    init(frame: CGRect,configure:SegmentConfigure) {
        super.init(frame:frame)
        self.configure = configure
        self.configUI()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //self.configUI()

        //fatalError("init(coder:) has not been implemented")
    }

    //初始化UI
    func configUI() {
        
        //let rect = CGRect(x:0,y:0,width:frame.width,height:frame.height)

        guard let frame = scrollViewFrame else {
            return
        }

        let rect = CGRect(x: frame.minX, y: frame.minY, width: frame.width, height:frame.height)
        
        bottomContainer = UIScrollView.init(frame:rect)
        topContainer = UIScrollView.init(frame: rect)
        highlightView = UIView.init()
        highlightView?.layer.masksToBounds = false

        highlightLineView  = UIView.init()
        highlightView?.addSubview(highlightLineView!)
        
        self.configScrollView()
        
        self.addSubview(bottomContainer!)
        bottomContainer?.addSubview(highlightView!)
        highlightView?.addSubview(topContainer!)
        
    }

    //初始化scrollview容器
    func configScrollView()  {
        for view in (bottomContainer?.subviews)! {
            if view.isEqual(highlightView) {
                return
            }else{
                view.removeFromSuperview()
            }
        }

        for view in (topContainer?.subviews)! {
            view.removeFromSuperview()
        }

        highlightLineView?.backgroundColor = Global.yellowColorF8BF00
        highlightView?.backgroundColor = configure.highlightColor
        //----
        self.createBottomLabels(scrollView: bottomContainer!, titleArray: configure.titles)
        self.createBottomLabels(scrollView: topContainer!, titleArray: configure.titles)
        //----

//        self.createBottomLabels(scrollView: bottomContainer!, titleArray: configure.titles,isHighlight:false)
//        self.createBottomLabels(scrollView: topContainer!, titleArray: configure.titles,isHighlight:true)
    }

    //对scrollview容器进行设置
    func createBottomLabels(scrollView: UIScrollView, titleArray: [String]) {
        var firstX:CGFloat = 0
        scrollView.showsHorizontalScrollIndicator = false

        for index in 0..<titleArray.count{

            //-----
            let titleGroup = titleArray[index].components(separatedBy: "\n")


            let text = NSMutableAttributedString.init(string: titleArray[index])

            var attributed: [NSAttributedString.Key: Any] = [:]
            let chineseFont = UIFont(name: "NotoSerifCJKtc-Medium", size: CGFloat(16).adaptiveForHeight)
            attributed[NSAttributedString.Key.font] = chineseFont
            attributed[NSAttributedString.Key.foregroundColor] = UIColor.red
            text.addAttributes(attributed, range: NSRange(location: 0, length: titleGroup[0].count))

            var attributed1: [NSAttributedString.Key: Any] = [:]
            let englishFont = UIFont(name: "NotoSerifCJKtc-Medium", size: CGFloat(8).adaptiveForHeight)
            attributed1[NSAttributedString.Key.font] = englishFont
            attributed1[NSAttributedString.Key.foregroundColor] = UIColor.green
            text.addAttributes(attributed1, range: NSRange(location: titleGroup[0].count, length: titleGroup[1].count + 1))

            let chineseFontAttributed = [NSAttributedString.Key.font: chineseFont]
            let chineseFontSize = titleGroup[0].size(withAttributes: chineseFontAttributed as [NSAttributedString.Key : Any])


            let englishFontAttributed = [NSAttributedString.Key.font: englishFont]
            let englishFontSize = titleGroup[1].size(withAttributes: englishFontAttributed as [NSAttributedString.Key : Any])

            var titleWidth: CGFloat = 0
            if chineseFontSize.width > englishFontSize.width {
                titleWidth = chineseFontSize.width
            } else {
                titleWidth = englishFontSize.width
            }


            //-----



            let title: NSString = titleArray[index] as NSString

            let dict = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: textFont)]
            //富文本自己算label宽度和高度
            let itemWidth = titleWidth + widthAddition
//            let itemWidth = title.size(withAttributes: dict).width + widthAddition

            let label = UILabel.init()
            label.frame = CGRect(x: firstX, y: 0, width: itemWidth, height: self.frame.height)
            //label.text = title as String
            //-----
            label.attributedText = text
            //------
            label.numberOfLines = 2
            label.textAlignment = NSTextAlignment.center

            if scrollView == topContainer {

                //label.font = UIFont.systemFont(ofSize: textFont + 1)
                label.textColor = configure.textSelectColor

            }else{

                //label.font = UIFont.systemFont(ofSize:textFont)
                label.textColor = textNorColor
                label.isUserInteractionEnabled = true
                label.tag = index + tagAddition;

                let gesture = UITapGestureRecognizer.init(target: self, action: #selector(tap))
                label.addGestureRecognizer(gesture)


                if index == 0 {
                    highlightView?.frame = label.frame
                    setHighlightLineViewFrameByHighlightView()

                    self.clipView(view: highlightView!)
                }

            }

            firstX += itemWidth
            scrollView.contentSize = CGSize(width:firstX,height:0)

            if scrollView == bottomContainer {
                let view = UIView()
                view.backgroundColor = Global.grayColorB2

                let highlightViewFrame = highlightView?.frame

                view.frame = CGRect(x: 0, y: highlightViewFrame!.height - lineHeight, width: scrollView.width, height: lineHeight)

//                let lineHeight = label.frame.height * 0.05
//                view.frame = CGRect(x: 0, y: label.frame.height - lineHeight, width: label.frame.width, height: lineHeight)

                scrollView.addSubview(view)
            }

            scrollView.addSubview(label)
        }
    }

//    //对scrollview容器进行设置
//    func createBottomLabels(scrollView:UIScrollView,titleArray:[String],isHighlight:Bool) {
//        var firstX:CGFloat = 0
//        scrollView.showsHorizontalScrollIndicator = false
//
//        for index in 0..<titleArray.count{
//
//            let title:NSString = titleArray[index] as NSString
//
//            let dict = [NSAttributedString.Key.font:UIFont.systemFont(ofSize: textFont)]
//            //富文本自己算label宽度和高度
//            let itemWidth = title.size(withAttributes: dict).width + widthAddition
//
//            let label = UILabel.init()
//            label.frame = CGRect(x:firstX,y:0,width:itemWidth,height:self.frame.height)
//            label.text = title as String
//            label.textAlignment = NSTextAlignment.center
//
//            if isHighlight {
//
//                label.font = UIFont.systemFont(ofSize:textFont+1)
//                label.textColor = configure.textSelectColor
//
//            }else{
//
//                label.font = UIFont.systemFont(ofSize:textFont)
//                label.textColor = textNorColor
//                label.isUserInteractionEnabled = true
//                label.tag = index + tagAddition;
//
//                let gesture = UITapGestureRecognizer.init(target: self, action: #selector(tap))
//                label.addGestureRecognizer(gesture)
//
//
//                if index == 0 {
//                    highlightView?.frame = label.frame
//                    self.clipView(view: highlightView!)
//                }
//
//            }
//
//            firstX += itemWidth
//            scrollView.contentSize = CGSize(width:firstX,height:0)
//            scrollView.addSubview(label)
//        }
//    }

    //设置闭包
    func setScrollClosure(tempClosure:@escaping SegmentClosureType) {
        
        self.scrollClosure = tempClosure
        
    }
    
    //点击手势方法
    @objc func tap(sender:UITapGestureRecognizer) {
        
        let item:UILabel = sender.view as! UILabel
        let index = item.tag
        
        self.scrollClosure!(Int(index-100))
        
    }
    
    //scrollViewDidScroll调用
    func segmentWillMove(point:CGPoint) {
        guard let frame = scrollViewFrame else {
            return
        }

        let index = Int(point.x/frame.width)
        let remainder = point.x/frame.width - CGFloat(index)
        
        for view in (bottomContainer?.subviews)! {
            if index == (view.tag - tagAddition) {
                
                // 判断bottomContainer 是否需要移动
                var offsetx = view.center.x - Global.screenWidth / 2
                
                let offsetMax = (bottomContainer?.contentSize.width)! - Global.screenWidth
                
                if offsetx < 0 {
                    offsetx = 0
                } else if offsetx > offsetMax {
                    offsetx = offsetMax
                }
                let bottomPoint = CGPoint(x:offsetx,y:0)
                
                bottomContainer?.setContentOffset(bottomPoint, animated: false)
                
                //调整高亮区域的frame
                highlightView?.frame = view.frame
                setHighlightLineViewFrameByHighlightView()

                highlightView?.x = view.x + view.frame.width*remainder


                //获取下一个label的宽度
                let nextView = bottomContainer?.subviews[index+1]
                highlightView?.width = (nextView?.width)!*remainder + view.width * (1-remainder)
                
                
                //裁剪高亮区域
                self.clipView(view: highlightView!)
                
                let topPoint = CGPoint(x:((highlightView?.frame.minX)!), y:0)
                
                //移动topContainer
                topContainer?.setContentOffset(topPoint, animated: false)
            }
        }
    }
    
    
    //scrollViewDidEndScrollingAnimation方法调用
    func segmentDidEndMove(point:CGPoint)  {
        //四舍五入
        let index = lroundf(Float(point.x/Global.screenWidth))
        
        for view in (bottomContainer?.subviews)! {
            
            if index == (view.tag - 100) {
                
                //调整高亮区域的frame
                highlightView?.frame = view.frame
//                highlightLineView?.frame = CGRect(x: 0, y: view.frame.height - view.frame.height * 0.2, width: view.frame.width, height: view.frame.height * 0.2)
                setHighlightLineViewFrameByHighlightView()

                //移动topContainer
                topContainer?.contentOffset = CGPoint(x:((highlightView?.frame.minX)!), y:0)
                
                
                // 判断bottomContainer 是否需要移动
                var offsetx = view.centerX - Global.screenWidth/2
                
                let offsetMax = (bottomContainer?.contentSize.width)! - Global.screenWidth
                
                if offsetx < 0 {
                    offsetx = 0
                }else if offsetx > offsetMax{
                    
                    offsetx = offsetMax
                }
                let bottomPoint = CGPoint(x:offsetx,y:0)
                
                bottomContainer?.setContentOffset(bottomPoint, animated: true)
                
            }
        }
        
    }
    
    //切割高亮区域
    func clipView(view:UIView)  {
        
//        let rect = CGRect(x:widthAddition/4,y:heightAddtion/4,width:view.width-widthAddition/2,height:view.height-heightAddtion/2)

        let rect = CGRect(x: 0, y: 0, width: view.width, height: view.height)

        let bezierPath = UIBezierPath.init(roundedRect: rect, cornerRadius: cornerRadius)
        let maskLayer = CAShapeLayer()
        maskLayer.path = bezierPath.cgPath
        view.layer.mask = maskLayer
    }

    private func setHighlightLineViewFrameByHighlightView() {
        //let lineHeight = targetView.frame.height * 0.05
        let highlightViewFrame = highlightView?.frame

        highlightLineView?.frame = CGRect(x: 0, y: highlightViewFrame!.height - lineHeight, width: highlightViewFrame!.width, height: lineHeight)
    }
}
