//
//  CustomSlider.swift
//  Voice
//
//  Created by kasim on 2019/1/4.
//  Copyright © 2019 Shiefu. All rights reserved.
//

import UIKit

class CustomSlider: UISlider {

    @IBInspectable var trackHeight: CGFloat = 4

    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(origin: bounds.origin, size: CGSize(width: bounds.width, height: trackHeight / 667 * Global.screenHeight))
    }

}
