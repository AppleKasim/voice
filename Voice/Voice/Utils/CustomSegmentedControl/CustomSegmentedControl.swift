//
//  CustomSegmentedControl.swift
//  Voice
//
//  Created by kasim on 2018/12/10.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

@IBDesignable
class CustomSegmentedControl: UIView {

    @IBOutlet weak var contentView: UIView!

    @IBOutlet weak var lineWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var lineLeadingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var lineBackgroundView: UIView! {
        didSet {
            lineBackgroundView.backgroundColor = Global.blackColor00
        }
    }
    
    @IBOutlet weak var lineView: UIView! {
        didSet {
            lineView.backgroundColor = Global.yellowColorF8BF00
        }
    }

    @IBOutlet weak var segmentedControl: UISegmentedControl!

    var titles: [String] = [] {
        didSet {
            segmentedControl.removeBorder()

            segmentedControl.customUnderlineStyle(titles: titles, withFontStyle: fontStyle, disposeBag: disposeBag)

            segmentedControl.rx.selectedSegmentIndex.asObservable()
                .subscribe(onNext: { [weak self] in
                    guard let strongSelf = self else { return }

                    strongSelf.lineLeadingConstraint.constant = strongSelf.itemWidth * CGFloat($0)
                    UIView.animate(withDuration: 0.1) {
                        strongSelf.layoutIfNeeded()
                    }
                })
                .disposed(by: disposeBag)
        }
    }

    let fontStyle = SegmentedControlFontStyle()
    var itemWidth: CGFloat = 0

    let disposeBag = DisposeBag()

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initFromXIB()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        initFromXIB()
    }

    func initFromXIB() {

        let bundle = Bundle(for: type(of: self))

        let nib = UINib(nibName: "CustomSegmentedControl", bundle: bundle)

        guard let nibView = nib.instantiate(withOwner: self, options: nil)[0] as? UIView else {
            return
        }

        contentView = nibView
        contentView.frame = bounds
        self.addSubview(contentView)

    }

    func updateLineWidth() {
        self.setNeedsLayout()
        self.layoutIfNeeded()
        
        let lineWidth = self.frame.width / CGFloat(self.segmentedControl.numberOfSegments)
        self.lineWidthConstraint.constant = lineWidth
        self.itemWidth = lineWidth


    }
}
