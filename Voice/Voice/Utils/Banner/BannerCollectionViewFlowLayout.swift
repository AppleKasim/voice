//
//  BannerCollectionViewFlowLayout.swift
//  TableViewHeaderBannerTest
//
//  Created by kasim on 2018/10/26.
//  Copyright © 2018 kasim. All rights reserved.
//

import UIKit

class BannerCollectionViewFlowLayout: UICollectionViewFlowLayout {
    //prepare方法在collectionView第一次布局的时候被调用
    override func prepare() {
        super.prepare()//必须写
        collectionView?.backgroundColor = UIColor.white
        //通过打印可以看到此时collectionView的frame就是我们前面设置的frame
        //print("self.collectionView:\(String(describing: self.collectionView))")
        // 通过collectionView 的属性布局cell
        self.itemSize = (self.collectionView?.bounds.size)!
        self.minimumInteritemSpacing = 0 //cell之间最小间距
        self.minimumLineSpacing = 0 //最小行间距
        self.scrollDirection = .horizontal;

        self.collectionView?.bounces = false //禁用弹簧效果
        self.collectionView?.isPagingEnabled = true //分页
        self.collectionView?.showsHorizontalScrollIndicator = false
        self.collectionView?.showsVerticalScrollIndicator = false
    }
}
