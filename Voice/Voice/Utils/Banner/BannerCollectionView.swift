//
//  BannerCollectionView.swift
//  TableViewHeaderBannerTest
//
//  Created by kasim on 2018/10/26.
//  Copyright © 2018 kasim. All rights reserved.
//

import UIKit
import SDWebImage
import SnapKit

let placeholder:String = "ic_bannerPlace"
@objc protocol BannerScrollViewDelegate: NSObjectProtocol {
    //点击代理方法
    @objc optional func scrollView(_ scrollView: BannerCollectionView, didSelectRowAt index: NSInteger)
}

fileprivate let collectionViewCellId = "collectionViewCellId"
class BannerCollectionView: UICollectionView {

    //代理
    weak var bannerScrollViewDelegate: BannerScrollViewDelegate?

    //分页指示器页码颜色
    var pageControlColor:UIColor?

    //分页指示器当前页颜色
    var currentPageControlColor:UIColor?

    //分页指示器位置
    var pageControlPoint:CGPoint?

    //分页指示器
    weak var pageControl: UIPageControl?

    //自动滚动时间默认为3.0
    var autoScrollDelay:TimeInterval = 3 {
        didSet{
            removeTimer()
            setUpTimer()
        }
    }

    //图片是否来自网络,默认是
    var isFromNet:Bool = true

    //占位图
    var placeholderImage:String = placeholder

    //设置图片资源url字符串。
    var imageUrls: [String] = [] {
        didSet{
            pageControl?.numberOfPages = imageUrls.count
            self.reloadData()
        }
    }
    fileprivate var itemCount:NSInteger?//cellNum
    fileprivate var timer:Timer?//定时器

    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: BannerCollectionViewFlowLayout())
        self.backgroundColor = UIColor.white
        self.dataSource = self
        self.delegate = self
        self.register(BannerCollectionViewCell.self, forCellWithReuseIdentifier: collectionViewCellId)

        setUpTimer()
        //在collectionView加载完成后默认滚动到索引为imgUrls.count的位置，这样cell就可以向左或右滚动
        DispatchQueue.main.async {
            //注意：在轮播器视图添加到控制器的view上以后，这样是为了将分页指示器添加到self.superview上(如果将分页指示器直接添加到collectionView上的话，指示器将不能正常显示)
            //self.setUpPageControl()
            let indexpath = NSIndexPath.init(row: self.imageUrls.count, section: 0)
            //滚动位置
            self.scrollToItem(at: indexpath as IndexPath, at: .left, animated: false)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    deinit {
        removeTimer()
    }
}

extension BannerCollectionView: UICollectionViewDelegate {
    //MARK: - UIScrollViewDelegate
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        //当前的索引
        var offset:NSInteger = NSInteger(scrollView.contentOffset.x / scrollView.bounds.size.width)

        //第0页时，跳到索引imgUrls.count位置；最后一页时，跳到索引imgUrls.count-1位置
        if offset == 0 || offset == (self.numberOfItems(inSection: 0) - 1) {
            if offset == 0 {
                offset = imageUrls.count
            }else {
                offset = imageUrls.count - 1
            }
        }
        //跳转方式一：
        //        let indexpath = NSIndexPath.init(row: offset, section: 0)
        //        //滚动位置
        //        self.scrollToItem(at: indexpath as IndexPath, at: .left, animated: false)
        //跳转方式二：
        scrollView.contentOffset = CGPoint.init(x: CGFloat(offset) * scrollView.bounds.size.width, y: 0)
    }

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        removeTimer()
    }

    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        setUpTimer()
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // 设置分页指示器索引
        let currentPage:NSInteger = NSInteger(scrollView.contentOffset.x / scrollView.bounds.size.width)
        let currentPageIndex = imageUrls.count > 0 ? currentPage % imageUrls.count : 0
        self.pageControl?.currentPage = currentPageIndex
    }

    //添加定时器
    @objc private func setUpTimer(){
        timer = Timer.init(timeInterval: autoScrollDelay, target: self, selector: #selector(autoScroll), userInfo: nil, repeats: true)
        RunLoop.current.add(timer!, forMode: .default)
    }
    //移除定时器
    @objc private func removeTimer(){
        if (timer != nil) {
            timer?.invalidate()
            timer = nil
        }
    }
    //自动滚动
    @objc private func autoScroll(){
        //当前的索引
        var offset:NSInteger = NSInteger(self.contentOffset.x / self.bounds.size.width)

        //第0页时，跳到索引imgUrls.count位置；最后一页时，跳到索引imgUrls.count-1位置
        if offset == 0 || offset == (itemCount! - 1) {
            if offset == 0 {
                offset = imageUrls.count
            }else {
                offset = imageUrls.count - 1
            }

            self.contentOffset = CGPoint.init(x: CGFloat(offset) * self.bounds.size.width, y: 0)
            //再滚到下一页
            self.setContentOffset(CGPoint.init(x: CGFloat(offset + 1) * self.bounds.size.width, y: 0), animated: true)
        }else{
            //直接滚到下一页
            self.setContentOffset(CGPoint.init(x: CGFloat(offset + 1) * self.bounds.size.width, y: 0), animated: true)
        }
    }
}

extension BannerCollectionView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if imageUrls.count != 0 {
            bannerScrollViewDelegate?.scrollView!(self, didSelectRowAt: indexPath.row % imageUrls.count)
        }else{
            print("图片数组为空！")
        }
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //itemNum 设置为图片数组的n倍数(n>1)，当未设置imgUrls时，随便返回一个数6
        return imageUrls.count > 0 ? imageUrls.count*1000 : 6
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: collectionViewCellId, for: indexPath) as! BannerCollectionViewCell
        cell.backgroundColor = UIColor.lightGray

        if imageUrls.count > 0 {
            let urlStr = self.imageUrls[indexPath.row % imageUrls.count]
            if isFromNet {
                let url:URL = URL.init(string: urlStr)!
                cell.imageView?.sd_setImage(with: url, placeholderImage: UIImage.init(named: placeholderImage), options: SDWebImageOptions.refreshCached)
            }else{
                cell.imageView?.image = UIImage.init(named: urlStr)
            }
        }

        itemCount = self.numberOfItems(inSection: 0)
        return cell
    }
}
