//
//  BannerCollectionViewCell.swift
//  TableViewHeaderBannerTest
//
//  Created by kasim on 2018/10/26.
//  Copyright © 2018 kasim. All rights reserved.
//

import UIKit

class BannerCollectionViewCell: UICollectionViewCell {
    var imageView:UIImageView?
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.clipsToBounds = true
        //通过打印可以看到此时cell的frame就是我们在flowLayout中设置的结果
        //print("self.collectionView:\(String(describing: self))")
        imageView = UIImageView.init(frame: self.bounds)
        imageView?.image = UIImage.init(named: placeholder)
        imageView?.contentMode = .scaleAspectFill
        contentView.addSubview(imageView!)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
