//
//  CustomTabBar.swift
//  Voice
//
//  Created by Shiefu on 2018/9/28.
//  Copyright © 2018年 Shiefu. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class CustomTabBar: UITabBar {

    private var backgroundImageView = UIImageView()
    private var middleButton = UIButton()
    private var outerFrameView = UIView()
    private var backgroundView = UIView()

    private let disposeBag = DisposeBag()

    override func awakeFromNib() {
        super.awakeFromNib()
        setupOuterFrameView()
        setupBackgroundView()
        setupBackgroundImageView()
        setupMiddleButton()
    }

    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        if self.isHidden {
            return super.hitTest(point, with: event)
        }

        let from = point
        let toPoint = middleButton.center

        // swiftlint:disable line_length
        return sqrt((from.x - toPoint.x) * (from.x - toPoint.x) + (from.y - toPoint.y) * (from.y - toPoint.y)) <= 39 ? middleButton : super.hitTest(point, with: event)
        // swiftlint:enable line_length
    }

    func setupBackgroundImageView() {
        PlayManager.share().playIntroduction
            .asDriver()
            .drive(onNext: { [weak self] introduction in
                guard let strongSelf = self,
                    let coverUrl = introduction?.cover else { return }
                let url = URL(string: coverUrl)
                strongSelf.backgroundImageView.sd_setImage(with: url, completed: nil)
            })
            .disposed(by: disposeBag)

        backgroundImageView.frame.size = CGSize(width: 60, height: 60)
        backgroundImageView.backgroundColor = .red
        backgroundImageView.layer.cornerRadius = 30
        backgroundImageView.layer.masksToBounds = true
        backgroundImageView.center = CGPoint(x: UIScreen.main.bounds.width / 2, y: 12)
        addSubview(backgroundImageView)

    }

    func setupBackgroundView() {
        backgroundView.frame.size = CGSize(width: 62, height: 62)
        backgroundView.backgroundColor = .white
        backgroundView.layer.cornerRadius = 31
        backgroundView.layer.masksToBounds = true
        backgroundView.center = CGPoint(x: UIScreen.main.bounds.width / 2, y: 12)
        addSubview(backgroundView)
    }

    func setupOuterFrameView() {
        outerFrameView.frame.size = CGSize(width: 70, height: 70)
        outerFrameView.backgroundColor = .black
        outerFrameView.layer.cornerRadius = 35
        outerFrameView.layer.masksToBounds = true
        outerFrameView.center = CGPoint(x: UIScreen.main.bounds.width / 2, y: 12)
        addSubview(outerFrameView)
    }

    func setupMiddleButton() {
        middleButton.frame.size = CGSize(width: 62, height: 62)
        //middleButton.backgroundColor = .blue
        middleButton.layer.cornerRadius = 31
        middleButton.layer.masksToBounds = true
        middleButton.setImage(UIImage(named: "tabbar_play"), for: .normal)
        middleButton.center = CGPoint(x: UIScreen.main.bounds.width / 2, y: 12)
        middleButton.addTarget(self, action: #selector(toPlayPage), for: .touchUpInside)
        addSubview(middleButton)
    }

    @objc func toPlayPage() {
        let storyboard = UIStoryboard(name: "PlayStoryboard", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "PlayStoryboard") as? PlayViewController else {
            return
        }

        let tabBar = getTabBarController()
        viewController.transitioningDelegate = tabBar
        viewController.interactor = tabBar.interactor

        tabBar.present(viewController, animated: true, completion: nil)
    }

    func getTabBarController() -> CustomTabBarController {
        var next: UIResponder?
        next = self.next

        while next != nil {

            if next as? CustomTabBarController != nil {
                guard let tabbar = next as? CustomTabBarController else {
                    return CustomTabBarController()
                }
                return tabbar
            } else {
                next = next?.next
            }
        }

        return CustomTabBarController()
    }
}
