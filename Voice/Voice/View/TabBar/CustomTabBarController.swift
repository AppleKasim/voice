//
//  CustomTabBarController.swift
//  Voice
//
//  Created by kasim on 2018/11/29.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit

class CustomTabBarController: UITabBarController {

    let interactor = Interactor()

    override func viewDidLoad() {
        super.viewDidLoad()

        Global.tabBarHeight = self.tabBar.height

        guard let viewControllers = self.viewControllers else {
            return
        }

        for viewControllers in viewControllers
            where UIDevice.current.userInterfaceIdiom == .phone {

            let offset: CGFloat = 6.0
            let imageInset = UIEdgeInsets(top: offset, left: 0, bottom: -offset, right: 0)
            viewControllers.tabBarItem.imageInsets = imageInset
        }

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if let arrayOfTabBarItems = self.tabBar.items as AnyObject as? NSArray,
            let tabBarItem = arrayOfTabBarItems[2] as? UITabBarItem {
            tabBarItem.isEnabled = false
        }
    }

}

extension CustomTabBarController: UIViewControllerTransitioningDelegate {
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return DismissAnimator()
    }

    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return interactor.hasStarted ? interactor : nil
    }
}
