//
//  MySetHomeViewController.swift
//  Voice
//
//  Created by kasim on 2018/11/25.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxDataSources

class MySetHomeViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    // swiftlint:disable line_length
    private lazy var dataSource = RxTableViewSectionedReloadDataSource<SectionOfMySetHome>(configureCell: configureCell)

    private lazy var configureCell: RxTableViewSectionedReloadDataSource<SectionOfMySetHome>.ConfigureCell = { [weak self] (dataSource, tableView, indexPath, data) in

        guard let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "MySetHomeCell") as? MySetHomeCell else {
            return UITableViewCell()
        }

        tableViewCell.update(model: data)

        return tableViewCell
    }
    // swiftlint:enable line_length

    let viewModel = MySetHomeViewModel()
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.rx.setDelegate(self)
            .disposed(by: disposeBag)

        viewModel.items
            .asDriver()
            .drive(tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)

        viewModel.updateItem()

        tableView.rx.itemSelected
            .subscribe(onNext: { [weak self] indexPath in
                guard let strongSelf = self else { return }

                switch indexPath.row {
                case 0:
                    strongSelf.performSegue(withIdentifier: "ToAlarmClock", sender: indexPath.row)
                case 2:
                    strongSelf.performSegue(withIdentifier: "ToMyFontSegue", sender: indexPath.row)
                default:
                    break
                }
            })
            .disposed(by: disposeBag)
    }

}

extension MySetHomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50 / 667 * Global.screenHeight
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 2.5 / 667 * Global.screenHeight
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 2.5 / 667 * Global.screenHeight
    }
}
