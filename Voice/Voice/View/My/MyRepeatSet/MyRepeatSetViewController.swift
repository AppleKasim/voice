//
//  MyRepeatSetViewController.swift
//  Voice
//
//  Created by kasim on 2018/11/27.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxDataSources

class MyRepeatSetViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    // swiftlint:disable line_length
    private lazy var dataSource = RxTableViewSectionedReloadDataSource<SingleSectionModelMy<MyTitleAndDot>>(configureCell: configureCell)

    private lazy var configureCell: RxTableViewSectionedReloadDataSource<SingleSectionModelMy<MyTitleAndDot>>.ConfigureCell = { [weak self] (dataSource, tableView, indexPath, data) in

        guard let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "MyRepeatSetCell") as? MyRepeatSetCell else {
            return UITableViewCell()
        }
        
        tableViewCell.update(model: data)

        return tableViewCell

    }
    // swiftlint:enable line_length

    let viewModel = MyTitleAndDotViewModel()
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.items
            .asDriver()
            .drive(tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)

        viewModel.updateItemForRepeatSet()

        tableView.rx.setDelegate(self)
            .disposed(by: disposeBag)
    }

}

extension MyRepeatSetViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50 / 667 * Global.screenHeight
    }
}
