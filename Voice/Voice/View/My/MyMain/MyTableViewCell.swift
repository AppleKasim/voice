//
//  MyTableViewCell.swift
//  Voice
//
//  Created by kasim on 2018/11/11.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit

class MyTableViewCell: UITableViewCell {

    @IBOutlet weak var leftLabel: UILabel!
    @IBOutlet weak var rightImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func update(title: String, image: UIImage) {
        leftLabel.text = title
        rightImageView.image = image
    }

}
