//
//  MyHeaderCell.swift
//  Voice
//
//  Created by kasim on 2018/11/11.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class MyHeaderCell: UITableViewCell {

    @IBOutlet weak var lightImageView: UIImageView!

    @IBOutlet weak var checkInButton: UIButton! {
        didSet {
            checkInButton.setTitle("有獎簽到", for: .normal)
            checkInButton.setCornerRadiusBorder(forMultiplier: 25 / 667 * Global.screenHeight)
        }
    }

    @IBOutlet weak var signOutButton: UIButton! {
        didSet {
            signOutButton.setTitle("登出", for: .normal)
            signOutButton.setTitleColor(.white, for: .normal)
            signOutButton.backgroundColor = .black
            signOutButton.layer.cornerRadius = (25 / 667 * Global.screenHeight) / 2.0
        }
    }

    @IBOutlet weak var profileButton: UIButton! {
        didSet {
            profileButton.contentMode = .scaleAspectFit
            profileButton.layer.cornerRadius = (65 / 667 * Global.screenHeight) / 2.0
        }
    }

    @IBOutlet weak var lineView: UIView! {
        didSet {
            lineView.backgroundColor = Global.grayColorEB
        }
    }

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var currenciesLabel: UILabel!
    @IBOutlet weak var arrowButton: UIButton!

    let disposeBag = DisposeBag()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
