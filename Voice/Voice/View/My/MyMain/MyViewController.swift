//
//  MyViewController.swift
//  Voice
//
//  Created by kasim on 2018/11/11.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxDataSources

class MyViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.bounces = false
        }
    }

    var isLogin = true

    // swiftlint:disable line_length
    private lazy var dataSource = RxTableViewSectionedReloadDataSource<SectionOfMy>(configureCell: configureCell)

    private lazy var configureCell: RxTableViewSectionedReloadDataSource<SectionOfMy>.ConfigureCell = { [weak self] (dataSource, tableView, indexPath, data) in

        guard let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "MyTableViewCell") as? MyTableViewCell else {
            return UITableViewCell()
        }

        tableViewCell.update(title: data.title, image: UIImage(named: data.imageName)!)

        return tableViewCell
    }
    // swiftlint:enable line_length

    let viewModel = MyViewModel()
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.rx.setDelegate(self)
            .disposed(by: disposeBag)

        viewModel.items
            .bind(to: tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)

        tableView.rx.itemSelected
            .subscribe(onNext: { [weak self] indexPath in
                guard let strongSelf = self else { return }

                switch indexPath.row {
                case 0, 1, 2:
                    strongSelf.performSegue(withIdentifier: "ToRecordSegue", sender: indexPath.row)
                case 3:
                    strongSelf.performSegue(withIdentifier: "ToNotificationSegue", sender: indexPath.row)
                case 5:
                    strongSelf.performSegue(withIdentifier: "ToSetHomeSegue", sender: indexPath.row)
                default:
                    break
                }
            })
            .disposed(by: disposeBag)

    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.navigationController?.setNavigationBarHidden(true, animated: true)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ToRecordSegue" {
            guard let viewController = segue.destination as? RecordViewController,
            let index = sender as? Int else {
                return
            }

            viewController.currentPageIndex.accept(index)
        } else if segue.identifier == "ToNotificationSegue" {
            guard let viewController = segue.destination as? MyNotificationViewController else {
                    return
            }
        }
    }

}

extension MyViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50 / 667 * Global.screenHeight
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 180 / 667 * Global.screenHeight
    }

    // swiftlint:disable line_length
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let headerCell = tableView.dequeueReusableCell(withIdentifier: "MyHeaderCell") as? MyHeaderCell else {
            return nil
        }

        viewModel.headerDriver
            .drive(onNext: { myHeader in
                headerCell.nameLabel.text = myHeader.name
                headerCell.currenciesLabel.text = myHeader.currencies
            })
            .disposed(by: disposeBag)

        headerCell.arrowButton.rx.tap
            .asObservable()
            .subscribe(onNext: { [weak self] in
                guard let strongSelf = self else {
                    return
                }

                if strongSelf.isLogin {
                    strongSelf.performSegue(withIdentifier: "ToMyInformationSegue", sender: nil)
                } else {
                    strongSelf.performSegue(withIdentifier: "loginSegue", sender: nil)
                }

            }).disposed(by: headerCell.disposeBag)

        return headerCell
    }
    // swiftlint:enable line_length
}
