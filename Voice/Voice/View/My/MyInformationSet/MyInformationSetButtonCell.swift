//
//  MyInformationSetButtonCell.swift
//  Voice
//
//  Created by kasim on 2018/11/27.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class MyInformationSetButtonCell: UITableViewCell {

    @IBOutlet weak var sendButton: UIButton! {
        didSet {
            sendButton.setTitleColor(Global.yellowColorF8BF00, for: .normal)
            sendButton.backgroundColor = .black
            sendButton.layer.cornerRadius = (45 / 667 * Global.screenHeight) / 10
            sendButton.clipsToBounds = true
        }
    }
    let disposeBag = DisposeBag()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
