//
//  MyInformationSetViewController.swift
//  Voice
//
//  Created by kasim on 2018/11/27.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxDataSources

enum InformationSetMode {
    case nickname
    case email
    case gender
    case city
    case changePassword
    case signature

    func updateItem(for viewModel: MyInformationSetViewModel) -> MyInformationSetViewModel {
        var viewModel = viewModel
        switch self {
        case .nickname:
            viewModel.updateItemForNickName()
        case .email:
            viewModel.updateItemForEmail()
        case .gender:
            viewModel.updateItemForGender()
        case .city:
            viewModel.updateItemForCity()
        case .changePassword:
            viewModel.updateItemForChangePassword()
        case .signature:
            viewModel.updateItemForSignature()
        }
        return viewModel
    }
}

class MyInformationSetViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    // swiftlint:disable line_length
    private lazy var dataSource = RxTableViewSectionedReloadDataSource<SingleSectionModelMy<MyInformationSetModel>>(configureCell: configureCell)

    private lazy var configureCell: RxTableViewSectionedReloadDataSource<SingleSectionModelMy<MyInformationSetModel>>.ConfigureCell = { [weak self] (dataSource, tableView, indexPath, data) in
        guard let strongSelf = self else { return UITableViewCell() }

        switch strongSelf.informationSetMode {
        case .nickname:
            guard let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "MyInformationSetTextFieldCell") as? MyInformationSetTextFieldCell else {
                return UITableViewCell()
            }
            tableViewCell.update(model: data)

            tableViewCell.textField.rx.text.orEmpty
                .bind(to: strongSelf.viewModel.nickName)
                .disposed(by: tableViewCell.disposeBag)

            return tableViewCell
        case .email, .city, .signature:
            guard let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "MyInformationSetTextFieldCell") as? MyInformationSetTextFieldCell else {
                return UITableViewCell()
            }
            tableViewCell.update(model: data)
            return tableViewCell

        case .changePassword:
            guard let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "MyInformationChangePasswordCell") as? MyInformationChangePasswordCell else {
                return UITableViewCell()
            }
            tableViewCell.update(model: data)
            return tableViewCell
        case .gender:
            guard let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "MyInformationSetDotCell") as? MyInformationSetDotCell else {
                return UITableViewCell()
            }
            tableViewCell.update(model: data)
            return tableViewCell
        }

    }
    // swiftlint:enable line_length

    var informationSetMode: InformationSetMode = .nickname
    var viewModel = MyInformationSetViewModel()
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.rx.setDelegate(self)
            .disposed(by: disposeBag)

        viewModel.items
            .asDriver()
            .drive(tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)

        viewModel = informationSetMode.updateItem(for: viewModel)
    }
}

extension MyInformationSetViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch informationSetMode {
        case .nickname, .email, .city, .changePassword, .signature:
            return 90 / 667 * Global.screenHeight
        case .changePassword:
            return 87 / 667 * Global.screenHeight
        case .gender:
            return 50 / 667 * Global.screenHeight
        }
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        guard let footerCell = tableView.dequeueReusableCell(withIdentifier: "MyInformationSetButtonCell") as? MyInformationSetButtonCell else {
            return nil
        }

        viewModel.registerButtonEnabled
            .drive(onNext: { valid in
                footerCell.sendButton.isEnabled = valid
                footerCell.sendButton.alpha = valid ? 1 : 0.5
            })
            .disposed(by: disposeBag)

        return footerCell
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 45 / 667 * Global.screenHeight
    }
}
