//
//  MyInformationSetDotCell.swift
//  Voice
//
//  Created by kasim on 2018/11/27.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit

class MyInformationSetDotCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dotImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func update(model: MyInformationSetModel) {
        titleLabel.text = model.title
    }
}
