//
//  MyInformationSetCell.swift
//  Voice
//
//  Created by kasim on 2018/11/27.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class MyInformationSetTextFieldCell: UITableViewCell {
    
    @IBOutlet weak var textField: UITextField!
    let disposeBag = DisposeBag()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func update(model: MyInformationSetModel) {
        textField.text = model.title
    }
}
