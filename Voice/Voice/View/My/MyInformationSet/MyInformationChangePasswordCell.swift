//
//  MyInformationChangePasswordCell.swift
//  Voice
//
//  Created by kasim on 2018/12/16.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit

class MyInformationChangePasswordCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!

    @IBOutlet weak var noteLabel: UILabel!

    @IBOutlet weak var textField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func update(model: MyInformationSetModel) {
        let data = model.title.components(separatedBy: ",")
        titleLabel.text = data[0]
        noteLabel.text = data[1]
    }
}
