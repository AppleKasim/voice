//
//  MyLoginViewController.swift
//  Voice
//
//  Created by kasim on 2018/11/24.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit

class MyLoginViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var accountLabel: UILabel!
    @IBOutlet weak var accountTextField: UITextField! {
        didSet {
            accountTextField.layer.borderWidth = 2.0
            accountTextField.layer.borderColor = UIColor.black.cgColor
            accountTextField.clipsToBounds = true
        }
    }

    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var passwordTextField: UITextField! {
        didSet {
            passwordTextField.layer.borderWidth = 2.0
            passwordTextField.layer.borderColor = UIColor.black.cgColor
            passwordTextField.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var loginButton: UIButton! {
        didSet {
            loginButton.setTitleColor(.black, for: .normal)
            loginButton.backgroundColor = Global.yellowColorF8BF00
        }
    }

    @IBOutlet weak var forgetPasswordButton: UIButton! {
        didSet {
            forgetPasswordButton.setTitleColor(.black, for: .normal)
        }
    }

    @IBOutlet weak var scrollView: UIScrollView!

    @IBOutlet weak var emailButton: UIButton! {
        didSet {
            emailButton.setTitleColor(.white, for: .normal)
            emailButton.backgroundColor = Global.greenColor08BC7E
        }
    }

    @IBOutlet weak var faceBookButton: UIButton! {
        didSet {
            faceBookButton.setTitleColor(.white, for: .normal)
            faceBookButton.backgroundColor = Global.blueColor3C5B9B
        }
    }

    @IBOutlet weak var googleButton: UIButton! {
        didSet {
            googleButton.setTitleColor(.white, for: .normal)
            googleButton.backgroundColor = Global.redColorF63D27
        }
    }

    @IBOutlet weak var logoImageView: UIImageView!

    @IBOutlet weak var lineView: UIView! {
        didSet {
            lineView.backgroundColor = Global.grayColor70
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        accountTextField.layer.cornerRadius = accountTextField.frame.height / 10
        passwordTextField.layer.cornerRadius = passwordTextField.frame.height / 10
    }
}
