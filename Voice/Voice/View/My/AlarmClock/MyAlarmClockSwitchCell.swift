//
//  AlarmClockSwitchCell.swift
//  Voice
//
//  Created by kasim on 2018/11/26.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit

class MyAlarmClockSwitchCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var alarmClockSwitch: UISwitch! {
        didSet {
            alarmClockSwitch.onTintColor = Global.yellowColorF8BF00
            alarmClockSwitch.layer.cornerRadius = alarmClockSwitch.frame.height / 2
            alarmClockSwitch.backgroundColor = .white

            alarmClockSwitch.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func update(model: MyAlarmClockModel) {
        titleLabel.text = model.title
        alarmClockSwitch.isOn = model.isOnForSwitch
    }

}
