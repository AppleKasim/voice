//
//  MyAlarmClockViewController.swift
//  Voice
//
//  Created by kasim on 2018/11/26.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxDataSources

class MyAlarmClockViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    // swiftlint:disable line_length
    private lazy var dataSource = RxTableViewSectionedReloadDataSource<SectionOfMyAlarmClock>(configureCell: configureCell)

    private lazy var configureCell: RxTableViewSectionedReloadDataSource<SectionOfMyAlarmClock>.ConfigureCell = { [weak self] (dataSource, tableView, indexPath, data) in

        if indexPath.row == 0 {
            guard let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "MyAlarmClockSwitchCell") as? MyAlarmClockSwitchCell else {
                return UITableViewCell()
            }
            tableViewCell.update(model: data)
            return tableViewCell
        } else {
            guard let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "MyAlarmClockOptionCell") as? MyAlarmClockOptionCell else {
                return UITableViewCell()
            }
            tableViewCell.update(model: data)
            return tableViewCell
        }
    }
    // swiftlint:enable line_length

    let viewModel = MyAlarmClockViewModel()
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.items
            .asDriver()
            .drive(tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)

        viewModel.updateItem()

        tableView.rx.setDelegate(self)
            .disposed(by: disposeBag)

        tableView.rx.itemSelected
            .subscribe(onNext: { [weak self] indexPath in
                guard let strongSelf = self else { return }

                switch indexPath.row {
                case 1:
                    strongSelf.performSegue(withIdentifier: "ToMyRepeatSetSegue", sender: indexPath.row)
                case 3:
                    strongSelf.performSegue(withIdentifier: "ToAlarmClockRingSegue", sender: indexPath.row)
                default:
                    break
                }
            })
            .disposed(by: disposeBag)
    }

}

extension MyAlarmClockViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50 / 667 * Global.screenHeight
    }
}
