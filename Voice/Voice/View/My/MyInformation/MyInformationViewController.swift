//
//  MyInformationViewController.swift
//  Voice
//
//  Created by kasim on 2018/11/27.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxDataSources

class MyInformationViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    // swiftlint:disable line_length
    private lazy var dataSource = RxTableViewSectionedReloadDataSource<SingleSectionModelMy<MyTitleAndSelectItem>>(configureCell: configureCell)

    private lazy var configureCell: RxTableViewSectionedReloadDataSource<SingleSectionModelMy<MyTitleAndSelectItem>>.ConfigureCell = { [weak self] (dataSource, tableView, indexPath, data) in

        guard let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "MyInformationCell") as? MyInformationCell else {
            return UITableViewCell()
        }

        tableViewCell.update(model: data)

        return tableViewCell

    }
    // swiftlint:enable line_length

    let viewModel = MyInformationViewModel()
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.rx.setDelegate(self)
            .disposed(by: disposeBag)

        viewModel.items
            .asDriver()
            .drive(tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)

        viewModel.updateItem()

        tableView.rx.itemSelected
            .subscribe(onNext: { [weak self] indexPath in
                guard let strongSelf = self else { return }

                if indexPath.row != 0, indexPath.row != 4 {
                    strongSelf.performSegue(withIdentifier: "ToInformationSetSegue", sender: indexPath.row)
                }
            })
            .disposed(by: disposeBag)
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ToInformationSetSegue" {
            guard let viewController = segue.destination as? MyInformationSetViewController,
                let index = sender as? Int else {
                    return
            }

            switch index {
            case 1:
                viewController.informationSetMode = .nickname
            case 2:
                viewController.informationSetMode = .email
            case 3:
                viewController.informationSetMode = .gender
            case 5:
                viewController.informationSetMode = .city
            case 6:
                viewController.informationSetMode = .changePassword
            case 7:
                viewController.informationSetMode = .signature
            default:
                break
            }
        }
    }

}

extension MyInformationViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50 / 667 * Global.screenHeight
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 5 / 667 * Global.screenHeight
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0001
    }
}
