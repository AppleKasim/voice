//
//  MyAlarmClockRingCell.swift
//  Voice
//
//  Created by kasim on 2018/11/27.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit

class MyAlarmClockRingCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var rightImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func update(model: MyTitleAndDot) {
        titleLabel.text = model.title
    }
}
