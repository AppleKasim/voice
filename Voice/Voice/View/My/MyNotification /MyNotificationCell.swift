//
//  MyNotificationCell.swift
//  Voice
//
//  Created by kasim on 2018/11/25.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit

class MyNotificationCell: UITableViewCell {

    @IBOutlet weak var unreadImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func update(model: MyNotificationModel) {
        titleLabel.text = model.title
        dateLabel.text = model.date
    }

}
