//
//  RecommendationDetailViewController.swift
//  Voice
//
//  Created by kasim on 2018/10/28.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class RecommendationDetailViewController: UIViewController {

    private var currentPageIndex = BehaviorRelay(value: 0)
    var pageContainerViewController: PageContainerViewController!
    var recommendationModel: RecommendationModel!

    //topView UI
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var topViewHeightConstraint: NSLayoutConstraint! {
        didSet {
            topViewHeightConstraint.constant = 235 / 603 * Global.notNavigationScreenHeight
        }
    }

    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var introductionLabel: UILabel!

    @IBOutlet weak var courseLabel: UILabel!

    @IBOutlet weak var studentsLabel: UILabel!
    @IBOutlet weak var keepButton: UIButton! {
        didSet {
            keepButton.imageView?.contentMode = .scaleAspectFit
        }
    }
    @IBOutlet weak var keepAmountLabel: UILabel!
    @IBOutlet weak var chargeTypeLabel: UILabel!
    @IBOutlet weak var courseBackgroundView: UIView! {
        didSet {
            courseBackgroundView.backgroundColor = Global.grayColorEFF4FC

            let currentCellHeight = 245 / 554 * (Global.notNavigationScreenHeight - Global.tabBarHeight)

            courseBackgroundView.layer.cornerRadius = (14 / 245 * currentCellHeight) / 2.0
        }
    }

    //segmentedControl UI
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var segmentedControlLineBackground: UIView! {
        didSet {
            segmentedControlLineBackground.backgroundColor = Global.blackColor00
        }
    }
    @IBOutlet weak var segmentedControlLine: UIView! {
        didSet {
            segmentedControlLine.backgroundColor = Global.yellowColorF8BF00
        }
    }

    @IBOutlet weak var lineLeadingConstraint: NSLayoutConstraint!

    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        segmentedControl.removeBorder()

        let fontStyle = SegmentedControlFontStyle()
        let titles = ["dsfudshfu\n1111", "fhduhfud\n2222"]
        segmentedControl.customUnderlineStyle(titles: titles, withFontStyle: fontStyle, disposeBag: disposeBag)

        pageContainerViewController.changeTabForMainViewController = {[weak self] index in
            guard let strongSelf = self else {
                return
            }

            strongSelf.currentPageIndex.accept(index)
        }

        currentPageIndex.asObservable()
            .subscribe(onNext: {[weak self] value in
                guard let strongSelf = self else {
                    return
                }
                strongSelf.segmentedControl.selectedSegmentIndex = value
                strongSelf.segmentedControl.sendActions(for: UIControlEvents.valueChanged)
                if value == 0 {
                    strongSelf.tabBarController?.tabBar.isHidden = true
                } else {
                    strongSelf.tabBarController?.tabBar.isHidden = false
                }

            })
            .disposed(by: disposeBag)

        segmentedControl.rx.selectedSegmentIndex
            .subscribe(onNext: {[weak self] index in
                guard let strongSelf = self else {
                    return
                }
                strongSelf.pageContainerViewController.showPage(byIndex: index)
            })
            .disposed(by: disposeBag)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "RecommendationDetailToContainer",
            let destination = segue.destination as? PageContainerViewController {
            pageContainerViewController = destination
            pageContainerViewController.subPageContainerMode = .recommendationDetail
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }

    @IBAction func changed(_ sender: UISegmentedControl) {
        //segmentedIndex = sender.selectedSegmentIndex
        changeUnderlinePosition()
    }

    func changeUnderlinePosition() {
        let itemWidth = (330 / 375 * Global.screenWidth) / CGFloat(segmentedControl.numberOfSegments)

        UIView.animate(withDuration: 0.3) {
            self.lineLeadingConstraint.constant = CGFloat(self.segmentedControl.selectedSegmentIndex) * itemWidth
        }
    }

}
