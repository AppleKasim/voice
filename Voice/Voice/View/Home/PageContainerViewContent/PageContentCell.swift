//
//  PageContentCell.swift
//  Voice
//
//  Created by kasim on 2018/11/14.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit

class PageContentCell: UITableViewCell {

    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var introductionLabel: UILabel!

    @IBOutlet weak var courseLabel: UILabel!

    @IBOutlet weak var studentsLabel: UILabel!
    @IBOutlet weak var keepButton: UIButton!
    @IBOutlet weak var keepAmountLabel: UILabel!
    @IBOutlet weak var chargeTypeLabel: UILabel!

    @IBOutlet weak var courseBackgroundView: UIView! {
        didSet {
            courseBackgroundView.backgroundColor = Global.grayColorEFF4FC

            let currentCellHeight = 245 / 554 * (Global.notNavigationScreenHeight - Global.tabBarHeight)

            courseBackgroundView.layer.cornerRadius = (14 / 245 * currentCellHeight) / 2.0
        }
    }

    @IBOutlet weak var separatorView: UIView! {
        didSet {
            separatorView.backgroundColor = Global.grayColorF5
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func update(model: RecommendationModel) {
        introductionLabel.text = model.introduction
        courseLabel.text = model.course
        studentsLabel.text = model.students
        keepAmountLabel.text = model.keepAmount
        chargeTypeLabel.text = model.chargeType
    }
}
