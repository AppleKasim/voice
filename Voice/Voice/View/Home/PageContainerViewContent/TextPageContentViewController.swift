//
//  TextPageContentViewController.swift
//  Voice
//
//  Created by kasim on 2018/11/17.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class TextPageContentViewController: UIViewController {

    @IBOutlet weak var textView: UITextView! {
        didSet {
            textView.text = ""
        }
    }

    var scrollView: UIScrollView! {
        didSet {
//            scrollView.delegate = self
//            scrollView.bounces = false
        }
    }

    var segmentRect = BehaviorRelay<CGRect>(value: CGRect())

    var viewModel: SubPageContainer!
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        switch viewModel {
        case let audioBookDetailViewModel as AudioBookDetailViewModel:
            audioBookDetailViewModel.introduction
                .subscribe(onNext: { [weak self] introduction in
                    guard let strongSelf = self else { return }
                    guard let text = introduction?.introduce else {
                        return
                    }
                    strongSelf.textView.text = text
                })
                .disposed(by: disposeBag)
        case let authorDetailViewModel as AuthorDetailViewModel:
            authorDetailViewModel.introduction
                .subscribe(onNext: { [weak self] introduction in
                    guard let strongSelf = self else { return }
                    guard let text = introduction?.brief else {
                        return
                    }
                    strongSelf.textView.text = text
                })
                .disposed(by: disposeBag)

        default:
            return
        }
    }
}

