//
//  CoursePageContentCell.swift
//  Voice
//
//  Created by kasim on 2018/11/19.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit

class CoursePageContentCell: UITableViewCell {

    @IBOutlet weak var leftImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var keepButton: UIButton! {
        didSet {
            keepButton.isHidden = true
            keepButton.imageView?.contentMode = .scaleAspectFit
            keepButton.setTitleColor(Global.grayColor78849E, for: .normal)
        }
    }
    @IBOutlet weak var courseLabel: UILabel! {
        didSet {
            courseLabel.isHidden = true
        }
    }
    @IBOutlet weak var studentsLabel: UILabel! {
        didSet {
            studentsLabel.isHidden = true
        }
    }
    @IBOutlet weak var chargeTypeLabel: UILabel! {
        didSet {
            chargeTypeLabel.isHidden = true
        }
    }
    @IBOutlet weak var courseBackgroundView: UIView! {
        didSet {
            courseBackgroundView.isHidden = true
            courseBackgroundView.backgroundColor = Global.grayColorEFF4FC
        }
    }

    var cellHeight: CGFloat = 0 {
        didSet {
            courseBackgroundView.layer.cornerRadius = (15 / 70 * cellHeight) / 2.0
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func update(model: AudioBook) {
        titleLabel.text = model.title
        let imageUrl = URL(string: model.profile)
        leftImageView.sd_setImage(with: imageUrl, completed: nil)
//        courseLabel.text = model.course
//        studentsLabel.text = model.students
//        chargeTypeLabel.text = model.price
//        keepButton.setTitle(model.keepAmount, for: .normal)
    }
}
