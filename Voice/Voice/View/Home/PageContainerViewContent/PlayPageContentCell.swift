//
//  PlayListPageCell.swift
//  Voice
//
//  Created by kasim on 2018/11/18.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SDWebImage

class PlayPageContentCell: UITableViewCell {

    @IBOutlet weak var dotImageView: UIImageView! {
        didSet {
            dotImageView.contentMode = .scaleAspectFit
        }
    }
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel! {
        didSet {
            detailLabel.isHidden = true
        }
    }
    @IBOutlet weak var playImageView: FLAnimatedImageView!
    @IBOutlet weak var bottomVerticalLine: UIView! {
        didSet {
            bottomVerticalLine.backgroundColor = Global.grayColorE0
        }
    }
    @IBOutlet weak var topVerticalLine: UIView! {
        didSet {
            topVerticalLine.backgroundColor = Global.grayColorE0
        }
    }
    @IBOutlet weak var titleStackView: UIStackView! {
        didSet {
            titleStackView.spacing = 6 / 375 * Global.screenWidth
        }
    }

    let disposeBag = DisposeBag()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func update(model: PlayVoice) {
        titleLabel.text = model.title
    }

    func update(model: PlayListPageContentModel) {
        titleLabel.text = model.title
    }
}
