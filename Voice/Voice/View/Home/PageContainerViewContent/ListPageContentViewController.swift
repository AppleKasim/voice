//
//  PlayListPageContentViewController.swift
//  Voice
//
//  Created by kasim on 2018/11/17.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit
import RxDataSources
import RxCocoa
import RxSwift
import SDWebImage

class ListPageContentViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.bounces = false
            tableView.isScrollEnabled = false
        }
    }

    // swiftlint:disable line_length
    private lazy var dataSource = RxTableViewSectionedReloadDataSource<SingleSectionModel<String, ListPageContent>>(configureCell: configureCell)

    private lazy var configureCell: RxTableViewSectionedReloadDataSource<SingleSectionModel<String, ListPageContent>>.ConfigureCell = { [weak self] (dataSource, tableView, indexPath, element) in

        guard let strongSelf = self else {
            return UITableViewCell()
        }

        switch element {
        case let coursePageContentModel as AudioBook:
            guard let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "CoursePageContentCell") as? CoursePageContentCell else {
                return UITableViewCell()
            }
            tableViewCell.cellHeight = strongSelf.viewModel.cellHeight()
            tableViewCell.update(model: coursePageContentModel)

            return tableViewCell

        case let playVoice as PlayVoice:
            guard let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "PlayListPageCell") as? PlayPageContentCell else {
                return UITableViewCell()
            }

            if indexPath.row == 0 {
                tableViewCell.topVerticalLine.isHidden = true
            }

            if indexPath.row == dataSource.sectionModels[0].items.count {
                tableViewCell.bottomVerticalLine.isHidden = true
            }

            tableViewCell.update(model: playVoice)

            PlayManager.share().currentVoice
                .subscribe( onNext: { [weak self] currentVoice in
                    if let voice = currentVoice, voice.voiceId == playVoice.voiceId {
                        do {
                            let imageData = try Data(contentsOf: Bundle.main.url(forResource: "pause", withExtension: "gif")!)
                            tableViewCell.playImageView.animatedImage = FLAnimatedImage(animatedGIFData: imageData)
                        } catch {
                            print("出現錯誤 ： \(error)")
                        }
                    } else {
                        tableViewCell.playImageView.image = UIImage(named: "play")
                    }
                })
                .disposed(by: tableViewCell.disposeBag)

            return tableViewCell
        default:
            return UITableViewCell()
        }
    }
    // swiftlint:enable line_length

    var scrollView: UIScrollView! {
        didSet {
            scrollView.delegate = self
            scrollView.bounces = false
        }
    }
    var segmentRect = BehaviorRelay<CGRect>(value: CGRect())
    let interactor = Interactor()
    var viewModel: SubPageContainer!
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.rx.setDelegate(self)
            .disposed(by: disposeBag)

        switch viewModel {
        case let recommendationDetail as RecommendationDetailViewModel:
            recommendationDetail.playListItems
                .asDriver()
                .drive(tableView.rx.items(dataSource: dataSource))
                .disposed(by: disposeBag)
        case let anchorDetail as AuthorDetailViewModel:
            anchorDetail.coursePageItems
                .asDriver()
                .drive(tableView.rx.items(dataSource: dataSource))
                .disposed(by: disposeBag)
        case let audioBook as AudioBookDetailViewModel:
            audioBook.playListItems
                .asDriver()
                .drive(tableView.rx.items(dataSource: dataSource))
                .disposed(by: disposeBag)
        default:
            break
        }

        tableView.rx.itemSelected
            .subscribe(onNext: { [weak self] indexPath in
                guard let strongSelf = self else {
                    return
                }

                if strongSelf.viewModel is AuthorDetailViewModel {
                    strongSelf.performSegue(withIdentifier: "PlayListPageContentToAudioBookDetail", sender: indexPath)
                } else {
                    strongSelf.performSegue(withIdentifier: "PlayListPageContentToPlayPage", sender: indexPath)
                }
        }).disposed(by: disposeBag)

        segmentRect.subscribe({ [unowned self] _ in
            self.tableView.reloadData()
        }).disposed(by: disposeBag)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PlayListPageContentToPlayPage" {
            guard let viewController = segue.destination as? PlayViewController else {
                    return
            }

            guard let indexPath = sender as? IndexPath,
                let audioBook = viewModel as? AudioBookDetailViewModel else {
                return
            }

            viewController.transitioningDelegate = self
            viewController.interactor = interactor

            PlayManager.share().isPlayNextSong = true
            PlayManager.share().playingIndex = indexPath.row
            PlayManager.share().albumId = audioBook.albumId

        } else if segue.identifier == "PlayListPageContentToAudioBookDetail" {
            guard let viewController = segue.destination as? AudioBooksDetailViewController else {
                return
            }

            guard let indexPath = sender as? IndexPath,
                let authorDetailViewModel = viewModel as? AuthorDetailViewModel,
                let audioBook = authorDetailViewModel.coursePageItems.value[0].items[indexPath.row] as? AudioBook
                else {
                    return
            }

            viewController.audioBook = audioBook
        }
    }

}

extension ListPageContentViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel.cellHeight()
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return segmentRect.value.maxY + (tabBarController?.tabBar.frame.size.height)!
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.00001
    }
}

extension ListPageContentViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {

        guard tableView != nil else {
            return
        }

        if self.scrollView == scrollView {
            tableView.isScrollEnabled = (self.scrollView.contentOffset.y >= segmentRect.value.minY)

            print("scroll: \(scrollView.contentOffset.y)")
            print("segmentRect: \(segmentRect.value.minY)")
            if tableView.isScrollEnabled {
                self.scrollView.contentOffset.y = segmentRect.value.minY
                print("＝＝＝＝＝: \(self.scrollView.contentOffset.y)")
            }

        }

        if scrollView == tableView {
            self.tableView.isScrollEnabled = (tableView.contentOffset.y > 0)

            print("table: \(scrollView.contentOffset.y)")
            print("tableRect: \(segmentRect.value.minY)")
            if !self.tableView.isScrollEnabled {
                self.scrollView.isScrollEnabled = true
            } else {
                self.scrollView.isScrollEnabled = false
            }
        }

    }
}

extension ListPageContentViewController: UIViewControllerTransitioningDelegate {
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return DismissAnimator()
    }

    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return interactor.hasStarted ? interactor : nil
    }
}
