//
//  PageContentViewController.swift
//  Voice
//
//  Created by kasim on 2018/11/14.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit
import RxDataSources
import RxCocoa
import RxSwift

class PageContentViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!

    // swiftlint:disable line_length
    private lazy var dataSource = RxTableViewSectionedReloadDataSource<SectionOfRecommendationList>(configureCell: configureCell)

    private lazy var configureCell: RxTableViewSectionedReloadDataSource<SectionOfRecommendationList>.ConfigureCell = { [weak self] (dataSource, tableView, indexPath, element) in

        guard let strongSelf = self else {
            return UITableViewCell()
        }

        guard let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "PageContentCell") as? PageContentCell else {
            return UITableViewCell()
        }
        tableViewCell.update(model: element)

        return tableViewCell
    }
    // swiftlint:enable line_length

    var viewModel = RecommendationListViewModel()
    let disposeBag = DisposeBag()
    var pageTitle = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.rx.setDelegate(self)
            .disposed(by: disposeBag)

        viewModel.getPageContentTuples(title: pageTitle)?.1
            .asDriver()
            .drive(tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)

        //viewModel.updateItem()

        tableView.rx.itemSelected
            .map { [weak self] indexPath -> RecommendationModel? in
                guard let strongSelf = self else {
                    return nil
                }
                return strongSelf.dataSource[indexPath]
            }
            .subscribe(onNext: { [weak self] item in
                guard let item = item, let strongSelf = self else { return }

                strongSelf.performSegue(withIdentifier: "PageContentToDetailSegue", sender: item)

            })
            .disposed(by: disposeBag)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PageContentToDetailSegue" {
            guard let viewController = segue.destination as? RecommendationDetailViewController,
                let recommendationModel = sender as? RecommendationModel else {
                    return
            }

            viewController.recommendationModel = recommendationModel
        }
    }
}

extension PageContentViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel.cellHeight()
    }

}
