//
//  AnchorListHeaderView.swift
//  Voice
//
//  Created by kasim on 2018/11/12.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit

class AnchorListHeaderView: UICollectionReusableView {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var englishTitleLabel: UILabel!
}
