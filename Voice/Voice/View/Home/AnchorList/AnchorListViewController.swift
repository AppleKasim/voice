//
//  AnchorListViewController.swift
//  Voice
//
//  Created by kasim on 2018/11/4.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

class AnchorListViewController: UIViewController {

    //var authors: [Author] = []
    var currentPage: Int = 1
    let viewModel = AuthorListViewModel()
    let disposeBag = DisposeBag()

    @IBOutlet weak var collectionView: UICollectionView!

    @IBOutlet weak var collectionViewLayout: UICollectionViewFlowLayout! {
        didSet {
            let width = 110 / 375 * Global.screenWidth
            let height = 170 / 667 * Global.screenHeight
            collectionViewLayout.itemSize = CGSize(width: width, height: height)
            collectionViewLayout.minimumLineSpacing = 0
            collectionViewLayout.minimumInteritemSpacing = 10 / 375 * Global.screenWidth

            let leftEdge = 13 / 375 * Global.screenWidth
            let rightEdge = 12 / 375 * Global.screenWidth
            collectionViewLayout.sectionInset = UIEdgeInsets(top: 0, left: leftEdge, bottom: 0, right: rightEdge)
        }
    }

    // swiftlint:disable line_length
    private lazy var dataSource =  RxCollectionViewSectionedReloadDataSource<SingleSectionModel<AuthorListHeader, Author>>(configureCell: configureCell, configureSupplementaryView: configureSupplementaryView)

    private lazy var configureCell: RxCollectionViewSectionedReloadDataSource<SingleSectionModel<AuthorListHeader, Author>>.ConfigureCell = {(dataSource, collectionView, indexPath, element) in

        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AnchorListCollectionViewCell", for: indexPath) as? AnchorListCollectionViewCell else {
            return UICollectionViewCell()
        }

        cell.update(author: element)

        return cell
    }

    private lazy var configureSupplementaryView: RxCollectionViewSectionedReloadDataSource<SingleSectionModel<AuthorListHeader, Author>>.ConfigureSupplementaryView = {(dataSource ,cv, kind, indexPath) in
        guard let section = cv.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "AnchorListHeaderView", for: indexPath) as? AnchorListHeaderView else {
            return UICollectionReusableView()
        }

        section.titleLabel.text = "\(dataSource[indexPath.section].header.title)"
        section.englishTitleLabel.text = "\(dataSource[indexPath.section].header.englishTitle)"

        return section
    }

    // swiftlint:enable line_length
    override func viewDidLoad() {
        super.viewDidLoad()

        removeNavigationBarBottomLine()

        collectionView.backgroundColor = UIColor.white

        collectionView.rx.setDelegate(self)
            .disposed(by: disposeBag)

        viewModel.items
            .asDriver()
            .drive(collectionView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)

        viewModel.updateCreatorList(page: currentPage) { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.currentPage += 1
        }

        collectionView.rx.itemSelected
            .map { [weak self] indexPath -> Author? in
                guard let strongSelf = self else {
                    return nil
                }
                return strongSelf.dataSource[indexPath]
            }
            .subscribe(onNext: { [weak self] item in
                guard let strongSelf = self else { return }

                strongSelf.performSegue(withIdentifier: "ToAnchorDetailSegue", sender: item)
            })
            .disposed(by: disposeBag)

    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ToAnchorDetailSegue" {
            guard let viewController = segue.destination as? AnchorDetailViewController,
                let anchor = sender as? Author else {
                return
            }
            viewController.authorName = anchor.alias
            viewController.authorId = Int(anchor.authorId)
            viewController.profileImageUrl = anchor.profileUrl
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)

    }
}

extension AnchorListViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {

        let height = 42 / 667 * Global.screenHeight
        return CGSize(width: Global.screenWidth, height: height)
    }
}
