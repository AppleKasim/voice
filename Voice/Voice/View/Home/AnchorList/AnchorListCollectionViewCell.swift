//
//  AnchorListCollectionViewCell.swift
//  Voice
//
//  Created by kasim on 2018/11/12.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit

class AnchorListCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var typeImageView: UIImageView! {
        didSet {
            typeImageView.isHidden = true
        }
    }

    func update(author: Author) {
        nameLabel.text = author.alias
        contentLabel.text = author.introduce

        guard let profileUrl = author.profileUrl else {
            return
        }
        let url = URL(string: profileUrl)
        profileImageView.sd_setImage(with: url, completed: nil)
    }
}
