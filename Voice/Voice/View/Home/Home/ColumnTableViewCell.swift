//
//  ColumnTableViewCell.swift
//  Voice
//
//  Created by kasim on 2018/11/3.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class ColumnTableViewCell: UITableViewCell {

    @IBOutlet weak var coverImageView: UIImageView! {
        didSet {
            coverImageView.clipsToBounds = true
            coverImageView.layer.cornerRadius = (85 / 95 * Global.columnTableViewCellHeight) / 2
        }
    }
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var introductionLabel: UILabel!
    @IBOutlet weak var authorNameLabel: UILabel!

    @IBOutlet weak var introductionButton: UIButton! {
        didSet {
            introductionButton.imageView?.contentMode = .scaleAspectFit
            introductionButton.setCornerRadiusBorder(forMultiplier: 20 / 95 * Global.columnTableViewCellHeight)
        }
    }
    @IBOutlet weak var playButton: UIButton! {
        didSet {
            playButton.imageView?.contentMode = .scaleAspectFit
            playButton.setCornerRadiusBorder(forMultiplier: 20 / 95 * Global.columnTableViewCellHeight)
        }
    }

    @IBOutlet weak var bottomStackView: UIStackView! {
        didSet {
            bottomStackView.spacing = 5 / 375 * Global.screenWidth
        }
    }

    var disposeBag = DisposeBag()

    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
