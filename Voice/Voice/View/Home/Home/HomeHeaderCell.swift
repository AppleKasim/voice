//
//  HomeHeaderCell.swift
//  Voice
//
//  Created by kasim on 2018/11/4.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class HomeHeaderCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var moreButton: UIButton! {
        didSet {
            moreButton.imageView?.contentMode = .scaleAspectFill
        }
    }
    @IBOutlet weak var englishLabel: UILabel!

    var disposeBag = DisposeBag()

    override func awakeFromNib() {
        super.awakeFromNib()

        //moreButton.imageView?.contentMode = .scaleAspectFill
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func layoutSubviews() {
        super.layoutSubviews()

    }

    func update(model: HomeHeader) {
        titleLabel.text = model.title
        englishLabel.text = model.titleEnglish
    }
}
