//
//  HomeViewController.swift
//  Voice
//
//  Created by kasim on 2018/10/20.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxDataSources

class HomeViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView! {
        didSet {
            let headerNib = UINib.init(nibName: "HomeBannerView", bundle: Bundle.main)
            tableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "HomeBannerView")

            tableView!.estimatedRowHeight = 44.0
            tableView!.rowHeight = UITableView.automaticDimension
        }
    }

    // swiftlint:disable line_length
    private lazy var dataSource = RxTableViewSectionedReloadDataSource<SingleSectionModel<HomeHeader, [HomeModelType]>>(configureCell: configureCell)

    private lazy var configureCell: RxTableViewSectionedReloadDataSource<SingleSectionModel<HomeHeader, [HomeModelType]>>.ConfigureCell = { [weak self] (dataSource, tableView, indexPath, data) in

        guard let strongSelf = self else {
            return UITableViewCell()
        }

        switch indexPath.section {
            //case 1************這版本不顯示***********
//        case 1:
//            guard let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "HomeCollectionTableViewCell") as? HomeCollectionTableViewCell else {
//                return UITableViewCell()
//            }
//
//            tableViewCell.collectionCellType = .recommendation
//            tableViewCell.frame = tableView.bounds
//            tableViewCell.layoutIfNeeded()
//            tableViewCell.navigationController = strongSelf.navigationController!
//            tableViewCell.collectionViewLayout.invalidateLayout()
//
//            guard let recommendationModels = data as? [RecommendationModel] else {
//                return UITableViewCell()
//            }
//
//            tableViewCell.recommendationModels = recommendationModels
//
//            return tableViewCell
        case 1:
            guard let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "HomeCollectionTableViewCell") as? HomeCollectionTableViewCell else {
                return UITableViewCell()
            }
            // swiftlint:enable line_length

            tableViewCell.collectionCellType = .audioBooks
            tableViewCell.frame = tableView.bounds
            tableViewCell.layoutIfNeeded()
            tableViewCell.navigationController = strongSelf.navigationController!

            guard let audioBooks = data as? [AudioBook] else {
                return UITableViewCell()
            }

            tableViewCell.audioBooks = audioBooks

            return tableViewCell
        case 2:
            guard let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "HomeCollectionTableViewCell") as? HomeCollectionTableViewCell else {
                return UITableViewCell()
            }
            // swiftlint:enable line_length

            tableViewCell.collectionCellType = .anchor
            tableViewCell.frame = tableView.bounds
            tableViewCell.layoutIfNeeded()
            tableViewCell.navigationController = strongSelf.navigationController!

            guard let authors = data as? [Author] else {
                return UITableViewCell()
            }

            tableViewCell.authors = authors

            return tableViewCell
            //case 4************這版本不顯示***********
//        case 4:
//            guard let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "ColumnTableViewCell") as? ColumnTableViewCell else {
//                return UITableViewCell()
//            }
//
//            guard let recommendationModels = data as? [RecommendationModel] else {
//                return UITableViewCell()
//            }
//
//            tableViewCell.introductionLabel.text = recommendationModels[indexPath.row].introduction
//            tableViewCell.titleLabel.text = recommendationModels[indexPath.row].courseModel.title
//            tableViewCell.authorNameLabel.text = recommendationModels[indexPath.row].courseModel.authorName
//
//            tableViewCell.introductionButton.rx.tap
//                .asObservable()
//                .subscribe(onNext: { [weak self] in
//                    guard let strongSelf = self else {
//                        return
//                    }
//
//                    let storyboard = UIStoryboard(name: "HomeStoryboard", bundle: nil)
//
//                    // swiftlint:disable line_length
//                    guard let viewController = storyboard.instantiateViewController(withIdentifier: "RecommendationDetailStoryboardId") as? RecommendationDetailViewController else {
//                        return
//                    }
//                    // swiftlint:enable line_length
//
//                    //-----
//                    guard let recommendationModels = data as? [RecommendationModel] else {
//                        return
//                    }
//
//                    viewController.recommendationModel = recommendationModels[indexPath.row]
//
//                    //-----
//
//                    strongSelf.navigationController!.pushViewController(viewController, animated: true)
//                }).disposed(by: tableViewCell.disposeBag)
//
//            tableViewCell.playButton.rx.tap
//                .asObservable()
//                .subscribe(onNext: { [weak self] in
//                    guard let strongSelf = self else {
//                        return
//                    }
//
//                    let storyboard = UIStoryboard(name: "PlayStoryboard", bundle: nil)
//                    let viewController = storyboard.instantiateViewController(withIdentifier: "PlayStoryboard") as UIViewController
//
//                    strongSelf.navigationController!.pushViewController(viewController, animated: true)
//                }).disposed(by: tableViewCell.disposeBag)
//
//            return tableViewCell
        default:
            return UITableViewCell()
        }

    }
    // swiftlint:enable line_length

    let viewModel = HomeViewModel()
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.backgroundColor = .black

        //需要再viewModel.items.bind之前，不然使用tableView style grouped，上方會出現空白。
        tableView.rx.setDelegate(self)
            .disposed(by: disposeBag)

        viewModel.items
            .bind(to: tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)

        viewModel.updateItem()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "RecommendationListSegue" {
            guard let viewController = segue.destination as? RecommendationListViewController else {
                return
            }

            viewModel.items.subscribe {
                guard let recommendationModels = $0.element![1].items[0] as? [RecommendationModel] else {
                    return
                }
                //viewController.recommendationModels = recommendationModels
            }.disposed(by: disposeBag)
        } else if segue.identifier == "AnchorListSegue" {
            guard let viewController = segue.destination as? AnchorListViewController else {
                return
            }

            viewModel.items.subscribe {
                guard let authorModels = $0.element![2].items[0] as? [AuthorModel] else {
                    return
                }
                //viewController.authorModels = authorModels
                }.disposed(by: disposeBag)
        } else if segue.identifier == "AudioBooksListSegue" {
            guard let viewController = segue.destination as? AudioBooksListViewController else {
                return
            }

            //viewController.viewModel = self.viewModel

            viewModel.items.subscribe {
                guard let audioBooks = $0.element![1].items[0] as? [AudioBook] else {
                    return
                }
                viewController.audioBooks = audioBooks

                }.disposed(by: disposeBag)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.navigationController?.setNavigationBarHidden(true, animated: true)
        self.tabBarController?.tabBar.isHidden = false
    }
}

extension HomeViewController: BannerScrollViewDelegate {
    func scrollView(_ scrollView: BannerCollectionView, didSelectRowAt index: NSInteger) {
        print("点击了第\(index)张图片")
    }
}

extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 100
            //case 1************這版本不顯示***********
//        case 1:
//            return 200 / 667 * Global.screenHeight
        case 1:
            return 325 / 667 * Global.screenHeight
        case 2:
            return 341 / 667 * Global.screenHeight
            //case 4************這版本不顯示***********
//        case 4:
//            return Global.columnTableViewCellHeight//95 / 667 * Global.screenHeight
        default:
            return 0
        }
    }

    // swiftlint:disable line_length
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        case 0:
            guard let headerCell = tableView.dequeueReusableCell(withIdentifier: "HomeBannerHeaderCell") as? HomeBannerHeaderCell else {
                return nil
            }

            let scrollView = BannerCollectionView.init(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 190 / 667 * Global.screenHeight))
            // swiftlint:enable line_length

            viewModel.bannerModels
                .drive(onNext: { imageUrls in
                    scrollView.imageUrls = imageUrls
                })
                .disposed(by: disposeBag)

            scrollView.bannerScrollViewDelegate = self
            scrollView.pageControl = headerCell.pageControl

            headerCell.addSubview(scrollView)

            return headerCell
            //case 1************這版本不顯示***********
//        case 1:
//            guard let headerCell = produceHeaderCell(tapSegueIdentifier: "RecommendationListSegue") else {
//                return nil
//            }
//
//            headerCell.update(model: dataSource.sectionModels[section].header)
//
//            return headerCell

        case 1:
            guard let headerCell = produceHeaderCell(tapSegueIdentifier: "AudioBooksListSegue") else {
                return nil
            }

            headerCell.update(model: dataSource.sectionModels[section].header)

            return headerCell
        case 2:
            guard let headerCell = produceHeaderCell(tapSegueIdentifier: "AnchorListSegue") else {
                return nil
            }

            headerCell.update(model: dataSource.sectionModels[section].header)

            return headerCell
            //case 4************這版本不顯示***********
//        case 4:
//            guard let headerCell = produceHeaderCell(tapSegueIdentifier: "") else {
//                return nil
//            }
//
//            headerCell.update(model: dataSource.sectionModels[section].header)
//
//            return headerCell
        default:
            return UIView()
        }
    }

    func produceHeaderCell(tapSegueIdentifier: String) -> HomeHeaderCell? {
        guard let headerCell = tableView.dequeueReusableCell(withIdentifier: "HomeHeaderCell") as? HomeHeaderCell else {
            return nil
        }

        if tapSegueIdentifier != "" {
            headerCell.moreButton.rx.tap
                .asObservable()
                .subscribe(onNext: { [weak self] in
                    guard let strongSelf = self else {
                        return
                    }

                    strongSelf.performSegue(withIdentifier: tapSegueIdentifier, sender: headerCell.moreButton)

                }).disposed(by: headerCell.disposeBag)
        } else {
            headerCell.moreButton.isHidden = true
        }

        return headerCell
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 200 / 667 * Global.screenHeight
        case 1, 2, 3:
            return 71 / 667 * Global.screenHeight
        default:
            return 0.001
        }
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0001
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == 4 {
            cell.layoutIfNeeded()
        }
    }
}
