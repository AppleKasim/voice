//
//  HomeHeaderTableViewCell.swift
//  Voice
//
//  Created by kasim on 2018/10/27.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class HomeBannerHeaderCell: UITableViewCell {

    let disposeBag = DisposeBag()

    @IBOutlet weak var pageControl: CustomPageControl!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
