//
//  HomeCollectionTableViewCell.swift
//  Voice
//
//  Created by kasim on 2018/10/28.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

enum CollectionCellType {
    case anchor
    case recommendation
    case audioBooks
}

class HomeCollectionTableViewCell: UITableViewCell {
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.delegate = self
            collectionView.dataSource = self
            // swiftlint:disable line_length
            self.collectionView!.register(UINib(nibName: "AnchorCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "AnchorCollectionViewCell")
            self.collectionView!.register(UINib(nibName: "RecommendationCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "RecommendationCollectionViewCell")
            self.collectionView!.register(UINib(nibName: "AudioBooksCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "AudioBooksCollectionViewCell")
            // swiftlint:enable line_length
        }
    }

    @IBOutlet weak var collectionViewLayout: UICollectionViewFlowLayout!

    var disposeBag = DisposeBag()
    var navigationController: UINavigationController! {
        didSet {
            collectionView.rx
                .itemSelected
                .subscribe(onNext: {[weak self] indexPath in
                    guard let strongSelf = self else {
                        return
                    }

                    let storyboard = UIStoryboard(name: "HomeStoryboard", bundle: nil)

                    switch strongSelf.collectionCellType {
                    case .recommendation:
                        // swiftlint:disable line_length
                        guard let viewController = storyboard.instantiateViewController(withIdentifier: "RecommendationDetailStoryboardId") as? RecommendationDetailViewController else {
                            return
                        }
                        // swiftlint:enable line_length
                        viewController.recommendationModel = strongSelf.recommendationModels[indexPath.row]

                        strongSelf.navigationController.pushViewController(viewController, animated: true)
                    case .audioBooks:
                        // swiftlint:disable line_length
                        guard let viewController = storyboard.instantiateViewController(withIdentifier: "AudioBooksDetail") as? AudioBooksDetailViewController else {
                            return
                        }
                        // swiftlint:enable line_length

                        viewController.audioBook = strongSelf.audioBooks[indexPath.row]

                        strongSelf.navigationController.pushViewController(viewController, animated: true)
                    case .anchor:
                        // swiftlint:disable line_length
                        guard let viewController = storyboard.instantiateViewController(withIdentifier: "AnchorDetailStoryboardId") as? AnchorDetailViewController else {
                            return
                        }
                        // swiftlint:enable line_length
                        viewController.authorId = Int(strongSelf.authors[indexPath.row].authorId)
                        viewController.profileImageUrl = strongSelf.authors[indexPath.row].profileUrl
                        viewController.authorName = strongSelf.authors[indexPath.row].alias

                        strongSelf.navigationController.pushViewController(viewController, animated: true)
                    }
                }).disposed(by: disposeBag)
        }
    }

    var collectionCellType: CollectionCellType = .anchor {
        didSet {
            switch collectionCellType {
            case .recommendation:
                let width = 160 / 375 * Global.screenWidth
                let height = 200 / 667 * Global.screenHeight
                collectionViewLayout.itemSize = CGSize(width: width, height: height)
                collectionViewLayout.minimumLineSpacing = 15 / 375 * Global.screenWidth
                collectionViewLayout.minimumInteritemSpacing = 0
                collectionViewLayout.sectionInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
            case .audioBooks:
                let width = 77 / 375 * Global.screenWidth
                let height = 157 / 667 * Global.screenHeight
                collectionViewLayout.itemSize = CGSize(width: width, height: height)
                collectionViewLayout.minimumLineSpacing = 13 / 375 * Global.screenWidth
                collectionViewLayout.minimumInteritemSpacing = 10 / 667 * Global.screenHeight
                collectionViewLayout.sectionInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
            case .anchor:
                let width = 105 / 375 * Global.screenWidth
                let height = 170 / 667 * Global.screenHeight
                collectionViewLayout.itemSize = CGSize(width: width, height: height)
                collectionViewLayout.minimumLineSpacing = 15 / 375 * Global.screenWidth
                collectionViewLayout.minimumInteritemSpacing = 0
                collectionViewLayout.sectionInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
            }

            collectionView.reloadData()
        }
    }

    var authors: [Author] = [] {
        didSet {
            collectionView.reloadData()
        }
    }

    var recommendationModels: [RecommendationModel] = [] {
        didSet {
            collectionView.reloadData()
        }
    }

    var audioBooks: [AudioBook] = [] {
        didSet {
            collectionView.reloadData()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }
}

extension HomeCollectionTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        switch collectionCellType {
        case .recommendation:
            return recommendationModels.count
        case .anchor:
            return authors.count
        case .audioBooks:
            return audioBooks.count
        }
    }

    // swiftlint:disable line_length
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // swiftlint:enable line_length

        switch collectionCellType {
        case .recommendation:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RecommendationCollectionViewCell", for: indexPath)

            guard let recommendationCell = cell as? RecommendationCollectionViewCell else {
                return cell
            }

            if indexPath.row < recommendationModels.count {
                recommendationCell.introductionLabel.text = recommendationModels[indexPath.row].introduction
                recommendationCell.courseLabel.text = recommendationModels[indexPath.row].course
                recommendationCell.studentsLabel.text = recommendationModels[indexPath.row].students
            }

            return recommendationCell
        case .anchor:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AnchorCollectionViewCell", for: indexPath)

            guard let anchorCell = cell as? AnchorCollectionViewCell else {
                return cell
            }

            if indexPath.row < authors.count {
                anchorCell.update(author: authors[indexPath.row])
            }

            return anchorCell
        case .audioBooks:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AudioBooksCollectionViewCell", for: indexPath)

            guard let audioCell = cell as? AudioBooksCollectionViewCell else {
                return cell
            }

            if indexPath.row < audioBooks.count {
                audioCell.update(model: audioBooks[indexPath.row])
            }

            return audioCell
        }
    }

}

extension HomeCollectionTableViewCell: UICollectionViewDelegate {

}
