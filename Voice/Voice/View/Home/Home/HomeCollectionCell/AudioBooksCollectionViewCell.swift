//
//  AudioBooksCollectionViewCell.swift
//  Voice
//
//  Created by kasim on 2018/12/3.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SDWebImage

class AudioBooksCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var voiceActorLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!

    var disposeBag = DisposeBag()

    override func awakeFromNib() {
        super.awakeFromNib()

    }

    func update(model: AudioBook) {
        nameLabel.text = model.title
        voiceActorLabel.text = "[播] \(model.authors[0].alias)"
        authorLabel.text = "[著] \(model.voiceAuthors[0].alias)"

        coverImageView.sd_setImage(with: URL(string: model.profile), completed: nil)
    }

}
