//
//  RecommendationCollectionViewCell.swift
//  Voice
//
//  Created by kasim on 2018/10/28.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class RecommendationCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var introductionLabel: UILabel!
    @IBOutlet weak var courseLabel: UILabel!
    @IBOutlet weak var studentsLabel: UILabel!
    @IBOutlet weak var favoritesImageView: UIImageView!

    @IBOutlet weak var courseBackgroudView: UIView! {
        didSet {
            courseBackgroudView.backgroundColor = Global.grayColorEFF4FC
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        self.layer.borderWidth = 1.0
        self.layer.borderColor = Global.grayColorEB.cgColor
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        courseBackgroudView.clipsToBounds = true
        courseBackgroudView.layer.cornerRadius = courseBackgroudView.frame.size.height / 2.0
    }

}
