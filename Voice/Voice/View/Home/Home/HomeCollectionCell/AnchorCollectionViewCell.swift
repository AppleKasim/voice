//
//  AnchorCollectionViewCell.swift
//  Voice
//
//  Created by kasim on 2018/10/28.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class AnchorCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var typeImageView: UIImageView!
    
    var disposeBag = DisposeBag()

    override func awakeFromNib() {
        super.awakeFromNib()

    }

    func update(author: Author) {
        nameLabel.text = author.alias
        contentLabel.text = author.introduce

        guard let profileUrl = author.profileUrl else {
            return
        }
        let url = URL(string: profileUrl)
        profileImageView.sd_setImage(with: url, completed: nil)
    }
}
