//
//  RecommendationListViewController.swift
//  Voice
//
//  Created by kasim on 2018/11/4.
//  Copyright © 2018 Shiefu. All rights reserved.
//
import UIKit

class RecommendationListViewController: UIViewController {
    //var recommendationModels: [RecommendationModel] = []

    @IBOutlet weak var segmentViewHieght: NSLayoutConstraint!
    @IBOutlet weak var segmentView: CustomSegmentView! {
        didSet {
            segmentView.backgroundColor = Global.grayColorF5
        }
    }
    
    var pageContainerViewController: PageContainerViewController!

    override func viewDidLoad() {
        super.viewDidLoad()

        let viewModel = RecommendationListViewModel()
        let titles = viewModel.titles

        let configure = SegmentConfigure(textSelectColor: UIColor.black, highlightColor: Global.grayColorF5, titles: titles)

        let multiplier = 45 / 603 * Global.notNavigationScreenHeight
        segmentViewHieght.constant = multiplier
        self.view.layoutIfNeeded()

        segmentView.scrollViewFrame = CGRect(x: 0, y: 0, width: Global.screenWidth, height: multiplier)

        segmentView.configure = configure

        segmentView?.setScrollClosure { [weak self] (index) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.pageContainerViewController.showPage(byIndex: index)

            let point = CGPoint(x: CGFloat(index) * Global.screenWidth, y: 0)
            strongSelf.segmentView?.segmentWillMove(point: point)
            strongSelf.segmentView?.segmentDidEndMove(point: point)
        }

        pageContainerViewController.changeTabForMainViewController = {[weak self] index in
            guard let strongSelf = self else {
                return
            }
            strongSelf.changeTab(byIndex: index)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ContainerViewSegue",
            let destination = segue.destination as? PageContainerViewController {
            pageContainerViewController = destination
            pageContainerViewController.subPageContainerMode = .recommendationList
            //pageContainerViewController.mainViewController = self
        }
    }

    func changeTab(byIndex index: Int) {
        let point = CGPoint(x: CGFloat(index) * Global.screenWidth, y: 0)
        segmentView?.segmentWillMove(point: point)
        segmentView?.segmentDidEndMove(point: point)
    }
}
