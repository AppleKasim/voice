//
//  AudioBooksListViewController.swift
//  Voice
//
//  Created by kasim on 2019/1/7.
//  Copyright © 2019 Shiefu. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxDataSources

class AudioBooksListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    // swiftlint:disable line_length
    private lazy var dataSource = RxTableViewSectionedReloadDataSource<SingleSectionModel<String, AudioBook>>(configureCell: configureCell)

    private lazy var configureCell: RxTableViewSectionedReloadDataSource<SingleSectionModel<String, AudioBook>>.ConfigureCell = { [weak self] (dataSource, tableView, indexPath, element) in

        guard let strongSelf = self,
            let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "AudioBookListCell") as? AudioBookListCell else {
                return UITableViewCell()
        }

        tableViewCell.update(audioBook: element)

        return tableViewCell
    }
    // swiftlint:enable line_length

    var audioBooks: [AudioBook] = []
    var viewModel = AudioBookListViewModel()
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.setItems(audioBooks: audioBooks)

        tableView.rx.setDelegate(self)
            .disposed(by: disposeBag)

        viewModel.items
            .bind(to: tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)

        tableView.rx.itemSelected
            .subscribe(onNext: { [weak self] indexPath in
                guard let strongSelf = self else { return }
                // swiftlint:disable line_length
                let storyboard = UIStoryboard(name: "HomeStoryboard", bundle: nil)
                guard let viewController = storyboard.instantiateViewController(withIdentifier: "AudioBooksDetail") as? AudioBooksDetailViewController else {
                    return
                }
                // swiftlint:enable line_length

                viewController.audioBook = strongSelf.audioBooks[indexPath.row]

                strongSelf.navigationController?.pushViewController(viewController, animated: true)
            })
            .disposed(by: disposeBag)
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.tabBarController?.tabBar.isHidden = false
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AudioBooksListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120 / 667 * Global.screenHeight
    }
}
