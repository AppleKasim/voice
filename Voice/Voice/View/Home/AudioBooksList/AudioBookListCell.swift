//
//  AudioBookListCell.swift
//  Voice
//
//  Created by kasim on 2019/1/8.
//  Copyright © 2019 Shiefu. All rights reserved.
//

import UIKit

class AudioBookListCell: UITableViewCell {

    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.textColor = Global.yellowColorF8BF00
        }
    }

    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var introductionLabel: UILabel! {
        didSet {
            introductionLabel.textColor = Global.grayColor81878E
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func update(audioBook: AudioBook) {
        titleLabel.text = audioBook.title
        authorLabel.text = "[播]\(audioBook.voiceAuthors[0].alias)  [著]\(audioBook.authors[0].alias)"
        let url = URL(string: audioBook.profile)
        coverImageView.sd_setImage(with: url, completed: nil)

        introductionLabel.text = audioBook.introduce

    }

}
