//
//  AnchorDetailViewController.swift
//  Voice
//
//  Created by kasim on 2018/10/28.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class AnchorDetailViewController: UIViewController {
    private var currentPageIndex = BehaviorRelay(value: 0)
    var pageContainerViewController: PageContainerViewController!

    let disposeBag = DisposeBag()

    var isAttentaion = false {
        didSet {
            if isAttentaion {
                attentionButton.backgroundColor = Global.redColorFF415F
                attentionButton.setTitle("已關注主播", for: .normal)
            } else {
                attentionButton.setTitle("關注主播", for: .normal)
                attentionButton.backgroundColor = Global.redColorE3203E
            }
        }
    }

    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var contentTextView: UITextView! {
        didSet {
            contentTextView.textColor = Global.grayColorB2
        }
    }

    @IBOutlet weak var attentionButton: UIButton! {
        didSet {
            attentionButton.tintColor = .white
            attentionButton.backgroundColor = Global.redColorE3203E
            attentionButton.setTitle("關注主播", for: .normal)
        }
    }

    @IBOutlet weak var attentaionNumberLabel: UILabel!

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var customSegmentedControl: CustomSegmentedControl! {
        didSet {
            customSegmentedControl.titles = viewModel.titles
        }
    }
    var segmentedControl: UISegmentedControl {
        return customSegmentedControl.segmentedControl
    }

    let viewModel = AuthorDetailViewModel()
    var authorId: Int!
    var authorName: String = ""
    var profileImageUrl: String?
    var segmentRect = BehaviorRelay<CGRect>(value: CGRect())

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = authorName

        if let urlString = profileImageUrl {
            let profileUrl = URL(string: urlString)
            profileImageView.sd_setImage(with: profileUrl, completed: nil)
        }

        pageContainerViewController.changeTabForMainViewController = { [weak self] index in
            guard let strongSelf = self else {
                return
            }

            strongSelf.currentPageIndex.accept(index)
        }

        currentPageIndex.asObservable()
            .subscribe(onNext: {[weak self] value in
                guard let strongSelf = self else {
                    return
                }
                strongSelf.segmentedControl.selectedSegmentIndex = value
                strongSelf.segmentedControl.sendActions(for: UIControlEvents.valueChanged)
                if value == 0 {
                    strongSelf.tabBarController?.tabBar.isHidden = true
                } else {
                    strongSelf.tabBarController?.tabBar.isHidden = false
                }

            })
            .disposed(by: disposeBag)

        segmentedControl.rx.selectedSegmentIndex
            .subscribe(onNext: {[weak self] index in
                guard let strongSelf = self else {
                    return
                }
                strongSelf.pageContainerViewController.showPage(byIndex: index)
            })
            .disposed(by: disposeBag)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AnchorDetailToContainer",
            let destination = segue.destination as? PageContainerViewController {
            pageContainerViewController = destination

            pageContainerViewController.scrollView = scrollView
            pageContainerViewController.segmentRect = segmentRect
            pageContainerViewController.subPageContainerMode = .anchorDetail(authorId: authorId)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
//        attentionButton.layer.cornerRadius = attentionButton.frame.size.height / 2.0

        segmentRect.accept(view.convert(customSegmentedControl.frame, to: scrollView))

        customSegmentedControl.updateLineWidth()
    }
}
