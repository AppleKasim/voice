//
//  AudioBooksDetailViewController.swift
//  Voice
//
//  Created by kasim on 2018/12/4.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class AudioBooksDetailViewController: UIViewController {

    //top view
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var coverImageView: UIImageView! {
        didSet {
            coverImageView.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.textColor = Global.yellowColorF8BF00
        }
    }
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var voiceAuthorLabel: UILabel!
    @IBOutlet weak var peopleLabel: UILabel! {
        didSet {
            peopleLabel.textColor = Global.grayColorB2
            peopleLabel.isHidden = true
        }
    }
    @IBOutlet weak var endLabel: UILabel! {
        didSet {
            endLabel.textColor = .white
            endLabel.backgroundColor = Global.yellowColorF8BF00
            endLabel.isHidden = true
        }
    }
    @IBOutlet weak var classifictaionTitleLabel: UILabel! {
        didSet {
            classifictaionTitleLabel.isHidden = true
        }
    }
    @IBOutlet weak var classifictaionLabel: UILabel! {
        didSet {
            classifictaionLabel.textColor = Global.grayColor81878E
            classifictaionLabel.isHidden = true
        }
    }
    @IBOutlet weak var chapterTitleLabel: UILabel! {
        didSet {
            chapterTitleLabel.isHidden = true
        }
    }
    @IBOutlet weak var chapterLabel: UILabel! {
        didSet {
            chapterLabel.textColor = Global.grayColor81878E
            chapterLabel.isHidden = true
        }
    }
    @IBOutlet weak var playButton: UIButton! {
        didSet {
            playButton.imageView?.contentMode = .scaleAspectFit
            playButton.setTitleColor(.black, for: .normal)
            playButton.set(titleLocation: .bottom, padding: 0, state: .normal)
            playButton.isHidden = true
        }
    }
    @IBOutlet weak var keepButton: UIButton! {
        didSet {
            keepButton.imageView?.contentMode = .scaleAspectFit
            keepButton.setTitleColor(.black, for: .normal)
            keepButton.set(titleLocation: .bottom, padding: 0, state: .normal)
            keepButton.isHidden = true
        }
    }

    @IBOutlet weak var gapView: UIView! {
        didSet {
            gapView.backgroundColor = Global.grayColorD9
        }
    }

    @IBOutlet weak var scrollView: UIScrollView!

    private var currentPageIndex = BehaviorRelay(value: 0)
    var pageContainerViewController: PageContainerViewController!

    @IBOutlet weak var customSegmentedControl: CustomSegmentedControl! {
        didSet {
            customSegmentedControl.titles = viewModel.titles
        }
    }
    private var segmentedControl: UISegmentedControl {
        return customSegmentedControl.segmentedControl
    }

    var audioBook: AudioBook!
    var segmentRect = BehaviorRelay<CGRect>(value: CGRect())
    let viewModel = AudioBookDetailViewModel()
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = audioBook.title

        let coverUrl = URL(string: audioBook.profile)
        coverImageView.sd_setImage(with: coverUrl, completed: nil)
        titleLabel.text = audioBook.title
        authorLabel.text = "[著] \(audioBook.authors[0].alias)"
        voiceAuthorLabel.text = "[播] \(audioBook.voiceAuthors[0].alias)"

        pageContainerViewController.changeTabForMainViewController = { [weak self] index in
            guard let strongSelf = self else {
                return
            }

            strongSelf.currentPageIndex.accept(index)
        }

        currentPageIndex.asObservable()
            .subscribe(onNext: {[weak self] value in
                guard let strongSelf = self else {
                    return
                }
                strongSelf.segmentedControl.selectedSegmentIndex = value
                strongSelf.segmentedControl.sendActions(for: UIControlEvents.valueChanged)
                if value == 0 {
                    strongSelf.tabBarController?.tabBar.isHidden = true
                } else {
                    strongSelf.tabBarController?.tabBar.isHidden = false
                }

            })
            .disposed(by: disposeBag)

        segmentedControl.rx.selectedSegmentIndex
            .subscribe(onNext: {[weak self] index in
                guard let strongSelf = self else {
                    return
                }
                strongSelf.pageContainerViewController.showPage(byIndex: index)
            })
            .disposed(by: disposeBag)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AudioBookDetailToContainer",
            let destination = segue.destination as? PageContainerViewController {
            pageContainerViewController = destination

            pageContainerViewController.scrollView = scrollView

//            scrollView.rx.contentOffset
//                .subscribe { [weak self] in
//                }.disposed(by: disposeBag)

            guard let albumId = Int(audioBook.audioBooksId) else {
                return
            }
            pageContainerViewController.segmentRect = segmentRect
            pageContainerViewController.subPageContainerMode = .audioBookDetail(albumId: albumId)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        endLabel.layer.cornerRadius = endLabel.frame.height / 2.0

        segmentRect.accept(view.convert(customSegmentedControl.frame, to: scrollView))

        customSegmentedControl.updateLineWidth()
    }

}
