//
//  PageContainerViewModel.swift
//  Voice
//
//  Created by kasim on 2018/11/18.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol SubPageContainer {
    mutating func updateItem()
    func cellHeight() -> CGFloat
}

enum SubPageContainerMode {
    case recommendationList
    case recommendationDetail
    case anchorDetail(authorId: Int)
    case audioBookDetail(albumId: Int)

    func getViewModel() -> SubPageContainer? {
        switch self {
        case .recommendationList:
            var pageContainer = RecommendationListViewModel()
            pageContainer.updateItem()
            return pageContainer
        case .recommendationDetail:
            var recommendationDetailViewModel = RecommendationDetailViewModel()
            recommendationDetailViewModel.updateItem()
            return recommendationDetailViewModel
        case .anchorDetail(let authorId):
            let anchorDetailViewModel = AuthorDetailViewModel()
            anchorDetailViewModel.authorId = authorId
            anchorDetailViewModel.updateItem()
            return anchorDetailViewModel
        case .audioBookDetail(let albumId):
            let audioBookDetailViewModel = AudioBookDetailViewModel()
            audioBookDetailViewModel.albumId = albumId
            audioBookDetailViewModel.updateItem()
            return audioBookDetailViewModel
        }
    }
}
