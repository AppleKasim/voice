//
//  PageViewController.swift
//  Voice
//
//  Created by kasim on 2018/11/10.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class PageContainerViewController: UIPageViewController {

    lazy var orderedViewControllers: [UIViewController] = {
        switch viewModel {
        case let recommendationListViewModel as RecommendationListViewModel:
            let pageContentViewControllerArray =  recommendationListViewModel.titles.map { title -> PageContentViewController in
                guard let pageContent = self.storyboard!.instantiateViewController(withIdentifier: "PageContent") as? PageContentViewController else {
                    return PageContentViewController()
                }
                pageContent.viewModel = recommendationListViewModel
                pageContent.pageTitle = title
                return pageContent
            }
            return pageContentViewControllerArray

        case let recommendationDetailViewModel as RecommendationDetailViewModel:
            var viewControllers: [UIViewController] = []

            guard let textPageContent = self.storyboard!.instantiateViewController(withIdentifier: "TextPageContent") as? TextPageContentViewController else {
                return []
            }
            textPageContent.viewModel = recommendationDetailViewModel

            guard let playListPageContent = self.storyboard!.instantiateViewController(withIdentifier: "PlayListPageContent") as? ListPageContentViewController else {
                return []
            }
            playListPageContent.viewModel = recommendationDetailViewModel

            viewControllers.append(textPageContent)
            viewControllers.append(playListPageContent)

            return viewControllers

        case let authorDetailViewModel as AuthorDetailViewModel:
            var viewControllers: [UIViewController] = []

            guard let playListPageContent = self.storyboard!.instantiateViewController(withIdentifier: "PlayListPageContent") as? ListPageContentViewController else {
                return []
            }
            playListPageContent.viewModel = authorDetailViewModel
            playListPageContent.scrollView = scrollView
            playListPageContent.segmentRect = segmentRect

            guard let textPageContent = self.storyboard!.instantiateViewController(withIdentifier: "TextPageContent") as? TextPageContentViewController else {
                return []
            }
            textPageContent.viewModel = authorDetailViewModel
            textPageContent.scrollView = scrollView
            textPageContent.segmentRect = segmentRect

            viewControllers.append(playListPageContent)
            viewControllers.append(textPageContent)

            return viewControllers

        case let audioBookDetailViewModel as AudioBookDetailViewModel:
            var viewControllers: [UIViewController] = []

            guard let textPageContent = self.storyboard!.instantiateViewController(withIdentifier: "TextPageContent") as? TextPageContentViewController else {
                return []
            }
            textPageContent.viewModel = audioBookDetailViewModel
            textPageContent.scrollView = scrollView
            textPageContent.segmentRect = segmentRect

            guard let playListPageContent = self.storyboard!.instantiateViewController(withIdentifier: "PlayListPageContent") as? ListPageContentViewController else {
                return []
            }
            playListPageContent.viewModel = audioBookDetailViewModel
            playListPageContent.scrollView = scrollView
            playListPageContent.segmentRect = segmentRect

            viewControllers.append(textPageContent)
            viewControllers.append(playListPageContent)

            return viewControllers
        default:
            return []
        }
    }()

    var currentPageIndex = 0
    var willTransitionTo: UIViewController!
    var scrollView: UIScrollView!
    var customSegmentedControl: CustomSegmentedControl!
    var segmentRect = BehaviorRelay<CGRect>(value: CGRect())
    var albumId: Int!

    var changeTabForMainViewController: ((_ index: Int) -> Void)?
    var viewModel: SubPageContainer!
    var subPageContainerMode: SubPageContainerMode! {
        didSet {
            viewModel = subPageContainerMode.getViewModel()
        }
    }

    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.dataSource = self
        self.delegate = self

        setViewControllers([orderedViewControllers[0]], direction: .forward, animated: true, completion: nil)
    }

    func showPage(byIndex index: Int) {
        let viewController = orderedViewControllers[index]
        var direction: UIPageViewController.NavigationDirection = .forward
        if currentPageIndex > index {
            direction = .reverse
        }
        setViewControllers([viewController], direction: direction, animated: true, completion: nil)
        currentPageIndex = index
    }

}

extension PageContainerViewController: UIPageViewControllerDataSource {
    // swiftlint:disable line_length
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let index = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        let previousIndex = index - 1
        guard previousIndex >= 0 && previousIndex < orderedViewControllers.count else {
            return nil
        }
        return orderedViewControllers[previousIndex]
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let index = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        let nextIndex = index + 1
        guard nextIndex >= 0 && nextIndex < orderedViewControllers.count else {
            return nil
        }
        return orderedViewControllers[nextIndex]
    }
    // swiftlint:enable line_length
}

// swiftlint:disable line_length
extension PageContainerViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        self.willTransitionTo = pendingViewControllers.first

    }

    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers previousVieControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            guard let index = orderedViewControllers.index(of: self.willTransitionTo) else { return }
            let previousViewController = previousVieControllers.first!
            let previousIndex = orderedViewControllers.index(of: previousViewController)
            if index != previousIndex {
                guard let changeTab = changeTabForMainViewController else {
                    return
                }
                changeTab(index)
                self.currentPageIndex = index
            }
        }
    }
}
// swiftlint:enable line_length
