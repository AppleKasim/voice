//
//  ClassificationCell.swift
//  Voice
//
//  Created by kasim on 2018/11/4.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit

class ClassificationCell: UITableViewCell {

    @IBOutlet weak var backgroundimageView: UIImageView!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var englishNameLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
