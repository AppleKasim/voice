//
//  ClassificationViewController.swift
//  Voice
//
//  Created by kasim on 2018/11/4.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

class ClassificationViewController: UIViewController {

    let viewModel = ClassificationViewModel()
    let disposeBag = DisposeBag()

    @IBOutlet weak var tableView: UITableView!

    private lazy var dataSource = RxTableViewSectionedReloadDataSource<SectionOfClassification>(configureCell: configureCell)

    private lazy var configureCell: RxTableViewSectionedReloadDataSource<SectionOfClassification>.ConfigureCell = { [weak self] (dataSource, tableView, indexPath, data) in

        guard let strongSelf = self else {
            return UITableViewCell()
        }

        guard let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "ClassificationCell") as? ClassificationCell else {
            return UITableViewCell()
        }

        tableViewCell.contentLabel.text = data.content
        tableViewCell.nameLabel.text = data.name
        tableViewCell.englishNameLabel.text = data.englishName

        return tableViewCell
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.items
            .bind(to: tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)

        tableView.rx.setDelegate(self)
            .disposed(by: disposeBag)

        tableView.rx.itemSelected
            .subscribe(onNext: { [weak self] indexPath in
                guard let strongSelf = self else {
                    return
                }
                strongSelf.performSegue(withIdentifier: "ToRecommendationListSegue", sender: indexPath)
            }).disposed(by: disposeBag)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ToRecommendationListSegue" {
            guard let viewController = segue.destination as? RecommendationListViewController,
                let indexPath = sender as? IndexPath else {
                    return
            }
            //viewController.sss = String(indexPath.row)
        }
    }
}

extension ClassificationViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
}
