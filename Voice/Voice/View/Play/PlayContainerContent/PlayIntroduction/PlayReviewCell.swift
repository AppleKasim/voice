//
//  PlayReviewCell.swift
//  Voice
//
//  Created by kasim on 2018/11/21.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit

class PlayReviewCell: UITableViewCell {
    @IBOutlet weak var dotImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.textColor = Global.blueColorADCAFF
        }
    }
    @IBOutlet weak var dateLabel: UILabel! {
        didSet {
            dateLabel.textColor = Global.grayColorB2
        }
    }
    @IBOutlet weak var contentLabel: UILabel! {
        didSet {
            contentLabel.textColor = .white
        }
    }
    @IBOutlet weak var profileImageView: UIImageView! {
        didSet {
            profileImageView.layer.cornerRadius = (20 / 375 * Global.screenWidth) / 2
        }
    }

    @IBOutlet weak var cellBackgroundView: UIView! {
        didSet {
            cellBackgroundView.backgroundColor = Global.blackColor121724
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        self.backgroundColor = Global.blackColor121724
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func update(model: PlayReviewModel) {
        nameLabel.text = model.name
        dateLabel.text = model.date
        contentLabel.text = model.content
    }
}
