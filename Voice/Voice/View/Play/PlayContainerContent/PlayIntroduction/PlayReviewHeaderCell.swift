//
//  PlayReviewHeaderCell.swift
//  Voice
//
//  Created by kasim on 2018/12/14.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit

class PlayReviewHeaderCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.textColor = .white
        }
    }
    
    @IBOutlet weak var lineView: UIView! {
        didSet {
            lineView.backgroundColor = Global.grayColor70
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func update(content: String) {
        titleLabel.text = content
    }
}
