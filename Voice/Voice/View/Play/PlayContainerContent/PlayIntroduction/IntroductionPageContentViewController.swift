//
//  PlayIntroductionPageContentViewController.swift
//  Voice
//
//  Created by kasim on 2018/11/21.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

class IntroductionPageContentViewController: UIViewController {

    // swiftlint:disable line_length
    private lazy var dataSource = RxTableViewSectionedReloadDataSource<SingleSectionModel<String, PlayIntroductionType>>(configureCell: configureCell)

    private lazy var configureCell: RxTableViewSectionedReloadDataSource<SingleSectionModel<String, PlayIntroductionType>>.ConfigureCell = { [weak self] (dataSource, tableView, indexPath, element) in

        guard let strongSelf = self else {
            return UITableViewCell()
        }

        guard let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "PlayReviewCell") as? PlayReviewCell else {
            return UITableViewCell()
        }

        guard let playReviewModel = element as? PlayReviewModel else {
            return UITableViewCell()
        }

        tableViewCell.update(model: playReviewModel)

        return tableViewCell
    }
    // swiftlint:enable line_length

    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.backgroundColor = Global.blackColor121724

            tableView.estimatedSectionHeaderHeight = 36
            tableView.estimatedRowHeight = 20.0
            tableView.rowHeight = UITableView.automaticDimension
        }
    }

    @IBOutlet weak var bottomView: UIView! {
        didSet {
            bottomView.isHidden = true
        }
    }

    var viewModel = PlayViewModel()
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.rx.setDelegate(self)
            .disposed(by: disposeBag)

        viewModel.reviewItems
            .asDriver()
            .drive(tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)

        viewModel.updateItem()
        //viewModel.updatePlayModel()
    }
}

extension IntroductionPageContentViewController: UITableViewDelegate {
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableView.automaticDimension
//    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        switch section {
        case 0:
            guard let headerCell = tableView.dequeueReusableCell(withIdentifier: "PlayIntroductionCell") as? PlayIntroductionCell else {
                return nil
            }

            headerCell.update(content: dataSource.sectionModels[0].header)

            return headerCell
        case 1:
            guard let headerCell = tableView.dequeueReusableCell(withIdentifier: "PlayReviewHeaderCell") as? PlayReviewHeaderCell else {
                return nil
            }

            headerCell.update(content: dataSource.sectionModels[1].header)

            return headerCell
        default:
            return nil
        }
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 1:
            return 24 / 667 * Global.screenHeight
        default:
            return UITableView.automaticDimension
        }
    }

}
