//
//  PlayIntroductionCell.swift
//  Voice
//
//  Created by kasim on 2018/11/21.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit

class PlayIntroductionCell: UITableViewCell {

    @IBOutlet weak var contentLabel: UILabel! {
        didSet {
            contentLabel.textColor = .white
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()

        self.backgroundColor = Global.blackColor121724
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func update(content: String) {
        contentLabel.text = content
    }
}
