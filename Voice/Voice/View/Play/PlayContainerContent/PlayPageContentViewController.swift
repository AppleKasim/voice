//
//  PlayPageContentViewController.swift
//  Voice
//
//  Created by kasim on 2018/11/21.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import AVFoundation
import MediaPlayer

class PlayPageContentViewController: UIViewController {
    @IBOutlet weak var coverBackgroundView: UIView! {
        didSet {
            coverBackgroundView.backgroundColor = .clear
            coverBackgroundView.layer.borderColor = Global.grayColor70.cgColor
            coverBackgroundView.layer.borderWidth = 1
        }
    }
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var playButton: UIButton! {
        didSet {
            PlayManager.share().isPlaying
                .subscribe(onNext: { isPlaying in
                    DispatchQueue.main.async { [weak self] in
                        guard let strongSelf = self else { return }
                        if isPlaying {
                            strongSelf.playButton.setImage(UIImage(named: "bigPause"), for: .normal)
                        } else {
                            strongSelf.playButton.setImage(UIImage(named: "bigPlay"), for: .normal)
                        }
                    }
                })
                .disposed(by: disposeBag)
        }
    }
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.textColor = .white
        }
    }

    @IBOutlet weak var endLabel: UILabel! {
        didSet {
            endLabel.textColor = .white
            endLabel.backgroundColor = Global.yellowColorF8BF00
            endLabel.text = "已完結"
            endLabel.isHidden = true
        }
    }

    @IBOutlet weak var keepButton: UIButton! {
        didSet {
            keepButton.imageView?.contentMode = .scaleAspectFit
            keepButton.setTitleColor(Global.grayColor81878E, for: .normal)
            keepButton.set(titleLocation: .bottom, padding: 0, state: .normal)
            keepButton.isEnabled = false
        }
    }
    @IBOutlet weak var timingButton: UIButton! {
        didSet {
            timingButton.imageView?.contentMode = .scaleAspectFit
            timingButton.setTitleColor(Global.grayColor81878E, for: .normal)
            timingButton.set(titleLocation: .bottom, padding: 0, state: .normal)
            timingButton.isEnabled = false
        }
    }
    @IBOutlet weak var speedButton: UIButton! {
        didSet {
            speedButton.imageView?.contentMode = .scaleAspectFit
            speedButton.setTitleColor(Global.grayColor81878E, for: .normal)
            speedButton.set(titleLocation: .bottom, padding: 0, state: .normal)
            speedButton.isEnabled = false
        }
    }
    @IBOutlet weak var courseListButton: UIButton! {
        didSet {
            courseListButton.imageView?.contentMode = .scaleAspectFit
            courseListButton.setTitleColor(Global.grayColor81878E, for: .normal)
            courseListButton.set(titleLocation: .bottom, padding: 0, state: .normal)
        }
    }
    @IBOutlet weak var playbackSlider: UISlider! {
        didSet {
            playbackSlider.minimumTrackTintColor = Global.yellowColorF8BF00
            playbackSlider.maximumTrackTintColor = Global.grayColorB2
            playbackSlider.thumbTintColor = Global.yellowColorF8BF00

            playbackSlider.rx.controlEvent(.touchDragInside)
                .subscribe(onNext: { [weak self] in
                    guard let strongSelf = self else { return }

                    let time = strongSelf.playbackSlider.value
                    strongSelf.currenyTimeLabel.text = strongSelf.getTime(byFloat64: Float64(time))

                }).disposed(by: disposeBag)
        }
    }

    @IBOutlet weak var currenyTimeLabel: UILabel!
    @IBOutlet weak var endTimeLabel: UILabel!

    @IBOutlet weak var playStackView: UIStackView! {
        didSet {
            playStackView.spacing = 5 / 375 * Global.screenWidth
        }
    }
    @IBOutlet weak var previousButton: UIButton!
    @IBOutlet weak var stopButton: UIButton! {
        didSet {
            PlayManager.share().isPlaying.asDriver()
                .drive(onNext: { [weak self] isPlaying in
                    guard let strongSelf = self else { return }
                    if isPlaying {
                        strongSelf.stopButton.setImage(UIImage(named: "playPause"), for: .normal)
                    } else {
                        strongSelf.stopButton.setImage(UIImage(named: "playArrow"), for: .normal)
                    }
                })
                .disposed(by: disposeBag)
        }
    }
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var bottomView: UIView! {
        didSet {
            bottomView.isHidden = true
        }
    }

    var isFirst = true
    var playTransitionManager = PlayTransitionManager()
    var currentPlayIndex = 0
    var disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

//        PlayManager.share().isPlayNextSong = true

        view.backgroundColor = Global.blackColor121724

        PlayManager.share().currentVoice.asDriver()
            .drive(onNext: { [weak self] item in
                guard let strongSelf = self else { return }
                strongSelf.titleLabel.text = item?.title

        }).disposed(by: disposeBag)

        PlayManager.share().periodicTimeAction = { cmTime in
            if PlayManager.share().player?.currentItem?.status == .readyToPlay {
                //更新进度条进度值
                let currentTime = CMTimeGetSeconds(cmTime)
                self.playbackSlider!.value = Float(currentTime)
                self.currenyTimeLabel!.text = self.getTime(byFloat64: currentTime)
            }
        }

        PlayManager.share().itemDuration.asDriver()
            .drive(onNext: { [weak self] duration in
                guard let strongSelf = self,
                    let strongDuration = duration else { return }
                let seconds: Float64 = CMTimeGetSeconds(strongDuration)

                strongSelf.playbackSlider.minimumValue = 0
                strongSelf.playbackSlider.maximumValue = Float(seconds)
                strongSelf.playbackSlider.isContinuous = false
                strongSelf.endTimeLabel.text = strongSelf.getTime(byFloat64: seconds)

                if strongSelf.isFirst {
                    let playTime = PlayManager.share().getCurrentPlayTime()
                    let seconds = CMTimeGetSeconds(playTime)
                    strongSelf.playbackSlider.value = Float(seconds)
                    strongSelf.isFirst = false
                }
            })
            .disposed(by: disposeBag)

        PlayManager.share().playIntroduction.asDriver()
            .drive(onNext: { [weak self] item in
                guard let strongSelf = self,
                    let strongItem = item else { return }

                let url = URL(string: strongItem.cover)
                strongSelf.coverImageView.sd_setImage(with: url, completed: nil)
            })
            .disposed(by: disposeBag)

        playbackSlider.rx.value
            .subscribe(onNext: { value in
                if PlayManager.share().voiceList.count > 0 {
                    let seconds: Int64 = Int64(value)
                    let targetTime: CMTime = CMTimeMake(value: seconds, timescale: 1)
                    //播放器定位到对应的位置
                    let playTime = PlayManager.share().getCurrentPlayTime()
                    let seconds1 = CMTimeGetSeconds(playTime)
                    if Int(targetTime.value) != Int(seconds1) {
                        //第一次進入PlayView時，如果在播放中，在執行一次目前的播放位置，會有卡頓的問題．故如果一樣的值，就不再設定一次
                        PlayManager.share().player?.seek(to: targetTime)
                    }

            //****去修改MPNowPlayingInfoCenter的slider 但是沒有，有空需修改這bug
                    var dic = MPNowPlayingInfoCenter.default().nowPlayingInfo
                    dic?[MPNowPlayingInfoPropertyElapsedPlaybackTime] = NSNumber(value: CMTimeGetSeconds(targetTime))

                    let isStop = PlayManager.share().player?.rate == 0
                    if isStop {
                        PlayManager.share().player?.play()
                        //strongSelf.playButton.setTitle("暂停", for: .normal)
                    }
                }
            }).disposed(by: disposeBag)
    }

    @IBAction func playButton(_ sender: UIButton) {
        if PlayManager.share().player != nil
            && PlayManager.share().player!.timeControlStatus == .playing {
            PlayManager.share().pause()
        } else {
            PlayManager.share().play()
        }
    }

    @IBAction func underPlayButton(_ sender: UIButton) {
        if PlayManager.share().player != nil
            && PlayManager.share().player!.timeControlStatus == .playing {
            PlayManager.share().pause()
        } else {
            PlayManager.share().play()
        }
    }

    @IBAction func stop(_ sender: UIButton) {
        PlayManager.share().pause()
    }

    @IBAction func previous(_ sender: UIButton) {
        PlayManager.share().last()
    }

    @IBAction func next(_ sender: UIButton) {
        PlayManager.share().next()
    }

    func getTime(byFloat64 float64: Float64) -> String {
        let allTime: Int = Int(float64)
        let hour: Int = allTime / 3600
        let minute: Int = Int(allTime / 60)
        let second: Int = allTime % 60
        var time: String = ""
        if hour > 0 {
            time += "\(hour):"
        }
        if minute < 10 {
            time += "0\(minute):"
        } else {
            time += "\(minute)"
        }
        if second < 10 {
            time += "0\(second)"
        } else {
            time += "\(second)"
        }

        return time
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PlayTimingSegue",
            let playTimingViewController = segue.destination as? PlayTimingViewController {
            playTimingViewController.transitioningDelegate = playTransitionManager
            playTransitionManager.delegate = self
        } else if segue.identifier == "ToPlayListSegue",
            let playListViewController = segue.destination as? PlayListViewController {
            playListViewController.transitioningDelegate = playTransitionManager
            playTransitionManager.delegate = self
        }
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        endLabel.layer.masksToBounds = true
        endLabel.layer.cornerRadius = endLabel.frame.height / 2.0
    }
}

extension PlayPageContentViewController: TransitionManagerDelegate {
    func dismiss() {
        dismiss(animated: true, completion: nil)
    }
}
