//
//  PlayViewController.swift
//  Voice
//
//  Created by kasim on 2018/11/13.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class PlayViewController: UIViewController {
    //要移到viewModel
    private var currentPageIndex = BehaviorRelay(value: 0)
    var playeContainerViewController: PlayContainerViewController!
    var interactor: Interactor?
    let reportInteractor = Interactor()

    @IBOutlet weak var dismissButton: UIButton! {
        didSet {
            dismissButton.imageView?.contentMode = .scaleAspectFit
        }
    }

    //top view
    @IBOutlet weak var profileImageView: UIImageView! {
        didSet {
            profileImageView.isHidden = true
        }
    }
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.text = ""
            nameLabel.textColor = .white
        }
    }
    @IBOutlet weak var attentionButton: UIButton! {
        didSet {
            attentionButton.tintColor = .white
            attentionButton.backgroundColor = Global.redColorE3203E
            attentionButton.setTitle("關注主播", for: .normal)
            attentionButton.isHidden = true
        }
    }
    @IBOutlet weak var pageControl: UIPageControl! {
        didSet {
            pageControl.pageIndicatorTintColor = Global.grayColor4F5358
            pageControl.currentPageIndicatorTintColor = Global.yellowColorF8BF00
        }
    }

    @IBOutlet weak var reportButton: UIButton! {
        didSet {
            reportButton.setTitleColor(.white, for: .normal)
        }
    }

    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        setUnderNavigationBackButton()

        view.backgroundColor = Global.blackColor121724
        navigationController?.navigationBar.barTintColor = Global.blackColor121724
        navigationController?.navigationBar.isTranslucent = false

        playeContainerViewController.changePageForMainViewController = {[weak self] index in
            guard let strongSelf = self else {
                return
            }

            strongSelf.currentPageIndex.accept(index)
        }

        currentPageIndex.asObservable()
            .bind(to: pageControl.rx.currentPage)
            .disposed(by: disposeBag)

        guard PlayManager.share().playingIndex != nil,
            PlayManager.share().albumId != nil else {
                showNoDateAlert()
                return
        }

        PlayManager.share().playIntroduction.asDriver()
            .drive(onNext: { [weak self] item in
                guard let strongSelf = self else { return }
                strongSelf.nameLabel.text = item?.title
            }).disposed(by: disposeBag)

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PlayViewToContainer",
            let destination = segue.destination as? PlayContainerViewController {
            playeContainerViewController = destination
        } else if segue.identifier == "ToReportPage",
            let destination = segue.destination as? ReportViewController {

            destination.transitioningDelegate = self
            destination.interactor = self.reportInteractor
        }
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        profileImageView.layer.cornerRadius = profileImageView.frame.height / 2.0
        attentionButton.layer.cornerRadius = attentionButton.frame.height / 2.0
    }

    @IBAction func dismissAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func report(_ sender: UIButton) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            let action = UIAlertAction(title: "檢舉", style: .default) { [weak self] _ in
                guard let strongSelf = self else { return }
                strongSelf.performSegue(withIdentifier: "ToReportPage", sender: nil)

            }

            let cancel = UIAlertAction(title: "取消", style: .cancel)

            alert.addAction(action)
            alert.addAction(cancel)

            alert.popoverPresentationController?.sourceView = sender
            alert.popoverPresentationController?.sourceRect = sender.bounds

            self.present(alert, animated: true, completion: nil)
        }
    }

    @IBAction func downSliding(_ sender: UIPanGestureRecognizer) {

        let percentThreshold: CGFloat = 0.3
        // convert y-position to downward pull progress (percentage)
        let translation = sender.translation(in: view)
        let verticalMovement = translation.y / view.bounds.height
        let downwardMovement = fmaxf(Float(verticalMovement), 0.0)
        let downwardMovementPercent = fminf(downwardMovement, 1.0)
        let progress = CGFloat(downwardMovementPercent)

        guard let interactor = interactor else { return }

        switch sender.state {
        case .began:
            interactor.hasStarted = true
            dismiss(animated: true, completion: nil)
        case .changed:
            interactor.shouldFinish = progress > percentThreshold
            interactor.update(progress)
        case .cancelled:
            interactor.hasStarted = false
            interactor.cancel()
        case .ended:
            interactor.hasStarted = false
            interactor.shouldFinish
                ? interactor.finish()
                : interactor.cancel()
        default:
            break
        }
    }

    func showNoDateAlert() {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "沒有資料", message: "請選擇專輯", preferredStyle: .alert)
            let action = UIAlertAction(title: "確認", style: .default) { [weak self] _ in
                guard let strongSelf = self else { return }
                strongSelf.dismiss(animated: true, completion: nil)
            }
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension PlayViewController: UIViewControllerTransitioningDelegate {
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return DismissAnimator()
    }

    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return reportInteractor.hasStarted ? reportInteractor : nil
    }
}
