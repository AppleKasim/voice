//
//  PlayTimingCell.swift
//  Voice
//
//  Created by kasim on 2018/11/23.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit

class PlayTimingCell: UICollectionViewCell {

    @IBOutlet weak var contentLabel: UILabel! {
        didSet {
            contentLabel.textColor = .white
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        self.backgroundColor = Global.blackColor353A50
    }

    func update(model: PlayTimingModel) {
        contentLabel.text = model.content
    }
}
