//
//  PlayTimingReusableView.swift
//  Voice
//
//  Created by kasim on 2018/11/23.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class PlayTimingReusableView: UICollectionReusableView {

    @IBOutlet weak var headerLabel: UILabel! {
        didSet {
            headerLabel.textColor = .white
        }
    }
    @IBOutlet weak var headerSwitch: UISwitch! {
        didSet {
            headerSwitch.onTintColor = Global.yellowColorF8BF00
            headerSwitch.layer.cornerRadius = headerSwitch.frame.height / 2
            headerSwitch.backgroundColor = .white

            headerSwitch.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)

//            headerSwitch.rx.isOn.asObservable()
//                .subscribe(onNext: { [weak self] in
//                    guard let strongSelf = self else { return }
//                    if $0 {
//                        strongSelf.headerSwitch.thumbTintColor = .white
//                    } else {
//                        strongSelf.headerSwitch.thumbTintColor = Global.grayColor86
//                    }
//
//                })
//                .disposed(by: disposeBag)

        }
    }

    var disposeBag = DisposeBag()

    override func awakeFromNib() {
        super.awakeFromNib()

        self.backgroundColor = Global.blackColor2A2E43
    }

    func update(model: PlayTimingHeaderModel) {
        headerLabel.text = model.title
        headerSwitch.isOn = model.isEnable
    }
}
