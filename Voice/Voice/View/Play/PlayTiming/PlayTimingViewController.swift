//
//  PlayTimingViewController.swift
//  Voice
//
//  Created by kasim on 2018/11/23.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

class PlayTimingViewController: UIViewController {

    //top view
    @IBOutlet weak var topTitleView: UIView! {
        didSet {
            topTitleView.backgroundColor = Global.blackColor2A2E43
            
            topTitleView.layer.cornerRadius = 12.0
            topTitleView.layer.maskedCorners = [CACornerMask.layerMinXMinYCorner, CACornerMask.layerMaxXMinYCorner]
        }
    }
    @IBOutlet weak var timeTitleLabel: UILabel! {
        didSet {
            timeTitleLabel.textColor = Global.yellowColorF8BF00
        }
    }
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.textColor = .white
        }
    }

    @IBOutlet weak var topViewBaseline: UIView! {
        didSet {
            topViewBaseline.backgroundColor = Global.grayColor555869
        }
    }

    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.backgroundColor = Global.blackColor2A2E43
        }
    }

    @IBOutlet weak var shutdownButton: UIButton! {
        didSet {
            shutdownButton.setTitleColor(Global.blueColorA2AACB, for: .normal)
            shutdownButton.backgroundColor = Global.blackColor1F2336
        }
    }

    @IBOutlet weak var collectionViewLayout: UICollectionViewFlowLayout! {
        didSet {
            let height = 60 / 667 * Global.screenHeight
            let width = height
            collectionViewLayout.itemSize = CGSize(width: width, height: height)
            collectionViewLayout.minimumLineSpacing = 6 / 375 * Global.screenWidth
            collectionViewLayout.minimumInteritemSpacing = 5 / 667 * Global.screenHeight
            let edgeInsets = 25 / 375 * Global.screenWidth
            collectionViewLayout.sectionInset = UIEdgeInsets(top: 0, left: edgeInsets, bottom: 0, right: edgeInsets)
        }
    }

    let viewModel = PlayTimingViewModel()
    let disposeBag = DisposeBag()

    // swiftlint:disable line_length
    private lazy var dataSource =  RxCollectionViewSectionedReloadDataSource<SectionOfPlayTiming>(configureCell: configureCell, configureSupplementaryView: configureSupplementaryView)

    private lazy var configureCell: RxCollectionViewSectionedReloadDataSource<SectionOfPlayTiming>.ConfigureCell = {(dataSource, collectionView, indexPath, element) in

        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlayTimingCell", for: indexPath) as? PlayTimingCell else {
            return UICollectionViewCell()
        }

        cell.update(model: element)

        return cell
    }

    private lazy var configureSupplementaryView: RxCollectionViewSectionedReloadDataSource<SectionOfPlayTiming>.ConfigureSupplementaryView = {(dataSource , viewController, kind, indexPath) in
        guard let section = viewController.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "PlayTimingReusableView", for: indexPath) as? PlayTimingReusableView else {
            return UICollectionReusableView()
        }

        section.update(model: dataSource[indexPath.section].header)
        return section
    }
    // swiftlint:enable line_length

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.items
            .asDriver()
            .drive(collectionView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)

        viewModel.updateItem()
    }

    @IBAction func shutDown(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

}

extension PlayTimingViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {

        let height = 44 / 667 * Global.screenHeight
        return CGSize(width: Global.screenWidth, height: height)
    }
}
