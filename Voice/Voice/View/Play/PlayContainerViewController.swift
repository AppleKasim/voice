//
//  PlayContainerViewController.swift
//  Voice
//
//  Created by kasim on 2018/11/22.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit
import RxDataSources
import RxCocoa
import RxSwift

class PlayContainerViewController: UIPageViewController {

    lazy var orderedViewControllers: [UIViewController] = {

        var viewControllers: [UIViewController] = []

        guard let playPageContent = self.storyboard!.instantiateViewController(withIdentifier: "PlayPageContentViewController") as? PlayPageContentViewController else {
            return []
        }
        //playPageContent.viewModel = viewModel

        guard let introductionPageContent = self.storyboard!.instantiateViewController(withIdentifier: "IntroductionPageContentViewController") as? IntroductionPageContentViewController else {
            return []
        }

        viewControllers.append(playPageContent)
        viewControllers.append(introductionPageContent)

        return viewControllers

    }()

    var currentPageIndex = 0
    var willTransitionTo: UIViewController!

    var changePageForMainViewController: ((_ index: Int) -> Void)?

    //var viewModel: PlayViewModel!
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.dataSource = self
        self.delegate = self

        setViewControllers([orderedViewControllers[0]], direction: .forward, animated: true, completion: nil)
    }

    func showPage(byIndex index: Int) {
        let viewController = orderedViewControllers[index]
        var direction: UIPageViewController.NavigationDirection = .forward
        if currentPageIndex > index {
            direction = .reverse
        }
        setViewControllers([viewController], direction: direction, animated: true, completion: nil)
        currentPageIndex = index
    }
}

extension PlayContainerViewController: UIPageViewControllerDataSource {
    // swiftlint:disable line_length
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let index = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        let previousIndex = index - 1
        guard previousIndex >= 0 && previousIndex < orderedViewControllers.count else {
            return nil
        }
        return orderedViewControllers[previousIndex]
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let index = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        let nextIndex = index + 1
        guard nextIndex >= 0 && nextIndex < orderedViewControllers.count else {
            return nil
        }
        return orderedViewControllers[nextIndex]
    }
    // swiftlint:enable line_length
}

// swiftlint:disable line_length
extension PlayContainerViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        self.willTransitionTo = pendingViewControllers.first

    }

    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers previousVieControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            guard let index = orderedViewControllers.index(of: self.willTransitionTo) else { return }
            let previousViewController = previousVieControllers.first!
            let previousIndex = orderedViewControllers.index(of: previousViewController)
            if index != previousIndex {
                guard let changePage = changePageForMainViewController else {
                    return
                }
                changePage(index)
                self.currentPageIndex = index
            }
        }
    }
}
// swiftlint:enable line_length
