//
//  PlaySingleton.swift
//  Voice
//
//  Created by kasim on 2019/1/1.
//  Copyright © 2019 Shiefu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import AVFoundation
import MediaPlayer

class PlayManager: NSObject {
    private static var playSingleton = PlayManager()

    var isPlayNextSong = false
    var isPlaying = BehaviorRelay<Bool>(value: false)

    var voiceList: [PlayVoice] = [] {
        didSet {
            currentVoice.accept(voiceList[playingIndex!])
        }
    }
    var playIntroduction = BehaviorRelay<PlayIntroduction?>(value: nil)
    var player: AVPlayer?
    var cachingPlayerItem: CachingPlayerItem?

    var currentVoice = BehaviorRelay<PlayVoice?>(value: nil)
    var playingIndex: Int? {
        didSet {
            if voiceList.count > 0 {
                currentVoice.accept(voiceList[playingIndex!])
            }
        }
    }

    let viewModel = PlayViewModel()

    var albumId: Int? {
        didSet {
            viewModel.updateItem()
        }
    }
    var itemDuration = BehaviorRelay<CMTime?>(value: nil)
    let disposeBag = DisposeBag()
    var periodicTimeAction: (_ cmTime: CMTime) -> Void = { _ in }

    private override init() {
        super.init()

        NotificationCenter.default.rx
            .notification(Notification.Name.AVPlayerItemDidPlayToEndTime)
            .asObservable().subscribe(onNext: { [weak self] _ in
                guard let strongSelf = self else { return }
                strongSelf.next()
            }).disposed(by: disposeBag)

        UIApplication.shared.beginReceivingRemoteControlEvents()
        configRemoteCommandCenter()
    }

    class func share() -> PlayManager {
        return playSingleton
    }

    func setNowPlayingInfoCenter() {
        let mySize = CGSize(width: 400, height: 400)
        let albumArt = MPMediaItemArtwork(boundsSize: mySize) { _ in
            return UIImage(named: "settingCoins")!
        }

        let elapsedTime = NSNumber(value: CMTimeGetSeconds(player!.currentTime()))
        let duration = NSNumber(value: CMTimeGetSeconds(self.itemDuration.value!))

        let mpic = MPNowPlayingInfoCenter.default()
        mpic.nowPlayingInfo = [MPMediaItemPropertyTitle: voiceList[playingIndex!].title,
                               MPMediaItemPropertyArtist: playingIndex! ,
                               MPMediaItemPropertyArtwork: albumArt,
                               MPNowPlayingInfoPropertyElapsedPlaybackTime: elapsedTime,
                               MPMediaItemPropertyPlaybackDuration: duration
                               //MPNowPlayingInfoPropertyPlaybackRate: playbackRate
        ]
    }

    func configRemoteCommandCenter() {
        let remoteCommandCenter = MPRemoteCommandCenter.shared()

        //播放事件
        let playCommand = remoteCommandCenter.playCommand
        playCommand.isEnabled = true
        playCommand.addTarget(self, action: #selector(playItem))

        //暂停事件
        let pauseCommand = remoteCommandCenter.pauseCommand
        pauseCommand.isEnabled = true
        pauseCommand.addTarget(self, action: #selector(pauseItem))

        let previousTrackCommand = remoteCommandCenter.previousTrackCommand
        previousTrackCommand.isEnabled = true
        previousTrackCommand.addTarget(self, action: #selector(previousItem))

        //下一曲
        let nextTrackCommand = remoteCommandCenter.nextTrackCommand
        nextTrackCommand.isEnabled = true
        nextTrackCommand.addTarget(self, action: #selector(nextItem))

        //拖动播放位置
        let changePlaybackPositionCommand = remoteCommandCenter.changePlaybackPositionCommand
        changePlaybackPositionCommand.isEnabled = true
        changePlaybackPositionCommand.addTarget { [weak self] (commandEvent) -> MPRemoteCommandHandlerStatus in
            //如何精确拖动的位置？
            guard let event = commandEvent as? MPChangePlaybackPositionCommandEvent,
                let strongSelf = self else {
                return MPRemoteCommandHandlerStatus.commandFailed
            }

            if strongSelf.player != nil {
                //需要使用带回调的SeekTime 回调重新设置进度 否则播放进度条会停止
                strongSelf.player?.seek(to: CMTimeMakeWithSeconds(event.positionTime, preferredTimescale: 600), completionHandler: { (finish) in
                    //Q:拖动介绍后进度条不动了
                    //A:恢复时重新配置MPNowPlayingInfoPropertyElapsedPlaybackTime
                    var dic = MPNowPlayingInfoCenter.default().nowPlayingInfo
                    dic?[MPNowPlayingInfoPropertyElapsedPlaybackTime] = NSNumber(value: CMTimeGetSeconds(strongSelf.player!.currentTime()))
                    MPNowPlayingInfoCenter.default().nowPlayingInfo = dic
                })
                return MPRemoteCommandHandlerStatus.success
            } else {
                return MPRemoteCommandHandlerStatus.commandFailed
            }
        }
    }

    @objc func playItem(command: MPRemoteCommand) {
        DispatchQueue.main.async {
            self.play()
        }
    }
    @objc func pauseItem(command: MPRemoteCommand) {
        DispatchQueue.main.async {
            self.pause()
        }
    }
    @objc func previousItem(command: MPRemoteCommand) {
        DispatchQueue.main.async {
            self.last()
        }
    }
    @objc func nextItem(command: MPRemoteCommand) {
        DispatchQueue.main.async {
            self.next()
        }
    }

    func play() {
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.classForCoder.cancelPreviousPerformRequests(withTarget: strongSelf)

            if strongSelf.voiceList.count <= 0 {
                assert(false, "没数据")
            }

            if strongSelf.player == nil {
                strongSelf.player = AVPlayer()
                // swiftlint:disable line_length
                strongSelf.player?.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1, preferredTimescale: 1), queue: DispatchQueue.main) { [weak self] (cmTime) -> Void in
                    guard let strongSelf = self else { return }
                    strongSelf.periodicTimeAction(cmTime)
                }
                // swiftlint:enable line_length
            }

            if strongSelf.isPlayNextSong == true {
                //上一首 下一首（更换item）
                guard let value = strongSelf.currentVoice.value,
                    let itmeURL = URL(string: value.url) else {
                        return
                }
                strongSelf.itemDuration.accept(AVAsset(url: itmeURL).duration)
                strongSelf.cachingPlayerItem = CachingPlayerItem(url: itmeURL)
                strongSelf.player?.replaceCurrentItem(with: strongSelf.cachingPlayerItem)
            } else {
                //暂停 播放
            }

            if strongSelf.player != nil {
                strongSelf.isPlaying.accept(true)
                strongSelf.isPlayNextSong = false
                strongSelf.setNowPlayingInfoCenter()

                strongSelf.player!.automaticallyWaitsToMinimizeStalling = false
                strongSelf.player!.play()

            } else {
                assert(false, "状态错误")
                return
            }
        }
    }

    func pause() {
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.isPlaying.accept(false)
            strongSelf.player?.pause()
        }
    }

    func next() {
        var nextIndex: Int = 0
        if voiceList.count > playingIndex! + 1 {
            nextIndex = playingIndex! + 1
        }

        playingIndex = nextIndex
        isPlayNextSong = true
        self.play()
    }

    func last() {
        var lastIndex: Int = 0
        if playingIndex! > 0 {
            lastIndex = playingIndex! - 1
        } else {
            lastIndex = voiceList.count - 1
        }

        playingIndex = lastIndex
        isPlayNextSong = true
        self.play()
    }

    func getCurrentVoice() -> PlayVoice {
        return voiceList[playingIndex!]
    }

    func getCurrentPlayTime() -> CMTime {
        guard let time = player?.currentTime() else {
            return CMTime(value: 0, timescale: 1)
        }
        return time
    }
}
