//
//  ReportViewController.swift
//  Voice
//
//  Created by kasim on 2019/1/13.
//  Copyright © 2019 Shiefu. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ReportViewController: UIViewController {

    var interactor: Interactor?
    //var albumId = BehaviorRelay<Int?>(value: nil)
    let disposeBag = DisposeBag()
    let viewModel = ReportViewModel()

    @IBOutlet weak var textView: UITextView! {
        didSet {
            textView.layer.borderWidth = 1
            textView.layer.borderColor = UIColor.black.cgColor
            textView.text = ""
        }
    }

    @IBOutlet weak var sendButton: UIButton! {
        didSet {
            sendButton.backgroundColor = Global.yellowColorF8BF00
            sendButton.setTitleColor(.black, for: .normal)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()



//        albumId.bind(to: viewModel.albumId)
//            .disposed(by: disposeBag)

        viewModel.sendButtonEnabled
            .subscribe (onNext: { [unowned self] valid in
                self.sendButton.isEnabled = valid
                self.sendButton.alpha = valid ? 1.0 : 0.5
            })
            .disposed(by: disposeBag)

        textView.rx.text.orEmpty
            .bind(to: viewModel.text)
            .disposed(by: disposeBag)

        sendButton.rx.tap
            .bind(to: viewModel.reportTaps)
            .disposed(by: disposeBag)

//        viewModel.registerResult
//            .subscribe(onNext: { [unowned self] result in
//                switch result {
//                case let .ok(message):
//                    self.showAlert(message: message)
//                case .empty:
//                    self.showAlert(message: "")
//                case let .failed(message):
//                    self.showAlert(message: message)
//                }
//            })
//            .disposed(by: disposeBag)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }

    @IBAction func dismiss(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func send(_ sender: UIButton) {
        guard let strongAlbumId = PlayManager.share().albumId else { return }

        viewModel.sendReport(albumId: strongAlbumId, content: textView.text)
            .subscribe(onNext: { [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .empty:
                print("empty")
            case .succeed(let data):
                print("data")
                strongSelf.showAlert(message: "回報成功")
            case .failed(let errorMessage):
                strongSelf.showAlert(message: "失敗")
                print("failed:\(errorMessage)")
            }
        }).disposed(by: disposeBag)
    }

    @IBAction func downSliding(_ sender: UIPanGestureRecognizer) {
        let percentThreshold: CGFloat = 0.3

        let translation = sender.translation(in: view)
        let verticalMovement = translation.y / view.bounds.height
        let downwardMovement = fmaxf(Float(verticalMovement), 0.0)
        let downwardMovementPercent = fminf(downwardMovement, 1.0)
        let progress = CGFloat(downwardMovementPercent)

        guard let interactor = interactor else { return }

        switch sender.state {
        case .began:
            interactor.hasStarted = true
            dismiss(animated: true, completion: nil)
        case .changed:
            interactor.shouldFinish = progress > percentThreshold
            interactor.update(progress)
        case .cancelled:
            interactor.hasStarted = false
            interactor.cancel()
        case .ended:
            interactor.hasStarted = false
            interactor.shouldFinish
                ? interactor.finish()
                : interactor.cancel()
        default:
            break
        }
    }

    func showAlert(message: String) {
        let action = UIAlertAction(title: "确定", style: .default, handler: nil)
        let alertViewController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alertViewController.addAction(action)

        present(alertViewController, animated: true, completion: nil)

    }

}
