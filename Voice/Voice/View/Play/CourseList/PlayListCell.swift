//
//  PlayListCell.swift
//  Voice
//
//  Created by kasim on 2018/11/23.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import SDWebImage

class PlayListCell: UITableViewCell {

    let disposeBag = DisposeBag()

    @IBOutlet weak var dotImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.textColor = .white
        }
    }
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var playImageView: FLAnimatedImageView!
    
    @IBOutlet weak var cellContentView: UIView! {
        didSet {
            cellContentView.backgroundColor = Global.blackColor2A2E43
        }
    }

    @IBOutlet weak var bottomVerticalLine: UIView! {
        didSet {
            bottomVerticalLine.backgroundColor = Global.grayColor70
        }
    }

    @IBOutlet weak var topVerticalLine: UIView! {
        didSet {
            topVerticalLine.backgroundColor = Global.grayColor70
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = Global.blackColor2A2E43
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func update(model: PlayVoice) {
        nameLabel.text = model.title
        dateLabel.text = ""
    }

}
