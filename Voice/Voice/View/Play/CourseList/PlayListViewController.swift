//
//  PlayListViewController.swift
//  Voice
//
//  Created by kasim on 2018/11/23.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxDataSources
import SDWebImage

class PlayListViewController: UIViewController {

    @IBOutlet weak var shutdownButton: UIButton! {
        didSet {
            shutdownButton.backgroundColor = Global.blackColor1F2336
            shutdownButton.setTitleColor(Global.blueColorADCAFF, for: .normal)
            shutdownButton.setTitle("關閉", for: .normal)
        }
    }

    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.backgroundColor = Global.blackColor2A2E43
            tableView.layer.cornerRadius = 12.0
            tableView.layer.maskedCorners = [CACornerMask.layerMinXMinYCorner, CACornerMask.layerMaxXMinYCorner]
            tableView.bounces = false
        }
    }

    // swiftlint:disable line_length
    private lazy var dataSource = RxTableViewSectionedReloadDataSource<SingleSectionModel<String, PlayVoice>>(configureCell: configureCell)

    private lazy var configureCell: RxTableViewSectionedReloadDataSource<SingleSectionModel<String, PlayVoice>>.ConfigureCell = { [weak self] (dataSource, tableView, indexPath, element) in

        guard let strongSelf = self,
            let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "PlayListCell") as? PlayListCell else {
            return UITableViewCell()
        }

        PlayManager.share().currentVoice
            .subscribe( onNext: { [weak self] currentVoice in
                if let voice = currentVoice, voice.voiceId == element.voiceId {
                    do {
                        let imageData = try Data(contentsOf: Bundle.main.url(forResource: "pause", withExtension: "gif")!)
                        tableViewCell.playImageView.animatedImage = FLAnimatedImage(animatedGIFData: imageData)
                    } catch {
                        print("出現錯誤 ： \(error)")
                    }
                } else {
                    tableViewCell.playImageView.image = UIImage(named: "play")
                }
            })
            .disposed(by: tableViewCell.disposeBag)

        if indexPath.row == 0 {
            tableViewCell.topVerticalLine.isHidden = true
        }

        tableViewCell.update(model: element)

        return tableViewCell
    }
    // swiftlint:enable line_length

    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.rx.setDelegate(self)
            .disposed(by: disposeBag)

        PlayManager.share().viewModel.playList
            .bind(to: tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)

        tableView.rx.itemSelected
            .subscribe(onNext: { [weak self] indexPath in
                guard let strongSelf = self else { return }
                strongSelf.dismiss(animated: true, completion: nil)

                let playList = PlayManager.share().viewModel.playList
                let selectedItem = playList.value[0].items[indexPath.row]
                let currentIndex = strongSelf.getSelectedVoiceIndex(selectedItem: selectedItem)

                PlayManager.share().playingIndex = currentIndex
                PlayManager.share().isPlayNextSong = true
                PlayManager.share().play()
            })
            .disposed(by: disposeBag)
    }

    func getSelectedVoiceIndex(selectedItem: PlayVoice) -> Int {
        let voice = PlayManager.share().voiceList.filter {
            $0.voiceId == selectedItem.voiceId
        }

        var currentIndex = 0

        for (index, item) in PlayManager.share().voiceList.enumerated()
            where item.voiceId == voice[0].voiceId {
                currentIndex = index
        }
        return currentIndex
    }

    @IBAction func shutDown(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

}

extension PlayListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 43 / 667 * Global.screenHeight
    }

    // swiftlint:disable line_length
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        guard let headerCell = tableView.dequeueReusableCell(withIdentifier: "PlayListHeaderCell") as? PlayListHeaderCell else {
            return nil
        }

        headerCell.update(title: dataSource.sectionModels[section].header)

        headerCell.sortButton.rx.tap
            .subscribe(onNext: {
                let viewModel = PlayManager.share().viewModel
                viewModel.smallToLarge = !viewModel.smallToLarge
            })
            .disposed(by: headerCell.disposeBag)

        return headerCell
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 37 / 667 * Global.screenHeight
        default:
            return 0.001
        }
    }
}
