//
//  PlayListHeaderCell.swift
//  Voice
//
//  Created by kasim on 2018/11/23.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class PlayListHeaderCell: UITableViewCell {

    @IBOutlet weak var backView: UIView! {
        didSet {
            backView.backgroundColor = Global.blackColor2A2E43

            backView.layer.cornerRadius = 12.0
            backView.layer.maskedCorners = [CACornerMask.layerMinXMinYCorner, CACornerMask.layerMaxXMinYCorner]
        }
    }
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.textColor = .white
        }
    }
    @IBOutlet weak var sortButton: UIButton! {
        didSet {
            sortButton.backgroundColor = Global.blackColor2A2E43
            sortButton.setTitle("排序", for: .normal)
        }
    }

    @IBOutlet weak var lineView: UIView! {
        didSet {
            lineView.backgroundColor = Global.grayColor70
        }
    }

    let disposeBag = DisposeBag()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func update(title: String) {
        titleLabel.text = title
    }
}
