//
//  PlayTimingTransitionManager.swift
//  Voice
//
//  Created by kasim on 2018/11/23.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit

@objc protocol TransitionManagerDelegate {
    func dismiss()
}

class Interactor: UIPercentDrivenInteractiveTransition {
    var hasStarted = false
    var shouldFinish = false
}

// swiftlint:disable line_length
class PlayTransitionManager: NSObject, UIViewControllerAnimatedTransitioning {
    var duration = 0.5
    var isPresenting = false
    weak var delegate: TransitionManagerDelegate?
    var isAllScreen = false

    var snapshot: UIView? {
        didSet {
            if let delegate = delegate {
                let tapGestureRecognizer = UITapGestureRecognizer(target: delegate, action: #selector(TransitionManagerDelegate.dismiss))
                snapshot?.addGestureRecognizer(tapGestureRecognizer)
            }
        }
    }

    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {

        // 取得我們的 fromView、toView 以及 container view的參照
        let fromView = transitionContext.view(forKey: UITransitionContextViewKey.from)!
        let toView = transitionContext.view(forKey: UITransitionContextViewKey.to)!

        // 設定滑動的變換（transform）
        let container = transitionContext.containerView

        var upHeight = 420 / 667 * Global.screenHeight
        if isAllScreen {
            upHeight = Global.screenHeight
        }

        let moveUp = CGAffineTransform(translationX: 0, y: container.frame.height - upHeight)
        let moveDownScreenHeight = CGAffineTransform(translationX: 0, y: container.frame.height)

        //  將兩個視圖加進容器視圖
        if isPresenting {
            toView.transform = moveDownScreenHeight
            snapshot = fromView.snapshotView(afterScreenUpdates: true)
            container.addSubview(snapshot!)
            container.addSubview(toView)
        }

        // 執行動畫
        UIView.animate(withDuration: duration, delay: 0.0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.3, options: [], animations: {

            if self.isPresenting {
                toView.transform = moveUp
            } else {
                fromView.transform = moveDownScreenHeight
            }

        }, completion: { _ in
            transitionContext.completeTransition(true)
            if !self.isPresenting {
                self.snapshot?.removeFromSuperview()
            }
        })
    }
}
// swiftlint:enable line_length

extension PlayTransitionManager: UIViewControllerTransitioningDelegate {
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        isPresenting = false
        return self
    }

    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        isPresenting = true
        return self
    }
}
