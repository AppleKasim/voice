//
//  RecordPageContentCell.swift
//  Voice
//
//  Created by kasim on 2018/11/20.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit

class RecordPageContentCell: UITableViewCell {

    @IBOutlet weak var leftImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var keepButton: UIButton! {
        didSet {
            keepButton.imageView?.contentMode = .scaleAspectFit
            keepButton.setTitleColor(Global.grayColor78849E, for: .normal)
        }
    }
    @IBOutlet weak var courseBackgroundView: UIView! {
        didSet {
            courseBackgroundView.backgroundColor = Global.grayColorEFF4FC
        }
    }

    var cellHeight: CGFloat = 0 {
        didSet {
            courseBackgroundView.layer.cornerRadius = (15 / 70 * cellHeight) / 2.0
        }
    }

    @IBOutlet weak var chargeLabel: UILabel! {
        didSet {
            chargeLabel.textColor = Global.greenColor00CE87
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func update(model: RecordModel) {
        titleLabel.text = model.title
        detailLabel.text = model.detail
        chargeLabel.text = model.charge
        keepButton.setTitle(model.keepAmount, for: .normal)
    }
}
