//
//  RecordPageContentViewController.swift
//  Voice
//
//  Created by kasim on 2018/11/20.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

class RecordPageContentViewController: UIViewController {

    private let cellHeight = 70 / 667 * Global.screenHeight

    // swiftlint:disable line_length
    private lazy var dataSource = RxTableViewSectionedReloadDataSource<SectionOfRecordModel>(configureCell: configureCell)

    private lazy var configureCell: RxTableViewSectionedReloadDataSource<SectionOfRecordModel>.ConfigureCell = { [weak self] (dataSource, tableView, indexPath, element) in

        guard let strongSelf = self else {
            return UITableViewCell()
        }

        guard let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "RecordPageContentCell") as? RecordPageContentCell else {
            return UITableViewCell()
        }
        tableViewCell.cellHeight = strongSelf.cellHeight
        tableViewCell.update(model: element)

        return tableViewCell
    }
    // swiftlint:enable line_length

    @IBOutlet weak var tableView: UITableView!

    var viewModel: RecordViewModel!
    let disposeBag = DisposeBag()
    var pageTitle = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.updateItem()

        viewModel.getPageContentTuples(title: pageTitle)?.1
            .asDriver()
            .drive(tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)

        tableView.rx.setDelegate(self)
            .disposed(by: disposeBag)

//        tableView.rx.itemSelected
//            .map { [weak self] indexPath -> RecommendationModel? in
//                guard let strongSelf = self else {
//                    return nil
//                }
//                return strongSelf.dataSource[indexPath]
//            }
//            .subscribe(onNext: { [weak self] item in
//                guard let item = item, let strongSelf = self else { return }
//
//                strongSelf.performSegue(withIdentifier: "PageContentToDetailSegue", sender: item)
//
//            })
//            .disposed(by: disposeBag)
    }

}

extension RecordPageContentViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }
}
