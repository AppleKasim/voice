//
//  RecordViewController.swift
//  Voice
//
//  Created by kasim on 2018/11/20.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class RecordViewController: UIViewController {
    @IBOutlet weak var customSegmentedControl: CustomSegmentedControl! {
        didSet {
            customSegmentedControl.titles = ["dfjsifjid\n1111", "dfshudfh\n22222", "fdhfdfi\n3333"]
        }
    }
    
    var segmentedControl: UISegmentedControl {
        return customSegmentedControl.segmentedControl
    }

    var currentPageIndex = BehaviorRelay(value: 0)
    var recordPageViewController: RecordPageViewController!

    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

//        segmentedControl.removeBorder()
//
//        let fontStyle = SegmentedControlFontStyle()
//        let titles = ["dsfudshfu\n1111", "fhduhfud\n2222", "dfdffff\n333"]
//        segmentedControl.customUnderlineStyle(titles: titles, withFontStyle: fontStyle, disposeBag: disposeBag)

        recordPageViewController.changeTabForMainViewController = {[weak self] index in
            guard let strongSelf = self else {
                return
            }

            strongSelf.currentPageIndex.accept(index)
        }

        currentPageIndex.asObservable()
            .subscribe(onNext: {[weak self] value in
                guard let strongSelf = self else {
                    return
                }
                strongSelf.segmentedControl.selectedSegmentIndex = value
                strongSelf.segmentedControl.sendActions(for: UIControlEvents.valueChanged)
            })
            .disposed(by: disposeBag)

        segmentedControl.rx.selectedSegmentIndex
            .subscribe(onNext: {[weak self] index in
                guard let strongSelf = self else {
                    return
                }
                strongSelf.recordPageViewController.showPage(byIndex: index)
            })
            .disposed(by: disposeBag)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "RecordToContainer",
            let destination = segue.destination as? RecordPageViewController {
            recordPageViewController = destination
            currentPageIndex
                .bind(to: recordPageViewController.currentPageIndex)
                .disposed(by: disposeBag)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.navigationController?.setNavigationBarHidden(true, animated: true)
        self.tabBarController?.tabBar.isHidden = false
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let lineWidth = customSegmentedControl.frame.width / CGFloat(customSegmentedControl.segmentedControl.numberOfSegments)
        customSegmentedControl.lineWidthConstraint.constant = lineWidth
        customSegmentedControl.itemWidth = lineWidth

    }
}
