//
//  RecordPageViewController.swift
//  Voice
//
//  Created by kasim on 2018/11/20.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit
import RxDataSources
import RxSwift
import RxCocoa

class RecordPageViewController: UIPageViewController {

    lazy var orderedViewControllers: [UIViewController] = {
        let pageContentViewControllerArray =  viewModel.titles.map { title -> RecordPageContentViewController in
            guard let pageContent = self.storyboard!.instantiateViewController(withIdentifier: "RecordPageContentViewController") as? RecordPageContentViewController else {
                return RecordPageContentViewController()
            }
            pageContent.viewModel = viewModel
            pageContent.pageTitle = title
            return pageContent
        }
        return pageContentViewControllerArray
    }()

    //var currentPageIndex = 0
    var currentPageIndex = BehaviorRelay(value: 0)
    var willTransitionTo: UIViewController!

    var changeTabForMainViewController: ((_ index: Int) -> Void)?
    var viewModel = RecordViewModel()

    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.dataSource = self
        self.delegate = self

        setViewControllers([orderedViewControllers[currentPageIndex.value]], direction: .forward, animated: true, completion: nil)
    }

    func showPage(byIndex index: Int) {
        let viewController = orderedViewControllers[index]
        var direction: UIPageViewController.NavigationDirection = .forward
        if currentPageIndex.value > index {
            direction = .reverse
        }
        setViewControllers([viewController], direction: direction, animated: true, completion: nil)
        currentPageIndex.accept(index)
    }
}

extension RecordPageViewController: UIPageViewControllerDataSource {
    // swiftlint:disable line_length
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let index = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        let previousIndex = index - 1
        guard previousIndex >= 0 && previousIndex < orderedViewControllers.count else {
            return nil
        }
        return orderedViewControllers[previousIndex]
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let index = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        let nextIndex = index + 1
        guard nextIndex >= 0 && nextIndex < orderedViewControllers.count else {
            return nil
        }
        return orderedViewControllers[nextIndex]
    }
    // swiftlint:enable line_length
}

// swiftlint:disable line_length
extension RecordPageViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        self.willTransitionTo = pendingViewControllers.first

    }

    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers previousVieControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            guard let index = orderedViewControllers.index(of: self.willTransitionTo) else { return }
            let previousViewController = previousVieControllers.first!
            let previousIndex = orderedViewControllers.index(of: previousViewController)
            if index != previousIndex {
                guard let changeTab = changeTabForMainViewController else {
                    return
                }
                changeTab(index)
                self.currentPageIndex.accept(index)
            }
        }
    }
}
// swiftlint:enable line_length
