//
//  SegmentedControl.swift
//  Voice
//
//  Created by kasim on 2018/11/17.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

struct SegmentedControlFontStyle {
    var fontName: String = "NotoSerifCJKtc-Medium"
    var englishFontName: String = "NotoSerifCJKtc-Regular"
    var fontSize: CGFloat = 16
    var englishFontSize: CGFloat = 8
    var textColor: UIColor = Global.yellowColorF8BF00
}

extension UISegmentedControl {

    func removeBorder() {
        self.backgroundColor = .clear
        self.tintColor = .clear
    }

    //要整理，很亂
    func customUnderlineStyle(titles: [String], withFontStyle style: SegmentedControlFontStyle, disposeBag: DisposeBag) {

        self.removeAllSegments()
        for (index, title) in titles.enumerated() {
            self.insertSegment(withTitle: title, at: index, animated: false)
        }

        for index in 0...self.numberOfSegments - 1 {

            //let string = "dfu8uf8du\nddddd"

            let label = UILabel()
            label.numberOfLines = 0
            label.textAlignment = .center
            label.adjustsFontSizeToFitWidth = true

            self.rx.selectedSegmentIndex.asObservable()
                .subscribe(onNext: { [weak self] in
                    guard let strongSelf = self else { return }

                    var color = UIColor()
                    if $0 == index {
                        color = style.textColor
                    } else {
                        color = Global.blackColor00
                    }
                    UIView.transition(with: label, duration: 0.1, options: .transitionCrossDissolve, animations: {
                        label.attributedText = strongSelf.getAttributedText(string: titles[index], chineseColor: color, style: style)
                    }, completion: nil)
                })
                .disposed(by: disposeBag)

            self.setTitle("", forSegmentAt: index)
            self.subviews[index].addSubview(label)

            label.translatesAutoresizingMaskIntoConstraints = false

            label.topAnchor.constraint(equalTo: self.subviews[index].topAnchor).isActive = true
            label.bottomAnchor.constraint(equalTo: self.subviews[index].bottomAnchor).isActive = true
            label.leftAnchor.constraint(equalTo: self.subviews[index].leftAnchor).isActive = true
            label.rightAnchor.constraint(equalTo: self.subviews[index].rightAnchor).isActive = true
        }
    }

    private func getAttributedText(string: String, chineseColor: UIColor, style: SegmentedControlFontStyle) -> NSMutableAttributedString {
        let titleGroup = string.components(separatedBy: "\n")

        let text = NSMutableAttributedString.init(string: string)

        var attributed: [NSAttributedString.Key: Any] = [:]
        let chineseFont = UIFont(name: style.fontName, size: CGFloat(style.fontSize).adaptiveForHeight)
        attributed[NSAttributedString.Key.font] = chineseFont
        attributed[NSAttributedString.Key.foregroundColor] = chineseColor
        text.addAttributes(attributed, range: NSRange(location: 0, length: titleGroup[0].count))

        var attributed1: [NSAttributedString.Key: Any] = [:]
        let englishFont = UIFont(name: style.englishFontName, size: CGFloat(style.englishFontSize).adaptiveForHeight)
        attributed1[NSAttributedString.Key.font] = englishFont
        attributed1[NSAttributedString.Key.foregroundColor] = Global.grayColor707D95
        text.addAttributes(attributed1, range: NSRange(location: titleGroup[0].count, length: titleGroup[1].count + 1))

        return text
    }
}
