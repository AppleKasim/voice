//
//  TextView.swift
//  Voice
//
//  Created by kasim on 2018/12/25.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit

extension UITextView {
    @IBInspectable
    var regularFontSize: CGFloat {
        set {
            self.font = UIFont(name: "NotoSerifCJKtc-Regular", size: CGFloat(newValue).adaptiveForHeight)
        }
        get {
            return (font?.pointSize)!
        }
    }
}
