//
//  Reactive.swift
//  Voice
//
//  Created by kasim on 2018/11/28.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

extension Reactive where Base: UILabel {
    var validationResult: Binder<Result> {
        return Binder.init(base, binding: { (label, result) in
            label.textColor = result.textColor
            label.text = result.description
        })
    }
}

extension Reactive where Base: UITextField {
    var inputEnabled: Binder<Result> {
        return Binder(self.base) { textFiled, result in
            textFiled.isEnabled = result.isValid
        }
    }
}

extension Reactive where Base: UIBarButtonItem {
    var tapEnabled: Binder<Result> {
        return Binder(self.base) { button, result in
            button.isEnabled = result.isValid
        }
    }
}
