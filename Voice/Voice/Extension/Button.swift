//
//  Button.swift
//  Voice
//
//  Created by kasim on 2018/12/2.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit

extension UIButton {

    @IBInspectable
    var boldFontSize: CGFloat {
        set {
            self.titleLabel?.font = UIFont(name: "NotoSerifCJKtc-Bold", size: CGFloat(newValue).adaptiveForHeight)
        }
        get {
            return (titleLabel?.font?.pointSize)!
        }
    }

    @IBInspectable
    var mediumFontSize: CGFloat {
        set {
            self.titleLabel?.font = UIFont(name: "NotoSerifCJKtc-Medium", size: CGFloat(newValue).adaptiveForHeight)
        }
        get {
            return (titleLabel?.font?.pointSize)!
        }
    }

    @IBInspectable
    var semiBoldFontSize: CGFloat {
        set {
            self.titleLabel?.font = UIFont(name: "NotoSerifCJKtc-SemiBold", size: CGFloat(newValue).adaptiveForHeight)
        }
        get {
            return (titleLabel?.font?.pointSize)!
        }
    }

    @IBInspectable
    var regularFontSize: CGFloat {
        set {
            self.titleLabel?.font = UIFont(name: "NotoSerifCJKtc-Regular", size: CGFloat(newValue).adaptiveForHeight)
        }
        get {
            return (titleLabel?.font?.pointSize)!
        }
    }

    @IBInspectable
    var regularGrayFontSize: CGFloat {
        set {
            self.titleLabel?.font = UIFont(name: "NotoSerifCJKtc-Regular", size: CGFloat(newValue).adaptiveForHeight)
            self.setTitleColor(Global.grayColor78849E, for: .normal)
        }
        get {
            return (titleLabel?.font?.pointSize)!
        }
    }

    @IBInspectable
    var isBlackTitle: Bool {
        set {
            self.setTitleColor(Global.blackColor00, for: .normal)
        }
        get {
            return false
        }
    }

    func setCornerRadiusBorder(forMultiplier: CGFloat) {
        self.layer.borderWidth = 1.0
        self.layer.borderColor = Global.yellowColorF8BF00.cgColor
        self.clipsToBounds = true
        self.layer.cornerRadius = forMultiplier / 2
    }

    func set(titleLocation: UIView.ContentMode, padding: CGFloat, state: UIControl.State ) {
        self.imageView?.contentMode = .center
        relativeLocation(title: (self.titleLabel?.text)!, location: titleLocation, padding: padding)
        self.titleLabel?.contentMode = .center
    }

    private func relativeLocation(title: String, location: UIView.ContentMode, padding: CGFloat) {
        let imageSize = self.imageRect(forContentRect: self.frame)
        let titleFont = self.titleLabel?.font!
        let titleSize = title.size(withAttributes: [NSAttributedString.Key.font: titleFont!])

        var titleInsets: UIEdgeInsets
        var imageInsets: UIEdgeInsets

        switch location {
        case .top:
            titleInsets = UIEdgeInsets(top: -(imageSize.height + titleSize.height + padding),
                                       left: -(imageSize.width), bottom: 0, right: 0)
            imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -titleSize.width)
        case .left:
            titleInsets = UIEdgeInsets(top: 0, left: -(imageSize.width * 2), bottom: 0, right: 0)
            imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -(titleSize.width * 2 + padding))
        case .bottom:
            titleInsets = UIEdgeInsets(top: (imageSize.height + titleSize.height + padding),
                                       left: -(imageSize.width), bottom: 0, right: 0)
            imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -titleSize.width)
        case .right:
            titleInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -padding)
            imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        default:
            titleInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
        self.titleEdgeInsets = titleInsets
        self.imageEdgeInsets = imageInsets
    }

}
