//
//  Label.swift
//  Voice
//
//  Created by kasim on 2018/12/2.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit

extension UILabel {

    @IBInspectable
    var isCellTitle: Bool {
        set {
            if newValue {
                self.font = UIFont(name: "NotoSerifCJKtc-Bold", size: CGFloat(18).adaptiveForHeight)
            }
        }
        get {
            return false
        }
    }

    @IBInspectable
    var boldBlackFontSize: CGFloat {
        set {
            self.font = UIFont(name: "NotoSerifCJKtc-Bold", size: CGFloat(newValue).adaptiveForHeight)

            self.textColor = Global.blackColor333540
        }
        get {
            return (font?.pointSize)!
        }
    }

    @IBInspectable
    var boldFontSize: CGFloat {
        set {
            self.font = UIFont(name: "NotoSerifCJKtc-Bold", size: CGFloat(newValue).adaptiveForHeight)
        }
        get {
            return (font?.pointSize)!
        }
    }

    @IBInspectable
    var isCellCaption: Bool {
        set {
            if newValue {
                self.font = UIFont(name: "NotoSerifCJKtc-Medium", size: CGFloat(8).adaptiveForHeight)
                self.textColor = Global.grayColorD2
            }
        }
        get {
            return false
        }
    }

    @IBInspectable
    var mediumFontSize: CGFloat {
        set {
            self.font = UIFont(name: "NotoSerifCJKtc-Medium", size: CGFloat(newValue).adaptiveForHeight)
        }
        get {
            return (font?.pointSize)!
        }
    }

    @IBInspectable
    var semiBoldFontSize: CGFloat {
        set {
            self.font = UIFont(name: "NotoSerifCJKtc-SemiBold", size: CGFloat(newValue).adaptiveForHeight)
        }
        get {
            return (font?.pointSize)!
        }
    }

    @IBInspectable
    var regularFontSize: CGFloat {
        set {
            self.font = UIFont(name: "NotoSerifCJKtc-Regular", size: CGFloat(newValue).adaptiveForHeight)
        }
        get {
            return (font?.pointSize)!
        }
    }

    @IBInspectable
    var regularBlackFontSize: CGFloat {
        set {
            self.font = UIFont(name: "NotoSerifCJKtc-Regular", size: CGFloat(newValue).adaptiveForHeight)

            self.textColor = Global.blackColor4F5358
        }
        get {
            return (font?.pointSize)!
        }
    }

    @IBInspectable
    var regularGrayFontSize: CGFloat {
        set {
            self.font = UIFont(name: "NotoSerifCJKtc-Regular", size: CGFloat(newValue).adaptiveForHeight)
            self.textColor = Global.grayColor81878E
        }
        get {
            return (font?.pointSize)!
        }
    }

    @IBInspectable
    var regularRedFontSize: CGFloat {
        set {
            self.font = UIFont(name: "NotoSerifCJKtc-Regular", size: CGFloat(newValue).adaptiveForHeight)
            self.textColor = Global.redColorFF4F85
        }
        get {
            return (font?.pointSize)!
        }
    }
}
