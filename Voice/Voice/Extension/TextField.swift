//
//  TextFeild.swift
//  Voice
//
//  Created by kasim on 2018/12/16.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit

extension UITextField {
    @IBInspectable var setOnlyLine: Bool {
        get {
            return false
        }
        set {
            if newValue {
                onlyLine()
            }
        }
    }

    func onlyLine() {
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.white.cgColor

        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }

}
