//
//  ViewController.swift
//  Voice
//
//  Created by kasim on 2018/12/7.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import UIKit

extension UIViewController {

    func setUnderNavigationBackButton() {
        let backImage = UIImage(named: "topArrowWhite")
        navigationController?.navigationBar.backIndicatorImage = backImage
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = backImage
        navigationController?.navigationBar.tintColor = .white

        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

    }

    func removeNavigationBarBottomLine() {
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
    }
}
