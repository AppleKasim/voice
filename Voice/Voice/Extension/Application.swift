//
//  Application.swift
//  Voice
//
//  Created by kasim on 2018/12/1.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import Foundation
import UIKit

extension UIApplication {
    var statusBarView: UIView? {
        guard let statusBarView = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else {
            return nil
        }
        return statusBarView
    }
}
