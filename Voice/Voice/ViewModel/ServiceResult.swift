//
//  ServiceResult.swift
//  Voice
//
//  Created by kasim on 2019/1/14.
//  Copyright © 2019 Shiefu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

enum ServiceResult {
    case ok(message: String)
    case empty
    case failed(message: String)
}

extension ServiceResult {
    var isValid: Bool {
        switch self {
        case .ok:
            return true
        default:
            return false
        }
    }
}

extension ServiceResult {
    var textColor: UIColor {
        switch self {
        case .ok:
            return UIColor(red: 138.0 / 255.0, green: 221.0 / 255.0, blue: 109.0 / 255.0, alpha: 1.0)
        case .empty:
            return UIColor.black
        case .failed:
            return UIColor.red
        }
    }
}

extension ServiceResult {
    var description: String {
        switch self {
        case let .ok(message):
            return message
        case .empty:
            return ""
        case let .failed(message):
            return message
        }
    }
}
