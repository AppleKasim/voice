//
//  AnchorDetailViewModel.swift
//  Voice
//
//  Created by kasim on 2018/11/20.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class AuthorDetailViewModel: SubPageContainer {
    let titles: [String] = ["課程清單\nLISTS", "主播介紹\nTHE BIG EAR"]
    var coursePageItems = BehaviorRelay<[SingleSectionModel<String, ListPageContent>]>(value: [])
//    var textModels: Driver<TextPageContentModel> = Variable<TextPageContentModel>(TextPageContentModel()).asDriver()
    var introduction = BehaviorRelay<CreatorInfo?>(value: nil)

    var authorId: Int!
    let disposeBag = DisposeBag()

    func updateItem() {

//        let textPageContentModel = TextPageContentModel()
//
//        textModels = Observable.just(textPageContentModel)
//            .observeOn(MainScheduler.instance)
//            .asDriver(onErrorJustReturn: TextPageContentModel())

//        var sections: [SectionOfPlayListPageContent] = []
//
//        var coursePageContentModels: [CoursePageContentModel] = []
//        for index in 0 ..< 10 {
//            var coursePageContentModel = CoursePageContentModel()
//            coursePageContentModel.title = "title\(index)"
//            coursePageContentModel.course = "course\(index)"
//            coursePageContentModel.students = "students\(index)"
//            coursePageContentModel.keepAmount = "keepAmount\(index)"
//            coursePageContentModel.chargeType = "chargeType\(index)"
//            coursePageContentModels.append(coursePageContentModel)
//        }
//
//        sections.append(SectionOfPlayListPageContent(header: "section 0", items: coursePageContentModels))
//
//        coursePageItems.accept(sections)

        getInfo(authorId: authorId).subscribe(onNext: { [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .empty:
                print("empty")
            case .succeed(let data):
                print("data")
                strongSelf.introduction.accept(data)
            case .failed(let errorMessage):
                print("failed:\(errorMessage)")
            }
        }).disposed(by: disposeBag)

        getUserAlbums(page: authorId).subscribe(onNext: { [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .empty:
                print("empty")
            //callBack(.empty)
            case .succeed(let data):
               // print("succeed:\(data)")
                var sections: [SingleSectionModel<String, ListPageContent>] = []
                sections.append(SingleSectionModel(header: "section 0", items: data))
                strongSelf.coursePageItems.accept(sections)
            case .failed(let errorMessage):
                print("failed:\(errorMessage)")
                //callBack(.failed)
            }
        }).disposed(by: disposeBag)
    }

    func cellHeight() -> CGFloat {
        return 90 / 667 * Global.screenHeight
    }

    public func getUserAlbums(page: Int) -> Observable<ResponseResult<[AudioBook]>> {
        return HttpService().request(config: AlbumApiConfig.userAlbums(creatorId: authorId, page: page))
    }

    public func getInfo(authorId: Int) -> Observable<ResponseResult<CreatorInfo>> {
        return HttpService().request(config: UserApiConfig.creator(authorId: authorId))
    }
}
