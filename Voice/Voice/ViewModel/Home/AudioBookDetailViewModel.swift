//
//  AudioBookDetailViewModel.swift
//  Voice
//
//  Created by kasim on 2018/12/26.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class AudioBookDetailViewModel: SubPageContainer {
    let titles: [String] = ["內容說明\nABOUT THE BOOK", "章節列表\nSTORY LIST"]
    let reviewItems = BehaviorRelay<[SingleSectionModel<String, PlayIntroductionType>]>(value: [])
    var playListItems = BehaviorRelay<[SingleSectionModel<String, ListPageContent>]>(value: [])
    var introduction = BehaviorRelay<PlayIntroduction?>(value: nil)

    var albumId = 0
    let disposeBag = DisposeBag()

    func updateItem() {
        getInfo(albumId: albumId).subscribe(onNext: { [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .empty:
                print("empty")
            case .succeed(let data):
                print("data")
                strongSelf.introduction.accept(data)
                let singleSection = SingleSectionModel<String, PlayIntroductionType>(header: data.introduce, items: [])
                strongSelf.reviewItems.accept([singleSection])
            case .failed(let errorMessage):
                print("failed:\(errorMessage)")
            }
        }).disposed(by: disposeBag)

        getVoices(albumId: albumId, page: 1).subscribe(onNext: { [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .empty:
                print("empty")
            case .succeed(let data):
                var sections: [SingleSectionModel<String, ListPageContent>] = []
                let header = "共\(data.count)章"
                sections.append(SingleSectionModel(header: header, items: data))
                strongSelf.playListItems.accept(sections)
            case .failed(let errorMessage):
                print("failed:\(errorMessage)")
            }
        }).disposed(by: disposeBag)
    }

    func cellHeight() -> CGFloat {
        return 45 / 667 * Global.screenHeight
    }

    public func getVoices(albumId: Int, page: Int) -> Observable<ResponseResult<[PlayVoice]>> {
        return HttpService().request(config: AlbumApiConfig.voices(albumId: albumId, page: page))
    }

    public func getInfo(albumId: Int) -> Observable<ResponseResult<PlayIntroduction>> {
        return HttpService().request(config: AlbumApiConfig.info(albumId: albumId))
    }
}
