//
//  RecommendationListViewModel.swift
//  Voice
//
//  Created by kasim on 2018/11/10.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct RecommendationListViewModel: SubPageContainer {
    let titles: [String] = ["推薦\nFOLLOWING","社群\nSOCIAL MEDIA","身心靈\nSPIRITUALITY","商業\nECONOMICS","生活\nLife","推薦\nFOLLOWING","社群\nSOCIAL MEDIA","身心靈\nSPIRITUALITY","商業\nECONOMICS"]
    var pageContentTuples: [(String, BehaviorRelay<[SectionOfRecommendationList]>)] = []

    mutating func updateItem() {
        pageContentTuples = titles.map { title -> (String, BehaviorRelay<[SectionOfRecommendationList]>) in
            var sections: [SectionOfRecommendationList] = []

            var recommendationModels: [RecommendationModel] = []
            for index in 0 ..< 10 {
                var recommendationModel = RecommendationModel()
                recommendationModel.introduction = "introduction\(index)"
                recommendationModel.course = "course\(index)"
                recommendationModel.students = "students\(index)"
                recommendationModel.keepAmount = "40\(index)"
                recommendationModel.chargeType = "免費\(index)"
                recommendationModels.append(recommendationModel)
            }

            sections.append(SectionOfRecommendationList(header: "section 0", items: recommendationModels))

            let items = BehaviorRelay<[SectionOfRecommendationList]>(value: [])
            items.accept(sections)

            return (title, items)
        }
    }

    func getItem(pageIndex: Int) -> BehaviorRelay<[SectionOfRecommendationList]>? {
        if pageIndex < pageContentTuples.count {
            return pageContentTuples[pageIndex].1
        }

        return nil
    }

    func getTitle(pageIndex: Int) -> String? {
        if pageIndex < pageContentTuples.count {
            return pageContentTuples[pageIndex].0
        }

        return nil
    }

    func getPageContentTuples(title: String) -> (String, BehaviorRelay<[SectionOfRecommendationList]>)? {
        let filterResult = pageContentTuples.filter {
            $0.0 == title
        }
        return filterResult.first
    }

    func cellHeight() -> CGFloat {
        return 245 / 554 * (Global.notNavigationScreenHeight - Global.tabBarHeight)
    }
}
