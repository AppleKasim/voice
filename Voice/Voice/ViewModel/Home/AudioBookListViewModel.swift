//
//  AudioBookListViewModel.swift
//  Voice
//
//  Created by kasim on 2019/1/8.
//  Copyright © 2019 Shiefu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class AudioBookListViewModel {

    var items = BehaviorRelay<[SingleSectionModel<String, AudioBook>]>(value: [])

    private var albumId = 0
    private let disposeBag = DisposeBag()
    private var latestPage = 0

    func setItems(audioBooks: [AudioBook]) {
        items.accept([SingleSectionModel(header: "section 0", items: audioBooks)])
    }

    func updateItem() {
        getAudioBooks(page: latestPage).subscribe(onNext: { [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .empty:
                print("empty")
            case .succeed(let data):
                var sections: [SingleSectionModel<String, AudioBook>] = []
                sections.append(SingleSectionModel(header: "section 0", items: data))
                strongSelf.items.accept(sections)
                strongSelf.latestPage += 1
            case .failed(let errorMessage):
                print("failed:\(errorMessage)")
            }
        }).disposed(by: disposeBag)
    }

    public func getAudioBooks(page: Int) -> Observable<ResponseResult<[AudioBook]>> {
        return HttpService().request(config: AlbumApiConfig.albumHots(page: page))
    }
}
