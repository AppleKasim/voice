//
//  RecommendationDetailViewModel.swift
//  Voice
//
//  Created by kasim on 2018/11/18.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct RecommendationDetailViewModel: SubPageContainer {
    let titles: [String] = ["內容說明", "課程列表"]
    var playListItems = BehaviorRelay<[SingleSectionModel<String, ListPageContent>]>(value: [])
    var textModels: Driver<TextPageContentModel> = Variable<TextPageContentModel>(TextPageContentModel()).asDriver()

    mutating func updateItem() {

        let textPageContentModel = TextPageContentModel()

        textModels = Observable.just(textPageContentModel)
            .observeOn(MainScheduler.instance)
            .asDriver(onErrorJustReturn: TextPageContentModel())

        var sections: [SingleSectionModel<String, ListPageContent>] = []

        var playListPageContentModels: [PlayListPageContentModel] = []
        for index in 0 ..< 10 {
            var playListPageContentModel = PlayListPageContentModel()
//            playListPageContentModel.title = "title\(index)"
//            playListPageContentModel.detail = "detail\(index)"
//            if index > 5 {
//                playListPageContentModel.isNew = true
//            }
            playListPageContentModels.append(playListPageContentModel)
        }

        sections.append(SingleSectionModel(header: "section 0", items: playListPageContentModels))

        playListItems.accept(sections)
    }

    func cellHeight() -> CGFloat {
        return 45 / 667 * Global.screenHeight
    }
}
