//
//  AnchorListViewModel.swift
//  Voice
//
//  Created by kasim on 2018/11/12.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct AuthorListViewModel {
    let items = BehaviorRelay<[SingleSectionModel<AuthorListHeader, Author>]>(value: [])

    let disposeBag = DisposeBag()

//    func updateItem() {
//        var sections: [SectionOfAnchorList] = []
//
//
//
//        var authorModels: [AuthorModel] = []
//        for index in 0 ..< 10 {
//            var authorModel = AuthorModel()
////            anchorModel.name = "name\(index)"
////            anchorModel.content = "content\(index)"
////            anchorModel.type = "type\(index)"
//
//            authorModels.append(authorModel)
//        }
//
//        sections.append(SectionOfAnchorList(header: header, items: authorModels))
//
//        items.accept(sections)
//    }

    public func updateCreatorList(page: Int, success: (() -> Void)?) {
        getCreatorList(page: page).subscribe(onNext: { result in
            switch result {
            case .empty:
                print("empty")
            //callBack(.empty)
            case .succeed(let data):
                let header = AuthorListHeader(title: "駐站大主播", englishTitle: "COLLABORATE")
                let sections = SingleSectionModel(header: header, items: data)
                self.items.accept([sections])
                guard let strongSuccess = success else { return }
                strongSuccess()
            case .failed(let errorMessage):
                print("failed:\(errorMessage)")
                //callBack(.failed)
            }
        }).disposed(by: disposeBag)
    }

    private func getCreatorList(page: Int) -> Observable<ResponseResult<[Author]>> {
        return HttpService().request(config: UserApiConfig.creatorList(page: 1))
    }
}
