//
//  HomeViewModel.swift
//  Voice
//
//  Created by kasim on 2018/10/26.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class HomeViewModel {
    // output:
    var bannerModels: Driver<[String]>
    let items = BehaviorSubject<[SingleSectionModel<HomeHeader, [HomeModelType]>]>(value: [])
    var sections: [SingleSectionModel<HomeHeader, [HomeModelType]>] = []

    //完整titles
//    let titles: [(chinese: String, english: String)] = [("banner", "banner"), ("走耳課堂", "WALK EAR SCHOOL"), ("有聲小說", "AUDIO BOOKS"), ("駐站大主播", "HOT EARS & MOUTHS"), ("專欄主題", "COLUMN TOPICS")]

    let titles: [(chinese: String, english: String)] = [("banner", "banner"), ("有聲小說", "AUDIO BOOKS"), ("駐站大主播", "HOT EARS & MOUTHS")]

    let disposeBag = DisposeBag()

    init() {
        // swiftlint:disable line_length
        let bannerImagesUrls: [String] = ["https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1502904177239&di=248e850857f3cf2439e49df2fcbb81bc&imgtype=0&src=http%3A%2F%2Fimg4q.duitang.com%2Fuploads%2Fitem%2F201410%2F26%2F20141026170404_vPBsP.jpeg", "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1502904177238&di=1d450db9590ee00d6f2f69cb69db2515&imgtype=0&src=http%3A%2F%2Fimg5.duitang.com%2Fuploads%2Fitem%2F201410%2F26%2F20141026170452_szPAW.jpeg", "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1502904177237&di=553618513c7e59b99c7e27840d4e2507&imgtype=0&src=http%3A%2F%2F06.imgmini.eastday.com%2Fmobile%2F20160719%2F20160719093731_6b3cbd4a26257b07579859a9b6673839_2.jpeg", "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1503125647888&di=549cfbda66d5cca96e46308e05b5edfd&imgtype=0&src=http%3A%2F%2Fpic1.win4000.com%2Fwallpaper%2F9%2F5897d10c9e72a.jpg"]
        // swiftlint:enable line_length

        bannerModels = Observable.just(bannerImagesUrls)
            .observeOn(MainScheduler.instance)
            .asDriver(onErrorJustReturn: [])

        sections = titles.map {
            let homeHeader = getHeader(chinese: $0.chinese, english: $0.english)
            return SingleSectionModel(header: homeHeader, items: [])
        }
        items.onNext(sections)
    }

    func updateItem() {

        sections[0] = SingleSectionModel(header: HomeHeader(), items: [])

        getAudioBooks(page: 1).subscribe(onNext: { [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .empty:
                print("empty")
            //callBack(.empty)
            case .succeed(let data):
                let audioBooksIndex = 1
                let header = strongSelf.getHeader(titlesIndex: audioBooksIndex)
                strongSelf.sections[audioBooksIndex] = SingleSectionModel(header: header, items: [data])
                strongSelf.items.onNext(strongSelf.sections)
            case .failed(let errorMessage):
                print("failed:\(errorMessage)")
                //callBack(.failed)
            }
        }).disposed(by: disposeBag)

        updateCreatorList(page: 1)

//        homeHeader.title = "專欄主題"
//        homeHeader.titleEnglish = "COLUMN TOPICS"
//        sections[4] = SingleSectionModel(header: getHeader(titlesIndex: 4), items: qqq)

        items.onNext(sections)
    }

    private func getHeader(chinese: String, english: String) -> HomeHeader {
        var homeHeader = HomeHeader()
        homeHeader.title = chinese
        homeHeader.titleEnglish = english
        return homeHeader
    }

    private func getHeader(titlesIndex index: Int) -> HomeHeader {
        var homeHeader = HomeHeader()
        homeHeader.title = titles[index].chinese
        homeHeader.titleEnglish = titles[index].english
        return homeHeader
    }

//    public func getData() -> Observable<ResponseResult<[ProductType]>> {
//        return HttpService().request(config: AlbumApiConfig.productType())
//    }

    public func updateCreatorList(page: Int) {
        getCreatorList(page: page).subscribe(onNext: { [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .empty:
                print("empty")
            //callBack(.empty)
            case .succeed(let data):
                let creatorListIndex = 2
                let header = strongSelf.getHeader(titlesIndex: creatorListIndex)
                strongSelf.sections[creatorListIndex] = SingleSectionModel(header: header, items: [data])
                strongSelf.items.onNext(strongSelf.sections)
            case .failed(let errorMessage):
                print("failed:\(errorMessage)")
                //callBack(.failed)
            }
        }).disposed(by: disposeBag)
    }

    private func getCreatorList(page: Int) -> Observable<ResponseResult<[Author]>> {
        return HttpService().request(config: UserApiConfig.creatorList(page: 1))
    }

    private func getAudioBooks(page: Int) -> Observable<ResponseResult<[AudioBook]>> {
        return HttpService().request(config: AlbumApiConfig.albumHots(page: page))
    }
}
