//
//  RecordViewModel.swift
//  Voice
//
//  Created by kasim on 2018/11/20.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import Foundation
import RxDataSources
import RxSwift
import RxCocoa

struct RecordViewModel {
    let titles: [String] = ["商業類","社群類","身心靈類"]
    var pageContentTuples: [(String, BehaviorRelay<[SectionOfRecordModel]>)] = []

    mutating func updateItem() {
        pageContentTuples = titles.map { title -> (String, BehaviorRelay<[SectionOfRecordModel]>) in
            var sections: [SectionOfRecordModel] = []

            var recordModels: [RecordModel] = []
            for index in 0 ..< 10 {
                var recordModel = RecordModel()
                recordModel.title = "title\(index)"
                recordModel.detail = "detail\(index)"
                recordModel.keepAmount = "keepAmount\(index)"
                recordModel.charge = "charge\(index)"
                recordModels.append(recordModel)
            }

            sections.append(SectionOfRecordModel(header: "section 0", items: recordModels))

            let items = BehaviorRelay<[SectionOfRecordModel]>(value: [])
            items.accept(sections)

            return (title, items)
        }
    }

    func getPageContentTuples(title: String) -> (String, BehaviorRelay<[SectionOfRecordModel]>)? {
        let filterResult = pageContentTuples.filter {
            $0.0 == title
        }
        return filterResult.first
    }
}
