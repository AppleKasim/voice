//
//  CourseListViewModel.swift
//  Voice
//
//  Created by kasim on 2018/11/23.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import Foundation
import RxDataSources
import RxSwift
import RxCocoa

struct CourseListViewModel {

    let items = BehaviorRelay<[SingleSectionModel<String, PlayVoice>]>(value: [])

    func updateItem() {
        var sections: [SingleSectionModel<String, PlayVoice>] = []

        var courseListModels: [PlayVoice] = []
        for index in 0 ..< 10 {
            var courseListModel = PlayVoice()
            courseListModel.title = "title\(index)"
            //courseListModel.date = "date\(index)"
            courseListModels.append(courseListModel)
        }

        sections.append(SingleSectionModel<String, PlayVoice>(header: "section 0", items: courseListModels))

        items.accept(sections)
    }
}
