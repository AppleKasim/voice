//
//  PlayTimingViewModel.swift
//  Voice
//
//  Created by kasim on 2018/11/23.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import Foundation
import RxDataSources
import RxSwift
import RxCocoa

struct PlayTimingViewModel {

    let items = BehaviorRelay<[SectionOfPlayTiming]>(value: [])

    func updateItem() {
        var sections: [SectionOfPlayTiming] = []

        var playTimingHeaderModel = PlayTimingHeaderModel()
        playTimingHeaderModel.title = "section 0"
        playTimingHeaderModel.isEnable = true

        var playTimingModels: [PlayTimingModel] = []
        for index in 0 ..< 10 {
            var playTimingModel = PlayTimingModel()
            playTimingModel.content = "content\(index)"
            playTimingModels.append(playTimingModel)
        }

        sections.append(SectionOfPlayTiming(header: playTimingHeaderModel, items: playTimingModels))

        var playTimingHeaderModel2 = PlayTimingHeaderModel()
        playTimingHeaderModel2.title = "section 1"
        playTimingHeaderModel2.isEnable = false

        var playTimingModels2: [PlayTimingModel] = []
        for index in 0 ..< 10 {
            var playTimingModel = PlayTimingModel()
            playTimingModel.content = "content\(index)"
            playTimingModels2.append(playTimingModel)
        }

        sections.append(SectionOfPlayTiming(header: playTimingHeaderModel2, items: playTimingModels2))

        items.accept(sections)
    }
}
