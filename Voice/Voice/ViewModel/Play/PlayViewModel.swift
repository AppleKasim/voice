//
//  PlayViewModel.swift
//  Voice
//
//  Created by kasim on 2018/11/23.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import Foundation
import RxDataSources
import RxSwift
import RxCocoa

class PlayViewModel {

    let reviewItems = BehaviorRelay<[SingleSectionModel<String, PlayIntroductionType>]>(value: [])
//    var playBehaviorRelay = BehaviorRelay<PlayModel>(value: PlayModel())

    var playList = BehaviorRelay<[SingleSectionModel<String, PlayVoice>]>(value: [])
    //var introductionModels: Driver<PlayIntroduction?> = Variable<PlayIntroduction?>(nil).asDriver()

    var smallToLarge: Bool = true {
        didSet {
            //if smallToLarge {
            guard let sectionFisrt = playList.value.first else {
                return
            }
            let newList: [PlayVoice] = sectionFisrt.items.reversed()
            let header = sectionFisrt.header
            playList.accept([SingleSectionModel<String, PlayVoice>(header: header, items: newList)])
//            } else {
//
//            }
        }
    }
    //var albumId: Int?
    let disposeBag = DisposeBag()

    func updateItem() {
        guard let strongAlbumId = PlayManager.share().albumId else { return }

        getInfo(albumId: strongAlbumId).subscribe(onNext: { [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .empty:
                print("empty")
            case .succeed(let data):
                print("data")
                PlayManager.share().playIntroduction.accept(data)
                let singleSection = SingleSectionModel<String, PlayIntroductionType>(header: data.introduce, items: [])
                strongSelf.reviewItems.accept([singleSection])
            case .failed(let errorMessage):
                print("failed:\(errorMessage)")
            }
        }).disposed(by: disposeBag)

        getVoices(albumId: strongAlbumId, page: 1).subscribe(onNext: { [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .empty:
                print("empty")
            //callBack(.empty)
            case .succeed(let data):
                //print("succeed:\(data)")
                var sections: [SingleSectionModel<String, PlayVoice>] = []
                let header = "共\(data.count)章"
                sections.append(SingleSectionModel<String, PlayVoice>(header: header, items: data))
                strongSelf.playList.accept(sections)
                PlayManager.share().voiceList = data
            case .failed(let errorMessage):
                print("failed:\(errorMessage)")
            }
        }).disposed(by: disposeBag)
    }

    public func getVoices(albumId: Int, page: Int) -> Observable<ResponseResult<[PlayVoice]>> {
        return HttpService().request(config: AlbumApiConfig.voices(albumId: albumId, page: page))
    }

    public func getInfo(albumId: Int) -> Observable<ResponseResult<PlayIntroduction>> {
        return HttpService().request(config: AlbumApiConfig.info(albumId: albumId))
    }
}
