//
//  ReportViewModel.swift
//  Voice
//
//  Created by kasim on 2019/1/13.
//  Copyright © 2019 Shiefu. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

struct ReportViewModel {
    //input:
    let text = BehaviorRelay<String>(value: "")
    let albumId = BehaviorRelay<String>(value: "")
    let reportTaps = PublishSubject<Void>()

    //output:
    let textUsable: Driver<Result>  //密码是否可用

    let sendButtonEnabled: Observable<Bool>

    //let reportResult: Driver<Result>

    let disposeBag = DisposeBag()

    init() {
        let service = ValidationService.instance

        textUsable = text
            //.debug()
            .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .flatMap { text in
                return service.validateReportText(text)
            }.asDriver(onErrorJustReturn: .failed(message: "錯誤"))

        sendButtonEnabled = textUsable
            .map { $0.isValid }
            .asObservable()

        let username = Variable<String>("")
        let password = Variable<String>("")
        let registerResult: Observable<Result>

        let usernameAndPassword = Observable.combineLatest(username.asObservable(), password.asObservable()) {
            ($0, $1)
        }

        let aaaa = reportTaps.asObservable().withLatestFrom(usernameAndPassword)

        registerResult = reportTaps.asObservable().withLatestFrom(usernameAndPassword)
            .flatMapLatest { (username, password) in
                return service.register(username, password: password)
                    .observeOn(MainScheduler.instance)
                    .catchErrorJustReturn(.failed(message: "注册出错"))
            }
            .share(replay: 1, scope: .forever)





        let albumIdAndText = Observable.combineLatest(albumId, text) {
            ($0, $1)
        }

        //reportResult
//            let tttr = reportTaps.asObservable().withLatestFrom(albumIdAndText)
//                .flatMapLatest { (arg) -> String in
//
//                    let (username, password) = arg
//                    return "wwww"
//            }
//            .share(replay: 1, scope: .forever)

//            let ttt = reportTaps.withLatestFrom(albumIdAndText)
//                .flatMapLatest { (username, password) in
//                        guard let strongAlbumId = albumId else {
//                            return .failed(message: "albumId = nil")
//                        }
//
//
//
//                        return "" //sendReport(albumId: strongAlbumId, content: text)
////                    return service.register(username, password: password)
////                        .observeOn(MainScheduler.instance)
////                        .catchErrorJustReturn(.failed(message: "注册出错"))
//                }
//                .asDriver(onErrorJustReturn: .failed(message: "傳輸錯誤"))
                //.share(replay: 1, scope: .forever)
            //.observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
//                .flatMapLatest { (albumId, text) in
//                                    guard let strongAlbumId = albumId else {
//                                        return .failed(message: "albumId = nil")
//                                    }
//
//
//
//                return sendReport(albumId: strongAlbumId, content: text)
//
//            }
//            .flatMap { responseResult in
//                switch responseResult {
//                case .empty:
//                    return .just(.empty)
//                case let .succeed(data):
//                    return .just(.ok(message: "注册成功"))
//                case let .failed(message):
//                    return .just(.failed(message: message))
//                }
//
//            }
            //.asDriver(onErrorJustReturn: .failed(message: "傳輸錯誤"))
        //.asDriver(onErrorJustReturn: .failed(message: "傳輸錯誤"))
    }

    public func sendReport(albumId: Int, content: String) -> Observable<ResponseResult<EmptyModel>> {
        return HttpService().request(config: AlbumApiConfig.report(voiceId: albumId, content: content))
    }


//    public func getReportData(albumId: Int, content: String) {
//        return sendReport(albumId: albumId, content: content)
//            .subscribe(onNext: { result in
//                switch result {
//                case .empty:
//                    return .empty
//                case .succeed(let data):
//                    print("data")
//                    return .ok(message: data)
//                case .failed(let errorMessage):
//                    //print("failed:\(errorMessage)")
//                    return .failed(message: errorMessage)
//                }
//            }).disposed(by: disposeBag)
//    }

}
