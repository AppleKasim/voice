//
//  MyNotificationViewModel.swift
//  Voice
//
//  Created by kasim on 2018/11/25.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct MyNotificationViewModel {
    let items = BehaviorRelay<[SectionOfMyNotification]>(value: [])

    func updateItem() {
        var sections: [SectionOfMyNotification] = []

        var notificationModels: [MyNotificationModel] = []
        for index in 0 ..< 10 {
            var notificationModel = MyNotificationModel()
            notificationModel.title = "title\(index)"
            notificationModel.date = "date\(index)"

            notificationModels.append(notificationModel)
        }

        sections.append(SectionOfMyNotification(header: "section 0", items: notificationModels))

        items.accept(sections)
    }

}
