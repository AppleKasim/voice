//
//  MyAlarmClockViewModel.swift
//  Voice
//
//  Created by kasim on 2018/11/26.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct MyAlarmClockViewModel {
    let items = BehaviorRelay<[SectionOfMyAlarmClock]>(value: [])

    func updateItem() {
        var sections: [SectionOfMyAlarmClock] = []

        var myAlarmClockModels: [MyAlarmClockModel] = []

        var myAlarmClockModel1 = MyAlarmClockModel()
        myAlarmClockModel1.title = "鬧鐘開關"
        myAlarmClockModel1.isOnForSwitch = true
        myAlarmClockModels.append(myAlarmClockModel1)

        var myAlarmClockModel2 = MyAlarmClockModel()
        myAlarmClockModel2.title = "重複"
        myAlarmClockModel2.selectItem = "option\(index)"
        myAlarmClockModels.append(myAlarmClockModel2)

        var myAlarmClockModel3 = MyAlarmClockModel()
        myAlarmClockModel3.title = "時間"
        myAlarmClockModel3.selectItem = "option\(index)"
        myAlarmClockModels.append(myAlarmClockModel3)

        var myAlarmClockModel4 = MyAlarmClockModel()
        myAlarmClockModel4.title = "鈴聲"
        myAlarmClockModel4.selectItem = "option\(index)"
        myAlarmClockModels.append(myAlarmClockModel4)

        sections.append(SectionOfMyAlarmClock(header: "section 0", items: myAlarmClockModels))

        items.accept(sections)
    }
}
