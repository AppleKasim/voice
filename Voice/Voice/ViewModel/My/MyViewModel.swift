//
//  MyViewModel.swift
//  Voice
//
//  Created by kasim on 2018/11/11.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct MyViewModel {
    let items = BehaviorSubject<[SectionOfMy]>(value: [])
    // swiftlint:disable line_length
    let cellData: [(title: String, image: String)] = [("收聽歷史", "settingTime"), ("我的收藏", "settingCollect"), ("購買紀錄", "settingPurchase"), ("休息通知", "settingNotice"), ("儲值之幣", "settingCoins"), ("設置", "settingSetting"), ("打小報告", "settingMail"), ("版權說明", "settingCopyrights")]
    // swiftlint:enable line_length

    var headerDriver: Driver<MyHeader>

    init() {
        var sections: [SectionOfMy] = []

        let myHeader =  MyHeader(name: "朵朵兒", currencies: "100", profileImageUrl: "", isCheckIn: true)

        let myModels: [MyModel] = cellData.map {
                MyModel(title: $0.title, imageName: $0.image)
        }

        sections.append(SectionOfMy(header: myHeader, items: myModels))

        items.onNext(sections)

        headerDriver = Observable.just(myHeader)
            .observeOn(MainScheduler.instance)
            .asDriver(onErrorJustReturn: MyHeader())

    }

}
