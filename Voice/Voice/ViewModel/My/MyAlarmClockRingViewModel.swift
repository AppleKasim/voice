//
//  MyAlarmClockRingViewModel.swift
//  Voice
//
//  Created by kasim on 2018/11/27.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct MyAlarmClockRingViewModel {
    let items = BehaviorRelay<[SingleSectionModelMy<MyTitleAndDot>]>(value: [])

    func updateItem() {
        var sections: [SingleSectionModelMy<MyTitleAndDot>] = []

        var myAlarmClockModels: [MyTitleAndDot] = []

        var myAlarmClockModel1 = MyTitleAndDot()
        myAlarmClockModel1.title = "預設鈴聲"
        myAlarmClockModel1.hasDot = false
        myAlarmClockModels.append(myAlarmClockModel1)

        var myAlarmClockModel2 = MyTitleAndDot()
        myAlarmClockModel2.title = "正在播放曲目"
        myAlarmClockModel2.hasDot = true
        myAlarmClockModels.append(myAlarmClockModel2)

        var myAlarmClockModel3 = MyTitleAndDot()
        myAlarmClockModel3.title = "已購課程"
        myAlarmClockModel3.hasDot = true
        myAlarmClockModels.append(myAlarmClockModel3)

        sections.append(SingleSectionModelMy<MyTitleAndDot>(header: "section 0", items: myAlarmClockModels))

        items.accept(sections)
    }

}
