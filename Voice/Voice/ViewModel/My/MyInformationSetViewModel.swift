//
//  MyInformationSetViewModel.swift
//  Voice
//
//  Created by kasim on 2018/11/27.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct MyInformationSetViewModel {
    let items = BehaviorRelay<[SingleSectionModelMy<MyInformationSetModel>]>(value: [])

    //input
    let nickName = BehaviorRelay<String>(value: "")

    //outPut
    var nickNameUsable: Driver<Result>

    //input:
    let registerTaps = PublishSubject<Void>()

    // output:
    var registerButtonEnabled: Driver<Bool>
    var registerResult: Driver<Result>

    var textModels: Driver<TextPageContentModel> = Variable<TextPageContentModel>(TextPageContentModel()).asDriver()

    init() {
        nickNameUsable = Variable<Result>(.empty).asDriver()
        registerButtonEnabled = Variable<Bool>(false).asDriver()
        registerResult = Variable<Result>(.empty).asDriver()
    }

    mutating func updateItemForNickName() {
        let service = ValidationService.instance

        nickNameUsable = nickName
            .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .flatMapLatest { nickName in
                return service.validateUsername(nickName)
            }
            .asDriver(onErrorJustReturn: .failed(message: "failed"))

        registerResult = registerTaps
            .withLatestFrom(nickName.asObservable())
            .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .flatMapLatest { nickName in
                return service.change(forNickName: nickName)
            }
            .asDriver(onErrorJustReturn: .failed(message: "failed"))

        registerButtonEnabled = nickName
            .map { $0.count > 0 }
            .asDriver(onErrorJustReturn: false)

        var sections: [SingleSectionModelMy<MyInformationSetModel>] = []

        var myInformationSetModels: [MyInformationSetModel] = []

        var myInformationSetModel = MyInformationSetModel()
        myInformationSetModel.title = "暱稱"
        myInformationSetModels.append(myInformationSetModel)

        sections.append(SingleSectionModelMy<MyInformationSetModel>(header: "section 0", items: myInformationSetModels))

        items.accept(sections)
    }

    func updateItemForEmail() {
        var sections: [SingleSectionModelMy<MyInformationSetModel>] = []

        var myInformationSetModels: [MyInformationSetModel] = []

        var myInformationSetModel = MyInformationSetModel()
        myInformationSetModel.title = "電子郵件"
        myInformationSetModels.append(myInformationSetModel)

        sections.append(SingleSectionModelMy<MyInformationSetModel>(header: "section 0", items: myInformationSetModels))

        items.accept(sections)
    }

    func updateItemForGender() {
        var sections: [SingleSectionModelMy<MyInformationSetModel>] = []

        var myInformationSetModels: [MyInformationSetModel] = []

        var myInformationSetModel1 = MyInformationSetModel()
        myInformationSetModel1.title = "我是帥哥"
        myInformationSetModels.append(myInformationSetModel1)

        var myInformationSetModel2 = MyInformationSetModel()
        myInformationSetModel2.title = "我是正妹"
        myInformationSetModels.append(myInformationSetModel2)

        sections.append(SingleSectionModelMy<MyInformationSetModel>(header: "section 0", items: myInformationSetModels))

        items.accept(sections)
    }

    func updateItemForCity() {
        var sections: [SingleSectionModelMy<MyInformationSetModel>] = []

        var myInformationSetModels: [MyInformationSetModel] = []

        var myInformationSetModel1 = MyInformationSetModel()
        myInformationSetModel1.title = "國家"
        myInformationSetModels.append(myInformationSetModel1)

        var myInformationSetModel2 = MyInformationSetModel()
        myInformationSetModel2.title = "城市"
        myInformationSetModels.append(myInformationSetModel2)

        sections.append(SingleSectionModelMy<MyInformationSetModel>(header: "section 0", items: myInformationSetModels))

        items.accept(sections)
    }

    func updateItemForChangePassword() {
        var sections: [SingleSectionModelMy<MyInformationSetModel>] = []

        var myInformationSetModels: [MyInformationSetModel] = []

        var myInformationSetModel1 = MyInformationSetModel()
        myInformationSetModel1.title = "當前密碼,請輸入當前密碼"
        myInformationSetModels.append(myInformationSetModel1)

        var myInformationSetModel2 = MyInformationSetModel()
        myInformationSetModel2.title = "新密碼,請設定8位數以上英數字"
        myInformationSetModels.append(myInformationSetModel2)

        var myInformationSetModel3 = MyInformationSetModel()
        myInformationSetModel3.title = "重複密碼,請重複輸入一次新密碼"
        myInformationSetModels.append(myInformationSetModel3)

        sections.append(SingleSectionModelMy<MyInformationSetModel>(header: "section 0", items: myInformationSetModels))

        items.accept(sections)
    }

    func updateItemForSignature() {
        var sections: [SingleSectionModelMy<MyInformationSetModel>] = []

        var myInformationSetModels: [MyInformationSetModel] = []

        var myInformationSetModel = MyInformationSetModel()
        myInformationSetModel.title = "個性簽名"
        myInformationSetModels.append(myInformationSetModel)

        sections.append(SingleSectionModelMy<MyInformationSetModel>(header: "section 0", items: myInformationSetModels))

        items.accept(sections)
    }

}
