//
//  MyInformationViewModel.swift
//  Voice
//
//  Created by kasim on 2018/11/27.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct MyInformationViewModel {
    let items = BehaviorRelay<[SingleSectionModelMy<MyTitleAndSelectItem>]>(value: [])

    func updateItem() {
        var sections: [SingleSectionModelMy<MyTitleAndSelectItem>] = []

        var myTitleAndSelectItems: [MyTitleAndSelectItem] = []

        var myTitleAndSelectItem1 = MyTitleAndSelectItem()
        myTitleAndSelectItem1.title = "言之ID: 12345"
        myTitleAndSelectItem1.selectItem = "selectItem"
        myTitleAndSelectItems.append(myTitleAndSelectItem1)

        var myTitleAndSelectItem2 = MyTitleAndSelectItem()
        myTitleAndSelectItem2.title = "暱稱"
        myTitleAndSelectItem2.selectItem = "selectItem"
        myTitleAndSelectItems.append(myTitleAndSelectItem2)

        var myTitleAndSelectItem3 = MyTitleAndSelectItem()
        myTitleAndSelectItem3.title = "電子郵件"
        myTitleAndSelectItem3.selectItem = "selectItem"
        myTitleAndSelectItems.append(myTitleAndSelectItem3)

        var myTitleAndSelectItem4 = MyTitleAndSelectItem()
        myTitleAndSelectItem4.title = "性別"
        myTitleAndSelectItem4.selectItem = "selectItem"
        myTitleAndSelectItems.append(myTitleAndSelectItem4)

        var myTitleAndSelectItem5 = MyTitleAndSelectItem()
        myTitleAndSelectItem5.title = "生日"
        myTitleAndSelectItem5.selectItem = "selectItem"
        myTitleAndSelectItems.append(myTitleAndSelectItem5)

        var myTitleAndSelectItem6 = MyTitleAndSelectItem()
        myTitleAndSelectItem6.title = "城市"
        myTitleAndSelectItem6.selectItem = "selectItem"
        myTitleAndSelectItems.append(myTitleAndSelectItem6)

        var myTitleAndSelectItem7 = MyTitleAndSelectItem()
        myTitleAndSelectItem7.title = "修改密碼"
        myTitleAndSelectItem7.selectItem = "selectItem"
        myTitleAndSelectItems.append(myTitleAndSelectItem7)

        var myTitleAndSelectItems2: [MyTitleAndSelectItem] = []

        var myTitleAndSelectItem8 = MyTitleAndSelectItem()
        myTitleAndSelectItem8.title = "個性簽名"
        myTitleAndSelectItem8.selectItem = "selectItem"
        myTitleAndSelectItems2.append(myTitleAndSelectItem8)

        sections.append(SingleSectionModelMy<MyTitleAndSelectItem>(header: "section 0", items: myTitleAndSelectItems))

        sections.append(SingleSectionModelMy<MyTitleAndSelectItem>(header: "section 1", items: myTitleAndSelectItems2))

        items.accept(sections)
    }

}
