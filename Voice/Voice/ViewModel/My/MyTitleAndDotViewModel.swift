//
//  MyTitleAndDotViewModel.swift
//  Voice
//
//  Created by kasim on 2018/11/26.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct MyTitleAndDotViewModel {
    let items = BehaviorRelay<[SingleSectionModelMy<MyTitleAndDot>]>(value: [])

    func updateItem() {
        var sections: [SingleSectionModelMy<MyTitleAndDot>] = []

        var myAlarmClockModels: [MyTitleAndDot] = []

        var myAlarmClockModel1 = MyTitleAndDot()
        myAlarmClockModel1.title = "黑體"
        myAlarmClockModel1.hasDot = false
        myAlarmClockModels.append(myAlarmClockModel1)

        var myAlarmClockModel2 = MyTitleAndDot()
        myAlarmClockModel2.title = "宋體"
        myAlarmClockModel2.hasDot = true
        myAlarmClockModels.append(myAlarmClockModel2)

        sections.append(SingleSectionModelMy<MyTitleAndDot>(header: "section 0", items: myAlarmClockModels))

        items.accept(sections)
    }

    func updateItemForRepeatSet() {
        var sections: [SingleSectionModelMy<MyTitleAndDot>] = []

        var myAlarmClockModels: [MyTitleAndDot] = []

        var myAlarmClockModel1 = MyTitleAndDot()
        myAlarmClockModel1.title = "星期一"
        myAlarmClockModel1.hasDot = false
        myAlarmClockModels.append(myAlarmClockModel1)

        var myAlarmClockModel2 = MyTitleAndDot()
        myAlarmClockModel2.title = "星期二"
        myAlarmClockModel2.hasDot = true
        myAlarmClockModels.append(myAlarmClockModel2)

        var myAlarmClockModel3 = MyTitleAndDot()
        myAlarmClockModel3.title = "星期三"
        myAlarmClockModel3.hasDot = true
        myAlarmClockModels.append(myAlarmClockModel3)

        var myAlarmClockModel4 = MyTitleAndDot()
        myAlarmClockModel4.title = "星期四"
        myAlarmClockModel4.hasDot = true
        myAlarmClockModels.append(myAlarmClockModel4)

        var myAlarmClockModel5 = MyTitleAndDot()
        myAlarmClockModel5.title = "星期五"
        myAlarmClockModel5.hasDot = true
        myAlarmClockModels.append(myAlarmClockModel5)

        var myAlarmClockModel6 = MyTitleAndDot()
        myAlarmClockModel6.title = "星期六"
        myAlarmClockModel6.hasDot = true
        myAlarmClockModels.append(myAlarmClockModel6)

        var myAlarmClockModel7 = MyTitleAndDot()
        myAlarmClockModel7.title = "星期日"
        myAlarmClockModel7.hasDot = true
        myAlarmClockModels.append(myAlarmClockModel7)

        sections.append(SingleSectionModelMy<MyTitleAndDot>(header: "section 0", items: myAlarmClockModels))

        items.accept(sections)
    }
}
