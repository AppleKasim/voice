//
//  MySetHomeViewModel.swift
//  Voice
//
//  Created by kasim on 2018/11/25.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct MySetHomeViewModel {
    let items = BehaviorRelay<[SectionOfMySetHome]>(value: [])

    func updateItem() {
        var sections: [SectionOfMySetHome] = []

        var setHomeModels: [MySetHomeModel] = []

        let setHomeModel1 = MySetHomeModel(title: "特色鬧鐘")
        let setHomeModel2 = MySetHomeModel(title: "清除緩存")
        let setHomeModel3 = MySetHomeModel(title: "顯示字型")

        setHomeModels.append(setHomeModel1)
        var setHomeModels2: [MySetHomeModel] = []
        setHomeModels2.append(setHomeModel2)
        var setHomeModels3: [MySetHomeModel] = []
        setHomeModels3.append(setHomeModel3)
        sections.append(SectionOfMySetHome(header: "section 0", items: setHomeModels))
        sections.append(SectionOfMySetHome(header: "section 1", items: setHomeModels2))
        sections.append(SectionOfMySetHome(header: "section 2", items: setHomeModels3))

        items.accept(sections)
    }

}
