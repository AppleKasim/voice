//
//  ClassificationViewModel.swift
//  Voice
//
//  Created by kasim on 2018/11/4.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct ClassificationViewModel {
    let items = BehaviorSubject<[SectionOfClassification]>(value: [])

    init() {
        var sections: [SectionOfClassification] = []

        var classificationModels: [ClassificationModel] = []
        for index in 0 ..< 10 {
            var classificationModel = ClassificationModel()
            classificationModel.content = "content\(index)"
            classificationModel.name = "name\(index)"
            classificationModel.englishName = "englishName\(index)"
            classificationModels.append(classificationModel)
        }

        sections.append(SectionOfClassification(header: "section 0", items: classificationModels))

        items.onNext(sections)
    }

}
