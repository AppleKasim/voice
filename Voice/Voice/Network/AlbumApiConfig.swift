//
//  AlbumApiConfig.swift
//  Voice
//
//  Created by kasim on 2018/12/21.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import Foundation
import Moya

public enum AlbumApiConfig {
    case productType()
    case categories()
    case rows(categoryId: Int, page: Int)
    case info(albumId: Int)
    case voices(albumId: Int, page: Int)
    case voice()
    case voiceClick()
    case userAlbums(creatorId: Int, page: Int)
    case messages()
    case historyAlbums()
    case historyVoices()
    case albumHots(page: Int)
    case report(voiceId: Int, content: String)
}

extension AlbumApiConfig: TargetType {
    public var baseURL: URL {
        return URL(string: Global.baseUrl)!
    }

    public var path: String {
        switch self {
        case .productType:
            return "/album/product_type"
        case .categories:
            return "/album/categories"
        case .rows:
            return "/album/rows"
        case .info:
            return "/album/info"
        case .voices:
            return "/album/voices"
        case .voice:
            return "/album/voice"
        case .voiceClick:
            return "/album/voice_click"
        case .userAlbums:
            return "/user/albums"
        case .messages:
            return "/album/messages"
        case .historyAlbums:
            return "/history/albums"
        case .historyVoices:
            return "/history/voices"
        case .albumHots:
            return "/album/hots"
        case .report:
            return "/album/report"
        }
    }

    public var method: Moya.Method {
        switch self {
        case .report:
            return .post
        default:
            return .get
        }
    }

    public var task: Task {
        var parameters: [String: Any] = [:]
        switch self {
        case .rows(let categoryId, let page):
            parameters = ["category_id": categoryId, "page": page]
        case .userAlbums(let creatorId, let page):
            parameters = ["creator_id": creatorId, "page": page]
        case .info(let albumId):
            parameters = ["album_id": albumId]
        case let .voices(albumId, page):
            parameters = ["album_id": albumId, "page": page]
        case let .albumHots(page):
            parameters = ["page": page]
        case let .report(voiceId, content):
            parameters = ["album_id": voiceId, "content": content]
        default:
            return .requestPlain
        }
        return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
    }

    //是否执行Alamofire验证
    public var validate: Bool {
        return false
    }

    //这个就是做单元测试模拟的数据，只会在单元测试文件中有作用
    public var sampleData: Data {
        return "{}".data(using: String.Encoding.utf8)!
    }

    public var headers: [String: String]? {
        return nil
    }
}
