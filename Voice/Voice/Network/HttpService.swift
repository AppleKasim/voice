//
//  HttpClient.swift
//  RxSwiftTest
//
//  Created by 小七 on 2017/10/23.
//  Copyright © 2017年 Seven. All rights reserved.
//

import Foundation
import RxSwift
import Moya

public struct HttpService {
    let albumProvider = MoyaProvider<AlbumApiConfig>()
    let userProvider = MoyaProvider<UserApiConfig>()

    func request<Element: Codable>(config: TargetType) -> Observable<ResponseResult<Element>> {
        return Observable.create({ (observable) -> Disposable in
            let primitiveSequence: PrimitiveSequence<SingleTrait, Response>

            switch config {
            case let album as AlbumApiConfig:
                primitiveSequence = self.albumProvider.rx.request(album)
            case let user as UserApiConfig:
                primitiveSequence = self.userProvider.rx.request(user)
            default:
                fatalError("錯誤的Api Config")
            }

            let callBack = primitiveSequence
                .subscribe { (responseResult) in
                    switch responseResult {
                    case let .success(response):
                        do {
//                            //顯示response結果
//                            let responseString = String.init(data: response.data, encoding: String.Encoding.utf8)!
//
//                            print("responseString: \(responseString.replacingOccurrences(of: "\\/", with: "/").replacingOccurrences(of: "\"", with: "\""))")

                            let decoder = JSONDecoder()
                            let data = try decoder.decode(ReponseData<Element>.self, from: response.data)
                            let subjects = data.data
                            let result = (subjects == nil) ? ResponseResult.empty: ResponseResult.succeed(data: subjects!)
                            observable.onNext(result)
                        } catch let error {
                            self.requestError(message: error.localizedDescription)
                            observable.onNext(ResponseResult.failed(message: error.localizedDescription))
                        }
                    case let .error(error):
                        self.requestError(message: error.localizedDescription)
                        observable.onNext(ResponseResult.failed(message: error.localizedDescription))
                    }
            }

            return callBack
        })
    }

    private func requestError(message: String) {
        // 统一处理错误
    }
    
}
