//
//  UserApiConfig.swift
//  Voice
//
//  Created by kasim on 2018/12/28.
//  Copyright © 2018 Shiefu. All rights reserved.
//

import Foundation
import Moya

public enum UserApiConfig {
    case login()
    case logout()
    case register()
    case modify()
    case changePassword()
    case uploadPhoto()
    case creator(authorId: Int)
    case audience()
    case transactions()
    case collections()
    case creatorList(page: Int)
}

extension UserApiConfig: TargetType {
    public var baseURL: URL {
        return URL(string: Global.baseUrl)!
    }

    public var path: String {
        switch self {
        case .login:
            return "/user/login"
        case .logout:
            return "/user/logout"
        case .register:
            return "/user/register"
        case .modify:
            return "/user/modify"
        case .changePassword:
            return "/user/fix_password"
        case .uploadPhoto:
            return "/user/upload_photo"
        case .creator:
            return "/user/creator"
        case .audience:
            return "/user/audience"
        case .transactions:
            return "/user/transactions"
        case .collections:
            return "/user/collections"
        case .creatorList:
            return "/user/creator_list"
        }
    }

    public var method: Moya.Method {
        return .get
    }

    public var task: Task {
        var parameters: [String: Any] = [:]
        switch self {
        case .creatorList(let page):
            parameters = ["page": page]
        case .creator(let authorId):
            parameters = ["id": authorId]
//        case .info(let albumId):
//            let parameters = ["album_id": albumId]
//            return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
//        case let .voices(albumId, page):
//            let parameters = ["album_id": albumId, "page": page]
//            return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
        default:
            return .requestPlain
        }
        return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
    }

    //是否执行Alamofire验证
    public var validate: Bool {
        return false
    }

    //这个就是做单元测试模拟的数据，只会在单元测试文件中有作用
    public var sampleData: Data {
        return "{}".data(using: String.Encoding.utf8)!
    }

    public var headers: [String: String]? {
        return nil
    }
}
