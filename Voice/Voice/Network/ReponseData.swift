//
//  ReponseResult.swift
//  RxSwiftTest
//
//  Created by 小七 on 2017/10/24.
//  Copyright © 2017年 Seven. All rights reserved.
//

import Foundation

class ReponseData<T: Codable>: Codable {
    var status: String?
    var message: String?
    var path: String?
    var data: T?
}
